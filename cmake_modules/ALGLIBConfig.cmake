#sudo apt install libalglib-dev
 
# Look for the necessary header
set(ALGLIB_INCLUDE_DIR /usr/include/libalglib)
mark_as_advanced(ALGLIB_INCLUDE_DIR)

# Look for the necessary library
set(ALGLIB_LIBRARY /usr/lib/x86_64-linux-gnu/libalglib.so)
mark_as_advanced(ALGLIB_LIBRARY)

set(ALGLIB_FOUND 1)


    set(ALGLIB_INCLUDE_DIRS ${ALGLIB_INCLUDE_DIR})
    set(ALGLIB_LIBRARIES ${ALGLIB_LIBRARY})
    if(NOT TARGET ALGLIB::ALGLIB)
        add_library(ALGLIB::ALGLIB UNKNOWN IMPORTED)
        set_target_properties(ALGLIB::ALGLIB PROPERTIES
            IMPORTED_LOCATION             "${ALGLIB_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${ALGLIB_INCLUDE_DIR}")
    endif()
