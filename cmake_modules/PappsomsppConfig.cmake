# Copyright : Olivier Langella (CNRS)
# License : GPL-3.0+
# Authors : Olivier Langella

set(PAPPSOMSPP_QT5_FOUND 1)
set(PAPPSOMSPP_WIDGET_QT5_FOUND 1)
set(PAPPSOMSPP_INCLUDE_DIR ${CMAKE_INSTALL_PREFIX}/include/pappsomspp)
mark_as_advanced(PAPPSOMSPP_INCLUDE_DIR)
set(PAPPSOMSPP_QT5_LIBRARY ${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/libpappsomspp-qt5.so.${PAPPSOMSPP_VERSION})
mark_as_advanced(PAPPSOMSPP_QT5_LIBRARY)
set(PAPPSOMSPP_WIDGET_QT5_LIBRARY ${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/libpappsomspp-widget-qt5.so.${PAPPSOMSPP_VERSION})
mark_as_advanced(PAPPSOMSPP_WIDGET_QT5_LIBRARY)

if(NOT TARGET Pappso::Core)
        add_library(Pappso::Core UNKNOWN IMPORTED)
        set_target_properties(Pappso::Core PROPERTIES
            IMPORTED_LOCATION             "${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/libpappsomspp-qt5.so.${PAPPSOMSPP_VERSION}"
            INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/include/pappsomspp")
endif()


if(NOT TARGET Pappso::Widget)
        add_library(Pappso::Widget UNKNOWN IMPORTED)
        set_target_properties(Pappso::Widget PROPERTIES
            IMPORTED_LOCATION             "${CMAKE_INSTALL_PREFIX}/lib${LIB_SUFFIX}/libpappsomspp-widget-qt5.so.${PAPPSOMSPP_VERSION}"
            INTERFACE_INCLUDE_DIRECTORIES "${CMAKE_INSTALL_PREFIX}/include/pappsomspp")
endif()
