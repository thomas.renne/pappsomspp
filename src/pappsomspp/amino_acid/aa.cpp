/**
 * \file pappsomspp/amino_acid/aaBase.cpp
 * \date 7/3/2015
 * \author Olivier Langella
 * \brief amino acid model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "aa.h"
#include <QDebug>
#include <vector>
#include <QStringList>
#include <algorithm>


namespace pappso
{

Aa::Aa(char aa_letter) : AaBase(aa_letter)
{
}


Aa::Aa(AminoAcidChar aa_char) : AaBase(aa_char)
{
}

Aa::Aa(const Aa &other) : AaBase(other), m_listMod(other.m_listMod)
{
}


Aa::Aa(Aa &&toCopy) // move constructor
    : AaBase(toCopy), m_listMod(std::move(toCopy.m_listMod))
{
}

Aa::~Aa()
{
}

Aa &
Aa::operator=(const Aa &toCopy)
{
    m_aaLetter = toCopy.m_aaLetter;
    m_listMod  = toCopy.m_listMod;
    return *this;
}

const std::vector<AaModificationP> &
Aa::getModificationList() const
{
    return m_listMod;
}

pappso_double
Aa::getMass() const
{
    qDebug() << "Aa::getMass() begin";
    pappso_double mass = AaBase::getMass();
    for(auto &&mod : m_listMod)
    {
        mass += mod->getMass();
    }

    // qDebug() << "Aa::getMass() end " << mass;
    return mass;
}

const QString
Aa::toAbsoluteString() const
{
    QString seq = "";
    seq += this->getLetter();
    auto it(m_listMod.begin());
    if(it != m_listMod.end())
    {
        QStringList modification_str_list;
        while(it != m_listMod.end())
        {
            modification_str_list << (*it)->getAccession();
            it++;
        }
        if(modification_str_list.size() > 0)
            seq += QString("(%1)").arg(modification_str_list.join(","));
    }
    return seq;
}

const QString
Aa::toString() const
{
    QString seq = "";
    seq += this->getLetter();
    auto it(m_listMod.begin());
    if(it != m_listMod.end())
    {
        QStringList modification_str_list;
        while(it != m_listMod.end())
        {
            if(!(*it)->isInternal())
            {
                modification_str_list << (*it)->getAccession();
            }
            it++;
        }
        if(modification_str_list.size() > 0)
            seq += QString("(%1)").arg(modification_str_list.join(","));
    }
    return seq;
}


void
Aa::removeAaModification(AaModificationP mod)
{
    std::vector<AaModificationP>::iterator it =
        std::find(m_listMod.begin(), m_listMod.end(), mod);
    if(it != m_listMod.end()) {
        m_listMod.erase(it);
    }

    qDebug() << m_listMod << endl;
}

void
Aa::addAaModification(AaModificationP aaModification)
{
    qDebug() << "Aa::addAaModification begin";
    qDebug() << aaModification->getAccession();
    m_listMod.push_back(aaModification);
    sort(m_listMod.begin(), m_listMod.end());
}

void
Aa::replaceAaModification(AaModificationP oldmod, AaModificationP newmod)
{
    std::replace(m_listMod.begin(), m_listMod.end(), oldmod, newmod);
}

int
Aa::getNumberOfAtom(AtomIsotopeSurvey atom) const
{
    int number_of_carbon = AaBase::getNumberOfAtom(atom);
    for(auto &&mod : m_listMod)
    {
        number_of_carbon += mod->getNumberOfAtom(atom);
    }

    // qDebug() << "Aa::getMass() end " << mass;
    return number_of_carbon;
}


int
Aa::getNumberOfIsotope(Isotope isotope) const
{
    int number = 0;
    for(auto &&mod : m_listMod)
    {
        number += mod->getNumberOfIsotope(isotope);
    }

    // qDebug() << "Aa::getMass() end " << mass;
    return number;
}

unsigned int
Aa::getNumberOfModification(AaModificationP mod) const
{
    unsigned int number_of_mod = 0;
    for(auto &&modb : m_listMod)
    {
        if(modb == mod)
            number_of_mod += 1;
    }

    // qDebug() << "Aa::getMass() end " << mass;
    return number_of_mod;
}

AaModificationP
Aa::getInternalNterModification() const
{
    for(auto &&modb : m_listMod)
    {
        if(modb->getAccession().startsWith("internal:Nter_"))
            return modb;
    }
    return nullptr;
}

AaModificationP
Aa::getInternalCterModification() const
{
    for(auto &&modb : m_listMod)
    {
        if(modb->getAccession().startsWith("internal:Cter_"))
            return modb;
    }
    return nullptr;
}

void
Aa::removeInternalNterModification()
{
    std::remove_if(
    m_listMod.begin(), m_listMod.end(), [](AaModificationP const &mod) {
        return mod->getAccession().startsWith("internal:Nter_");
    });
}

void
Aa::removeInternalCterModification()
{
    std::remove_if(
    m_listMod.begin(), m_listMod.end(), [](AaModificationP const &mod) {
        return mod->getAccession().startsWith("internal:Cter_");
    });
}

bool
Aa::isLesser(Aa const &r) const
{
    qDebug() << m_listMod << "//" << r.m_listMod;
    // qDebug() << "operator<(const Aa& l, const Aa& r)";
    if(m_aaLetter == r.m_aaLetter)
    {
        std::size_t a = m_listMod.size();
        std::size_t b = r.m_listMod.size();

        if(a == b)
        {
            return (m_listMod < r.m_listMod);
        }
        else
        {
            return (a < b);
        }
    }
    else
    {
        return (m_aaLetter < r.m_aaLetter);
    }
}

bool
Aa::isAaEqual(Aa const &r) const
{

    return (std::tie(m_aaLetter, m_listMod) ==
            std::tie(r.m_aaLetter, r.m_listMod));
}

bool
operator==(Aa const &l, Aa const &r)
{
    return l.isAaEqual(r);
}

bool
operator<(Aa const &l, Aa const &r)
{
    return l.isLesser(r);
}
} /* namespace pappso */
