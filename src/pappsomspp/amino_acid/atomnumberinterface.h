/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../types.h"

namespace pappso
{
class AtomNumberInterface
{
  public:
  /** \brief get the number of atom C, O, N, H in the molecule
   */
  virtual int getNumberOfAtom(AtomIsotopeSurvey atom) const = 0;

  /** \brief get the number of isotopes C13, H2, O17, O18, N15, S33, S34, S36 in
   * the molecule
   */
  virtual int getNumberOfIsotope(Isotope isotope) const = 0;
};

} // namespace pappso
