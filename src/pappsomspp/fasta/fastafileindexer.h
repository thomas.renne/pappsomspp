/**
 * \file pappsomspp/fasta/fastafileindexer.h
 * \date 22/06/2109
 * \author Olivier Langella
 * \brief Quick random access to sequences in a fasta file using an index
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <QFileInfo>
#include <QTextStream>
#include "fastahandlerinterface.h"

namespace pappso
{
class FastaFileIndexerInterface
{

  public:
  virtual void open()                                = 0;
  virtual void getSequenceByIndex(FastaHandlerInterface &fasta_handler,
                                  std::size_t index) = 0;

  virtual void close() = 0;
};

class FastaFileIndexer : public FastaFileIndexerInterface
{

  public:
  FastaFileIndexer(const QFileInfo &fastaFile);
  FastaFileIndexer(const FastaFileIndexer &other);
  virtual ~FastaFileIndexer();

  void getSequenceByIndex(FastaHandlerInterface &fasta_handler,
                          std::size_t index) override;
  void open() override;
  void close() override;

  private:
  void parseFastaFile();

  private:
  QFile m_fasta_file;
  std::vector<qint64> m_indexArray;
  QTextStream *mpa_sequenceTxtIn = nullptr;
};
} // namespace pappso
