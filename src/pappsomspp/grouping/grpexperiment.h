
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <map>
#include <QString>
#include <QDebug>
#include "../types.h"
#include "grpgroupingmonitor.h"
#include "grppeptideset.h"
#include "grpmappeptidetogroup.h"
#include "grpgroup.h"
#include "grpsubgroup.h"

namespace pappso
{


class GrpExperiment
{
  private:
  GrpGroupingMonitorInterface *mp_monitor = nullptr;
  bool m_isRemoveNonInformativeSubgroups  = true;
  std::map<QString, GrpProteinSp> m_mapProteins;
  std::map<QString, std::map<unsigned long, GrpPeptideSp>> m_mapPeptides;

  std::list<GrpPeptideSp> m_grpPeptideList;
  std::list<GrpProteinSp> m_grpProteinList;
  bool m_isGroupingStarted = false;
  std::list<GrpProtein *> m_remainingGrpProteinList;

  GrpPeptideSet m_grpPostGroupingProteinListRemoval;

  GrpPeptideSet m_grpPreGroupingProteinListRemoval;

  std::list<GrpGroupSp> m_grpGroupSpList;

  void addSubGroupSp(GrpMapPeptideToGroup &grp_map_peptide_to_group,
                     GrpSubGroupSp &grpSubGroupSp) const;
  void numbering();
  void removeNonInformativeSubGroups();

  public:
  GrpProteinSp &getGrpProteinSp(const QString &acc, const QString &description);
  GrpPeptideSp &setGrpPeptide(const GrpProteinSp &proteinSp,
                              const QString &sequence,
                              pappso_double mass);

  GrpExperiment(GrpGroupingMonitorInterface *p_monitor);
  ~GrpExperiment();

  void startGrouping();

  std::vector<GrpProteinSpConst> getGrpProteinSpList() const;

  void setRemoveNonInformativeSubgroups(bool ok);

  /** @brief protein to remove with its entire group after grouping is completed
   * typically : to use with protein contaminants if you want to ignore any
   * group containing one contaminant protein
   */
  void addPostGroupingGrpProteinSpRemoval(GrpProteinSp sp_protein);

  /** @brief protein peptides to remove before grouping
   * typically : remove protein contaminants in special metaproteomics cases
   */
  void addPreGroupingGrpProteinSpRemoval(GrpProteinSp sp_protein);

  std::vector<GrpGroupSpConst> getGrpGroupSpList() const;
};


} // namespace pappso
