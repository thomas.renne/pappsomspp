
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include <list>
#include "grpsubgroup.h"
#include "grpmappeptidetosubgroupset.h"

namespace pappso
{

class GrpExperiment;

class GrpGroup;
typedef std::shared_ptr<GrpGroup> GrpGroupSp;
typedef std::shared_ptr<const GrpGroup> GrpGroupSpConst;


class GrpGroup
{
  friend class GrpExperiment;

  private:
  std::list<GrpSubGroupSp> m_subGroupList;
  unsigned int m_groupNumber = 0;
  GrpPeptideSet m_peptideSet;

  GrpMapPeptideToSubGroupSet m_mapPeptideToSubGroupSet;

  protected:
  const std::list<GrpSubGroupSp> &getSubGroupSpList() const;
  GrpGroup(GrpSubGroupSp &grpSubGroupSp);
  GrpGroupSp makeGrpGroupSp();
  bool removeFirstNonInformativeSubGroup();
  void check() const;


  public:
  GrpGroup(const GrpGroup &grpGroupSp);
  ~GrpGroup();
  bool operator<(const GrpGroup &other) const;
  bool containsAny(const GrpPeptideSet &peptideSet) const;
  void addSubGroupSp(const GrpSubGroupSp &grpSubGroupSp);
  void addGroup(GrpGroup *p_group_to_add);
  void numbering();
  void setGroupNumber(unsigned int i);
  bool removeNonInformativeSubGroups();
  const QString getGroupingId() const;
  unsigned int getGroupNumber() const;
  const GrpPeptideSet &getGrpPeptideSet() const;
  std::vector<GrpSubGroupSpConst> getGrpSubGroupSpList() const;
};


} // namespace pappso
