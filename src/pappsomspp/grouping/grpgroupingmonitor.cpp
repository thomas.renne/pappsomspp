
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include <QTextStream>
#include <iostream>
#include "grpgroupingmonitor.h"

namespace pappso
{
GrpGroupingMonitor::GrpGroupingMonitor()
{
  mp_outStream = new QTextStream(stderr, QIODevice::WriteOnly);
}

GrpGroupingMonitor::~GrpGroupingMonitor()
{

  mp_outStream->flush();
  delete mp_outStream;
}

void
GrpGroupingMonitor::startGrouping(std::size_t total_number_protein,
                                  std::size_t total_number_peptide)
{
  m_totalNumberProtein = total_number_protein;
  m_totalNumberPeptide = total_number_peptide;
  m_currentProtein     = 0;
  (*mp_outStream) << "start grouping " << m_totalNumberProtein << " proteins "
                  << total_number_peptide << " peptides\n";
  mp_outStream->flush();
}
void
GrpGroupingMonitor::groupingProtein()
{
  m_currentProtein++;
  (*mp_outStream) << "grouping protein " << m_currentProtein << " on "
                  << m_totalNumberProtein << "\n";
  mp_outStream->flush();
}
void
GrpGroupingMonitor::startRemovingNonInformativeSubGroupsInAllGroups(
  std::size_t total_number_group)
{
  (*mp_outStream) << "removing non informative subgroups in all groups ("
                  << total_number_group << ")\n";
  mp_outStream->flush();
}
void
GrpGroupingMonitor::stopRemovingNonInformativeSubGroupsInAllGroups(
  std::size_t total_number_group)
{
  (*mp_outStream) << "removing non informative subgroups finished, remaining "
                  << total_number_group << " groups\n";
  mp_outStream->flush();
}
void
GrpGroupingMonitor::removingNonInformativeSubGroupsInGroup()
{
  (*mp_outStream) << "removing non informative on a single group\n";
  mp_outStream->flush();
}
void
GrpGroupingMonitor::startNumberingAllGroups(std::size_t total_number_group)
{
  (*mp_outStream) << "numbering " << total_number_group << " groups\n";
  mp_outStream->flush();
}
void
GrpGroupingMonitor::stopGrouping()
{
  (*mp_outStream) << "grouping finished\n";
  mp_outStream->flush();
}

} // namespace pappso
