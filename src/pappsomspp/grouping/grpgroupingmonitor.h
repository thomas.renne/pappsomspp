/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <QTextStream>

namespace pappso
{

class GrpGroupingMonitorInterface
{
  public:
  virtual void startGrouping(std::size_t total_number_protein,
                             std::size_t total_number_peptide) = 0;
  virtual void groupingProtein()                               = 0;
  virtual void startRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group) = 0;
  virtual void stopRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group)                                    = 0;
  virtual void removingNonInformativeSubGroupsInGroup()                = 0;
  virtual void startNumberingAllGroups(std::size_t total_number_group) = 0;
  virtual void stopGrouping()                                          = 0;
};

class GrpGroupingMonitor : public GrpGroupingMonitorInterface
{
  private:
  QTextStream *mp_outStream;
  std::size_t m_totalNumberProtein;
  std::size_t m_totalNumberPeptide;
  std::size_t m_currentProtein;

  public:
  GrpGroupingMonitor();
  ~GrpGroupingMonitor();
  virtual void startGrouping(std::size_t total_number_protein,
                             std::size_t total_number_peptide);
  virtual void groupingProtein();
  virtual void startRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group);
  virtual void stopRemovingNonInformativeSubGroupsInAllGroups(
    std::size_t total_number_group);
  virtual void removingNonInformativeSubGroupsInGroup();
  virtual void startNumberingAllGroups(std::size_t total_number_group);
  virtual void stopGrouping();
};

} // namespace pappso
