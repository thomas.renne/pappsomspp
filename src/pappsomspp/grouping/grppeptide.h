
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include <QString>
#include <memory>
#include "../types.h"

namespace pappso
{
class GrpPeptide;


typedef std::shared_ptr<const GrpPeptide> GrpPeptideSpConst;
typedef std::shared_ptr<GrpPeptide> GrpPeptideSp;

class GrpPeptide
{
  friend class GrpExperiment;

  private:
  unsigned int m_groupNumber = 0;
  unsigned int m_rank        = 0;

  const QString m_sequence;
  const pappso_double m_mass;

  protected:
  GrpPeptide(QString sequence, pappso_double mass);

  public:
  ~GrpPeptide();
  const QString &getSequence() const;

  /** \brief sort grp peptides between each other
   * sorts by peptide LI sequence and mass
   */
  bool operator<(const GrpPeptide &other) const;

  void setRank(unsigned int i);
  void setGroupNumber(unsigned int i);

  const QString getGroupingId() const;
  unsigned int getGroupNumber() const;
  unsigned int getRank() const;
};


} // namespace pappso
