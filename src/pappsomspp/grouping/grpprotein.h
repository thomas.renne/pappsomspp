
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef GRPPROTEIN_H
#define GRPPROTEIN_H

#include <memory>
#include <vector>
#include <QString>

#include "grppeptide.h"

namespace pappso
{
class GrpProtein;
typedef std::shared_ptr<const GrpProtein> GrpProteinSpConst;
typedef std::shared_ptr<GrpProtein> GrpProteinSp;

class GrpProtein
{
  friend class GrpExperiment;

  private:
  unsigned int m_groupNumber    = 0;
  unsigned int m_subGroupNumber = 0;
  unsigned int m_rank           = 0;
  unsigned int m_count          = 0;

  const QString m_accession;
  const QString m_description;

  protected:
  GrpProtein(const QString &accession, const QString &description);

  void countPlus();

  /** @brief ensure that each peptide in peptide list is unique and sorted by
   * pointer adress
   */
  void strip();


  std::vector<GrpPeptide *> m_grpPeptidePtrList;

  public:
  GrpProtein(const GrpProtein &other);
  ~GrpProtein();
  void push_back(GrpPeptide *p_grpPeptide);
  bool operator==(const GrpProtein &other) const;

  const QString &getAccession() const;
  const QString &getDescription() const;
  const QString getGroupingId() const;

  typedef std::vector<GrpPeptide *>::const_iterator const_iterator;
  const_iterator
  begin() const
  {
    return m_grpPeptidePtrList.begin();
  }
  const_iterator
  end() const
  {
    return m_grpPeptidePtrList.end();
  }
  void setRank(unsigned int i);
  void setGroupNumber(unsigned int i);
  void setSubGroupNumber(unsigned int i);
  unsigned int getGroupNumber() const;
  unsigned int getSubGroupNumber() const;
  unsigned int getRank() const;
  unsigned int getCount() const;
};
} // namespace pappso
#endif // GRPPROTEIN_H
