/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "grpsubgroup.h"

namespace pappso
{
class GrpGroup;

class GrpSubGroupSet
{
  friend GrpGroup;

  private:
  std::list<GrpSubGroup *> m_grpSubGroupPtrList;

  public:
  GrpSubGroupSet();
  GrpSubGroupSet(const GrpSubGroupSet &other);
  ~GrpSubGroupSet();
  unsigned int
  size() const
  {
    return m_grpSubGroupPtrList.size();
  };
  void addAll(const GrpSubGroupSet &other);
  void remove(GrpSubGroup *p_remove_sub_group);
  void add(GrpSubGroup *p_add_sub_group);
  std::_List_iterator<GrpSubGroup *>
  erase(std::_List_iterator<GrpSubGroup *> it)
  {
    return m_grpSubGroupPtrList.erase(it);
  };
  std::list<GrpSubGroup *>::const_iterator
  begin() const
  {
    return m_grpSubGroupPtrList.begin();
  };
  std::list<GrpSubGroup *>::const_iterator
  end() const
  {
    return m_grpSubGroupPtrList.end();
  };
  bool contains(GrpSubGroup *get) const;

  const QString printInfos() const;
};


} // namespace pappso
