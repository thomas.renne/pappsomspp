/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <QString>

#include "../msrun/msrunid.h"


namespace pappso
{

class MassSpectrumId
{
  private:
  MsRunIdCstSPtr mcsp_msRunId = nullptr;

  // This is the native id string that is stored in the mzML file. Its structure
  // is dependent on the vendor/format of the original data. In some cases, it
  // is possible to ask libpwiz to extract from it the scan number of the
  // spectrum. For water/mdsciex files, this is not possible. We thus need to
  // rely on the index of the spectrum in the spectrum list for the ms run of
  // interest. By combining the run id and the spectrum index, it is possible to
  // unambiguously identify a mass spectrum from a given file.
  QString m_nativeId;

  // The index of the mass spectrum in the spectrum list of a given ms run is
  // used when the scan number is not easily usable (see the native id comment
  // above).
  std::size_t m_spectrumIndex = std::numeric_limits<std::size_t>::max();

  public:
  MassSpectrumId();

  MassSpectrumId(const MsRunIdCstSPtr &msrun_id);

  MassSpectrumId(const MsRunIdCstSPtr &msrun_id, std::size_t spectrum_index);

  MassSpectrumId(const MassSpectrumId &other);
  ~MassSpectrumId();

  MassSpectrumId &operator=(const MassSpectrumId &other);

  void setMsRunId(MsRunIdCstSPtr other);
  const MsRunIdCstSPtr &getMsRunIdCstSPtr() const;

  void setNativeId(const QString &native_id);
  const QString &getNativeId() const;

  void setSpectrumIndex(std::size_t index);
  std::size_t getSpectrumIndex() const;

  bool operator==(const MassSpectrumId &other) const;

  bool isValid() const;

  QString toString() const;
};


} // namespace pappso
