/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <olivier.langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

/////////////////////// StdLib includes
#include <math.h>


/////////////////////// Qt includes
#include <QDebug>


/////////////////////// Local includes
#include "qualifiedmassspectrum.h"
#include "../utils.h"
#include "../pappsoexception.h"


namespace pappso
{

//! Construct an uninitialized QualifiedMassSpectrum.
QualifiedMassSpectrum::QualifiedMassSpectrum()
{
}


//! Construct a QualifiedMassSpectrum using a MassSpectrumId;
QualifiedMassSpectrum::QualifiedMassSpectrum(const MassSpectrumId &id)
  : m_massSpectrumId(id)
{
}


QualifiedMassSpectrum::QualifiedMassSpectrum(
  MassSpectrumSPtr mass_spectrum_SPtr)
  : msp_massSpectrum{mass_spectrum_SPtr}
{
}


//! Construct a QualifiedMassSpectrum as a copy of \p other.
QualifiedMassSpectrum::QualifiedMassSpectrum(const QualifiedMassSpectrum &other)
  : msp_massSpectrum(other.msp_massSpectrum),
    m_massSpectrumId(other.m_massSpectrumId),
    m_isEmptyMassSpectrum(other.m_isEmptyMassSpectrum),
    m_msLevel(other.m_msLevel),
    m_rt(other.m_rt),
    m_dt(other.m_dt),
    m_precursorSpectrumIndex(other.m_precursorSpectrumIndex),
    m_precursorNativeId(other.m_precursorNativeId),
    m_precursorMz(other.m_precursorMz),
    m_precursorCharge(other.m_precursorCharge),
    m_precursorIntensity(other.m_precursorIntensity),
    m_params(other.m_params)
{
  // qDebug();
}

//! Destruct this QualifiedMassSpectrum.
QualifiedMassSpectrum::~QualifiedMassSpectrum()
{
  // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << " ()";
}


QualifiedMassSpectrumSPtr
QualifiedMassSpectrum::makeQualifiedMassSpectrumSPtr() const
{
  return std::make_shared<QualifiedMassSpectrum>(*this);
}


QualifiedMassSpectrumCstSPtr
QualifiedMassSpectrum::makeQualifiedMassSpectrumCstSPtr() const
{
  return std::make_shared<const QualifiedMassSpectrum>(*this);
}


//! Set the MassSpectrumId.
void
QualifiedMassSpectrum::setMassSpectrumId(const MassSpectrumId &iD)
{
  m_massSpectrumId = iD;
}


//! Get the MassSpectrumId.
const MassSpectrumId &
QualifiedMassSpectrum::getMassSpectrumId() const
{
  return m_massSpectrumId;
}


//! Set the MassSpectrumSPtr.
void
QualifiedMassSpectrum::setMassSpectrumSPtr(MassSpectrumSPtr massSpectrum)
{
  msp_massSpectrum = massSpectrum;
}


//! Get the MassSpectrumSPtr.
MassSpectrumSPtr
QualifiedMassSpectrum::getMassSpectrumSPtr() const
{
  return msp_massSpectrum;
}


//! Get the MassSpectrumCstSPtr.
MassSpectrumCstSPtr
QualifiedMassSpectrum::getMassSpectrumCstSPtr() const
{
  return msp_massSpectrum;
}


void
QualifiedMassSpectrum::setEmptyMassSpectrum(bool is_empty_mass_spectrum)
{
  m_isEmptyMassSpectrum = is_empty_mass_spectrum;
}


bool
QualifiedMassSpectrum::isEmptyMassSpectrum() const
{
  return m_isEmptyMassSpectrum;
}


//! Set the precursor charge.
void
QualifiedMassSpectrum::setPrecursorCharge(unsigned int precursor_charge)
{
  m_precursorCharge = precursor_charge;
}


//! Get the precursor charge.
unsigned int
QualifiedMassSpectrum::getPrecursorCharge() const
{
  return m_precursorCharge;
}


//! Set the precursor m/z ratio.
void
QualifiedMassSpectrum::setPrecursorMz(pappso_double precursor_mz)
{
  m_precursorMz = precursor_mz;
}


//! Get the precursor m/z ratio.
pappso_double
QualifiedMassSpectrum::getPrecursorMz() const
{
  return m_precursorMz;
}


//! Set the mass spectrum level.
void
QualifiedMassSpectrum::setMsLevel(unsigned int level)
{
  m_msLevel = level;
}


//! Get the mass spectrum level.
unsigned int
QualifiedMassSpectrum::getMsLevel() const
{
  return m_msLevel;
}


//! Set the retention time in seconds.
void
QualifiedMassSpectrum::setRtInSeconds(pappso_double rt_in_seconds)
{
  m_rt = rt_in_seconds;
}


//! Get the retention time in seconds.
pappso_double
QualifiedMassSpectrum::getRtInSeconds() const
{
  return m_rt;
}


//! Get the retention time in minutes.
pappso_double
QualifiedMassSpectrum::getRtInMinutes() const
{
  return m_rt / 60;
}


//! Set the drift time in milliseconds.
void
QualifiedMassSpectrum::setDtInMilliSeconds(pappso_double dt_in_milli_seconds)
{
  if(isinf(dt_in_milli_seconds))
    m_dt = -1;
  else
    m_dt = dt_in_milli_seconds;
}


//! Get the drift time in milliseconds.
pappso_double
QualifiedMassSpectrum::getDtInMilliSeconds() const
{
  return m_dt;
}


//! Set the scan number of the precursor ion.
void
QualifiedMassSpectrum::setPrecursorSpectrumIndex(
  std::size_t precursor_spectrum_index)
{
  m_precursorSpectrumIndex = precursor_spectrum_index;
}

//! Get the scan number of the precursor ion.
std::size_t
QualifiedMassSpectrum::getPrecursorSpectrumIndex() const
{
  return m_precursorSpectrumIndex;
}

//! Set the scan native id of the precursor ion.
void
QualifiedMassSpectrum::setPrecursorNativeId(const QString &native_id)
{
  m_precursorNativeId = native_id;
}

const QString &
QualifiedMassSpectrum::getPrecursorNativeId() const
{
  return m_precursorNativeId;
}

//! Set the intensity of the precursor ion.
void
QualifiedMassSpectrum::setPrecursorIntensity(pappso_double intensity)
{
  m_precursorIntensity = intensity;
}


//! Get the intensity of the precursor ion.
pappso_double
QualifiedMassSpectrum::getPrecursorIntensity() const
{
  return m_precursorIntensity;
}


std::size_t
QualifiedMassSpectrum::size() const
{
  if(msp_massSpectrum == nullptr)
    {
      throw pappso::PappsoException(QObject::tr("msp_massSpectrum == nullptr"));
    }
  return msp_massSpectrum.get()->size();
}


QString
QualifiedMassSpectrum::toString() const
{
  QString text;

  if(msp_massSpectrum != nullptr && msp_massSpectrum.get() != nullptr)
    {
      QString pointer_string =
        QString("msp_massSpectrum.get(): %1 ")
          .arg(Utils::pointerToString((void *)msp_massSpectrum.get()));

      text += pointer_string;
    }
  else
    text += QString("msp_massSpectrum is nullptr ");

  //qDebug() << text;

  text +=
    QString(
      "; m_massSpectrumId : %1 \n"
      "m_msLevel: %2 ; m_rt (min): %3 ; m_dt (ms): %4 ; prec. spec. "
      "index: %5 ; prec. mz: %6 ; prec. z: %7 ; prec. int.: %8")
      .arg(m_massSpectrumId.toString())
      .arg(m_msLevel)
      .arg(getRtInMinutes(), 0, 'f', 2)
      .arg(m_dt, 0, 'f', 5)
      .arg(m_precursorSpectrumIndex != std::numeric_limits<std::size_t>::max()
             ? m_precursorSpectrumIndex
             : -1)
      .arg(m_precursorMz != std::numeric_limits<double>::max() ? m_precursorMz
                                                               : -1)
      .arg(m_precursorCharge != std::numeric_limits<unsigned int>::max()
             ? m_precursorCharge
             : -1)
      .arg(m_precursorIntensity != std::numeric_limits<double>::max()
             ? m_precursorIntensity
             : -1);

  return text;
}


void
QualifiedMassSpectrum::setParameterValue(
  QualifiedMassSpectrumParameter parameter, const QVariant &value)
{

  auto ret = m_params.insert(
    std::pair<QualifiedMassSpectrumParameter, QVariant>(parameter, value));

  if(ret.second == false)
    {
      ret.first->second = value;
    }
}
const QVariant
QualifiedMassSpectrum::getParameterValue(
  QualifiedMassSpectrumParameter parameter) const
{
  auto it = m_params.find(parameter);
  if(it == m_params.end())
    {
      return QVariant();
    }
  else
    {
      return it->second;
    }
}

} // namespace pappso
