
#pragma once

#include <QString>
#include <QMetaType>


#include "../types.h"
#include "../msrun/msrunreader.h"
#include "../msrun/msrunid.h"


namespace pappso
{

class TimsMsRunReaderMs2;
typedef std::shared_ptr<TimsMsRunReaderMs2> TimsMsRunReaderMs2SPtr;

enum class FileReaderType
{
  pwiz,
  xy,
  tims,
  tims_ms2,
};

// This class is used to access mass spectrometry data files. The file being
// opened and read might contain more than one MS run. The user of this class
// might request a vector of all these MS runs (in the form of a vector of
// MsRunIdCstSPtr. Once the MsRunIdCstSPtr of interest has been located by the
// caller, the caller might then request the MsRunReaderSPtr to use to read that
// MS run's data.
class MsFileAccessor
{
  public:
  MsFileAccessor(const QString &file_name, const QString &xml_prefix);
  MsFileAccessor(const MsFileAccessor &other);
  virtual ~MsFileAccessor();

  const QString &getFileName() const;

  /** @brief get the raw format of mz data
   */
  MzFormat getFileFormat() const;

  /** @brief given an mz format, explicitly set the prefered reader
   */
  void setPreferedFileReaderType(MzFormat format, FileReaderType reader_type);

  std::vector<MsRunIdCstSPtr> getMsRunIds();

  MsRunReaderSPtr msRunReaderSp(MsRunIdCstSPtr ms_run_id);

  /** @brief get an msrun reader by finding the run_id in file
   *
   * @param run_id identifier within file of the MSrun
   * @param xml_id XML identifier given by the user to identify this MSrun in
   * our experiment (not in the file)
   */
  MsRunReaderSPtr getMsRunReaderSPtrByRunId(const QString &run_id,
                                            const QString &xml_id);

  /** @brief get an MsRunReader directly from a valid MsRun ID
   *
   * no need to check the file format or filename : all is already part of the
   * msrunid
   */
  static MsRunReaderSPtr buildMsRunReaderSPtr(MsRunIdCstSPtr ms_run_id);

  /** @brief if possible, builds directly a dedicated Tims TOF tdf file reader
   */
  TimsMsRunReaderMs2SPtr buildTimsMsRunReaderMs2SPtr();

  private:
  QString m_fileName;

  // When opening more than one file concurrently in a determinate session, we
  // need this prefix to craft unabiguous ms run ids.
  const QString m_xmlPrefix;

  MzFormat m_fileFormat = MzFormat::unknown;

  // Type of the file reader that could load the file.
  FileReaderType m_fileReaderType;

  std::map<MzFormat, FileReaderType> m_preferedFileReaderTypeMap;
};

} // namespace pappso
