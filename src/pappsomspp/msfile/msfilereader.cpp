#include <pwiz/data/msdata/DefaultReaderList.hpp>

#include <QDebug>
#include <QFile>
#include <QFileInfo>


#include "msfilereader.h"

#include "../exception/exceptionnotfound.h"
#include "../utils.h"
#include "../msrun/msrunid.h"


namespace pappso
{


MsFileReader::MsFileReader(const QString &file_name) : m_fileName{file_name}
{
  QFile file(file_name);
  if(!file.exists())
    throw(ExceptionNotFound(QObject::tr("File %1 not found.")
                              .arg(QFileInfo(file_name).absoluteFilePath())));
}


MsFileReader::~MsFileReader()
{
}


} // namespace pappso
