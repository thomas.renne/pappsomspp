
#pragma once

#include <QString>

#include "../msrun/msrunreader.h"
#include "../msrun/msrunid.h"


namespace pappso
{


class MsFileReader
{
  protected:
  QString m_fileName;
  MzFormat m_fileFormat = MzFormat::unknown;

  public:
  MsFileReader(const QString &file_name);
  virtual ~MsFileReader();

  virtual MzFormat getFileFormat() = 0;

  virtual std::vector<MsRunIdCstSPtr>
  getMsRunIds(const QString &run_prefix) = 0;
};

} // namespace pappso
