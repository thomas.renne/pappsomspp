
#pragma once

#include <QString>

#include <pwiz/data/msdata/MSData.hpp>

#include "../types.h"
#include "msfilereader.h"
#include "../msrun/msrunid.h"


namespace pappso
{


class PwizMsFileReader : MsFileReader
{
  private:
  std::vector<pwiz::msdata::MSDataPtr> m_msDataPtrVector;

  virtual std::size_t initialize();

  public:
  PwizMsFileReader(const QString &file_name);
  virtual ~PwizMsFileReader();

  virtual MzFormat getFileFormat() override;

  virtual std::vector<MsRunIdCstSPtr>
  getMsRunIds(const QString &run_prefix) override;
};

} // namespace pappso
