
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "../msrunid.h"
#include "../msrunreader.h"
#include <vector>
#include "../../processing/filters/filtermorpho.h"

namespace pappso
{

struct RtPoint
{
  double retentionTime;
  std::size_t spectrumIndex;
};


template <class T>
struct MsRunRetentionTimeSeamarkPoint
{
  T entityHash;
  double retentionTime;
  double intensity;
};


template <class T>
class MsRunRetentionTime
{
  private:
  struct PeptideMs2Point
  {
    double retentionTime;
    double precursorIntensity;
    T entityHash;
  };

  enum class ComputeRetentionTimeReference
  {
    maximum_intensity,
    weighted_intensity,
    last
  };

  public:
  MsRunRetentionTime(MsRunReaderSPtr msrun_reader_sp);
  MsRunRetentionTime(const MsRunRetentionTime<T> &other);
  ~MsRunRetentionTime();

  void setMs2MedianFilter(const FilterMorphoMedian &ms2MedianFilter);
  void setMs2MeanFilter(const FilterMorphoMean &ms2MeanFilter);
  void setMs1MeanFilter(const FilterMorphoMean &ms1MeanFilter);

  Trace getCommonDeltaRt(
    const std::vector<MsRunRetentionTimeSeamarkPoint<T>> &other_seamarks) const;

  void addPeptideAsSeamark(const T &peptide_str,
                           std::size_t ms2_spectrum_index);

  void computePeptideRetentionTimes();

  std::size_t getNumberOfCorrectedValues() const;

  Trace align(const MsRunRetentionTime<T> &msrun_retention_time_reference);

  const std::vector<MsRunRetentionTimeSeamarkPoint<T>> &getSeamarks() const;
  const std::vector<double> &getAlignedRetentionTimeVector() const;
  const std::vector<RtPoint> &getMs1RetentionTimeVector() const;

  bool isAligned() const;

  double
  translateOriginal2AlignedRetentionTime(double original_retention_time) const;

  protected:
  double getFrontRetentionTimeReference() const;
  double getBackRetentionTimeReference() const;
  const std::vector<MsRunRetentionTimeSeamarkPoint<T>>
  getSeamarksReferences() const;

  private:
  void getCommonDeltaRt(
    Trace &delta_rt,
    const std::vector<MsRunRetentionTimeSeamarkPoint<T>> &other_seamarks) const;
  void correctNewTimeValues(Trace &ms1_aligned_points,
                            double correction_parameter);

  void linearRegressionMs2toMs1(Trace &ms1_aligned_points,
                                const Trace &common_points);

  private:
  FilterMorphoMedian m_ms2MedianFilter;
  FilterMorphoMean m_ms2MeanFilter;
  FilterMorphoMean m_ms1MeanFilter;
  pappso::MsRunReaderSPtr msp_msrunReader;
  pappso::MsRunIdCstSPtr mcsp_msrunId;
  std::vector<RtPoint> m_ms1RetentionTimeVector;
  std::vector<double> m_alignedRetentionTimeVector;

  std::vector<MsRunRetentionTimeSeamarkPoint<T>> m_seamarks;
  std::size_t m_valuesCorrected = 0;

  std::vector<PeptideMs2Point> m_allMs2Points;

  ComputeRetentionTimeReference m_retentionTimeReferenceMethod =
    ComputeRetentionTimeReference::maximum_intensity;
};

} // namespace pappso
