// GPL 3+
// Filippo Rusconi

#include <map>
#include <limits>
#include <iostream>
#include <iomanip>

#include "msrundatasettree.h"

#include "../pappsoexception.h"
#include "../exception/exceptionnotpossible.h"


namespace pappso
{


MsRunDataSetTree::MsRunDataSetTree(MsRunIdCstSPtr ms_run_id_csp)
  : mcsp_msRunId(ms_run_id_csp)
{
}


MsRunDataSetTree::~MsRunDataSetTree()
{
  // qDebug();

  for(auto &&node : m_rootNodes)
    {
      // Each node is responsible for freeing its children nodes!

      delete node;
    }

  m_rootNodes.clear();

  // Beware not to delete the node member of the map, as we have already
  // destroyed them above!
  //
  // for(auto iterator = m_indexNodeMap.begin(); iterator !=
  // m_indexNodeMap.end();
  //++iterator)
  //{
  // delete(iterator->second);
  //}

  // qDebug();
}


void
MsRunDataSetTree::addMassSpectrum(
  QualifiedMassSpectrumCstSPtr mass_spectrum_csp)
{
  // qDebug();

  if(mass_spectrum_csp == nullptr)
    qFatal("Cannot be nullptr");

  if(mass_spectrum_csp.get() == nullptr)
    qFatal("Cannot be nullptr");

  // We need to get the precursor spectrum index, in case this spectrum is a
  // fragmentation index.

  MsRunDataSetTreeNode *new_node_p = nullptr;

  std::size_t precursor_spectrum_index =
    mass_spectrum_csp->getPrecursorSpectrumIndex();

  // qDebug() << "The precursor_spectrum_index:" << precursor_spectrum_index;

  if(precursor_spectrum_index == std::numeric_limits<std::size_t>::max())
    {
      // This spectrum is a full scan spectrum, not a fragmentation spectrum.
      // Create a new node with no parent and push it back to the root nodes
      // vector.

      new_node_p = new MsRunDataSetTreeNode(mass_spectrum_csp, nullptr);

      // Since there is no parent in this overload, it is assumed that the node
      // to be populated with the new node is the root node.

      m_rootNodes.push_back(new_node_p);

      // qDebug() << "to the roots node vector.";
    }
  else
    {
      // This spectrum is a fragmentation spectrum.

      // Sanity check

      if(mass_spectrum_csp->getMsLevel() <= 1)
        {
          throw(ExceptionNotPossible(
            "The MS level needs to be > 1 in a fragmentation spectrum."));
        }

      // Get the node that contains the precursor ion mass spectrum.
      MsRunDataSetTreeNode *parent_node_p = findNode(precursor_spectrum_index);

      if(parent_node_p == nullptr)
        {
          throw(ExceptionNotPossible(
            "Could not find a a tree node matching the index."));
        }

      // qDebug() << "Fragmentation spectrum"
      //<< "Found parent node:" << parent_node_p
      //<< "for precursor index:" << precursor_spectrum_index;

      // At this point, create a new node with the right parent.

      new_node_p = new MsRunDataSetTreeNode(mass_spectrum_csp, parent_node_p);

      parent_node_p->m_children.push_back(new_node_p);
    }

  // And now document that addition in the node index map.
  m_indexNodeMap.insert(std::pair<std::size_t, MsRunDataSetTreeNode *>(
    mass_spectrum_csp->getMassSpectrumId().getSpectrumIndex(), new_node_p));

  ++m_spectrumCount;

  // qDebug() << "New index/node map:"
  //<< mass_spectrum_csp->getMassSpectrumId().getSpectrumIndex() << "/"
  //<< new_node_p;
}


const std::map<std::size_t, MsRunDataSetTreeNode *> &
MsRunDataSetTree::getIndexNodeMap() const
{
  return m_indexNodeMap;
}


std::size_t
MsRunDataSetTree::massSpectrumIndex(const MsRunDataSetTreeNode *node) const
{
  // We have a node and we want to get the matching mass spectrum index.

  if(node == nullptr)
    throw("Cannot be that the node pointer is nullptr");

  std::map<std::size_t, MsRunDataSetTreeNode *>::const_iterator iterator =
    std::find_if(
      m_indexNodeMap.begin(),
      m_indexNodeMap.end(),
      [node](const std::pair<std::size_t, MsRunDataSetTreeNode *> pair) {
        return pair.second == node;
      });

  if(iterator != m_indexNodeMap.end())
    return iterator->first;

  return std::numeric_limits<std::size_t>::max();
}


std::size_t
MsRunDataSetTree::massSpectrumIndex(
  QualifiedMassSpectrumCstSPtr qualified_mass_spectrum_csp) const
{
  MsRunDataSetTreeNode *node_p = findNode(qualified_mass_spectrum_csp);

	return massSpectrumIndex(node_p);
}


const std::vector<MsRunDataSetTreeNode *> &
MsRunDataSetTree::getRootNodes() const
{
  return m_rootNodes;
}


void
MsRunDataSetTree::accept(MsRunDataSetTreeNodeVisitorInterface &visitor)
{
  // qDebug() << "Going to call node->accept(visitor) for each root node.";

  for(auto &&node : m_rootNodes)
    {
      // qDebug() << "Calling accept for root node:" << node;

      if(visitor.shouldStop())
        break;

      node->accept(visitor);
    }
}


void
MsRunDataSetTree::accept(
  MsRunDataSetTreeNodeVisitorInterface &visitor,
  std::vector<MsRunDataSetTreeNode *>::const_iterator nodes_begin_iterator,
  std::vector<MsRunDataSetTreeNode *>::const_iterator nodes_end_iterator)
{
  // qDebug() << "Visitor:" << &visitor << "The distance is between iterators
  // is:"
  //<< std::distance(nodes_begin_iterator, nodes_end_iterator);

  using Iterator = std::vector<MsRunDataSetTreeNode *>::const_iterator;

  Iterator iter = nodes_begin_iterator;

  // Inform the visitor of the number of nodes to work on.

  std::size_t node_count =
    std::distance(nodes_begin_iterator, nodes_end_iterator);

  visitor.setNodesToProcessCount(node_count);

  while(iter != nodes_end_iterator)
    {
      // qDebug() << "Visitor:" << &visitor
      //<< "The distance is between iterators is:"
      //<< std::distance(nodes_begin_iterator, nodes_end_iterator);

      // qDebug() << "Node visited:" << (*iter)->toString();

      if(visitor.shouldStop())
        break;

      (*iter)->accept(visitor);
      ++iter;
    }
}


MsRunDataSetTreeNode *
MsRunDataSetTree::findNode(QualifiedMassSpectrumCstSPtr mass_spectrum_csp) const
{
  // qDebug();

  for(auto &node : m_rootNodes)
    {
      // qDebug() << "In one node of the root nodes.";

      MsRunDataSetTreeNode *iterNode = node->findNode(mass_spectrum_csp);
      if(iterNode != nullptr)
        return iterNode;
    }

  return nullptr;
}


MsRunDataSetTreeNode *
MsRunDataSetTree::findNode(std::size_t spectrum_index) const
{
  // qDebug();

  for(auto &node : m_rootNodes)
    {
      // qDebug() << "In one node of the root nodes.";

      MsRunDataSetTreeNode *iterNode = node->findNode(spectrum_index);
      if(iterNode != nullptr)
        return iterNode;
    }

  return nullptr;
}


std::vector<MsRunDataSetTreeNode *>
MsRunDataSetTree::flattenedView()
{
  // We want to push back all the nodes of the tree in a flat vector of nodes.

  std::vector<MsRunDataSetTreeNode *> nodes;

  for(auto &&node : m_rootNodes)
    {
      // The node will store itself and all of its children.
      node->flattenedView(nodes, true /* with_descendants */);
    }

  return nodes;
}


std::vector<MsRunDataSetTreeNode *>
MsRunDataSetTree::flattenedViewMsLevel(std::size_t ms_level,
                                       bool with_descendants)
{
  std::vector<MsRunDataSetTreeNode *> nodes;

  // Logically, ms_level cannot be 0.

  if(!ms_level)
    {
      qFatal(
        "Fatal error at %s@%d -- %s(). "
        "The MS level cannot be 0."
        "Program aborted.",
        __FILE__,
        __LINE__,
        __FUNCTION__);

      return nodes;
    }

  // The depth of the tree at which we are right at this point is 0, we have not
  // gone into the children yet.

  std::size_t depth = 0;

  // If ms_level is 1, then that means that we want the nodes starting right at
  // the root nodes with or without the descendants.

  // std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
  //<< "ms_level: " << ms_level << " depth: " << depth << std::endl;

  if(ms_level == 1)
    {
      for(auto &&node : m_rootNodes)
        {
          // std::cout << __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__
          //<< " () "
          //<< "Handling one of the root nodes at ms_level = 1."
          //<< std::endl;

          node->flattenedView(nodes, with_descendants);
        }

      return nodes;
    }

  // At this point, we know that we want the descendants of the root nodes since
  // we want ms_level > 1, so we need go to to the children of the root nodes.

  // Let depth to 0, because if we go to the children of the root nodes we will
  // still be at depth 0, that is MS level 1.

  for(auto &node : m_rootNodes)
    {
      // std::cout
      //<< __FILE__ << " @ " << __LINE__ << " " << __FUNCTION__ << " () "
      //<< std::setprecision(15)
      //<< "Requesting a flattened view of the root's child nodes with depth: "
      //<< depth << std::endl;

      node->flattenedViewMsLevelNodes(ms_level, depth, nodes, with_descendants);
    }

  return nodes;
}


MsRunDataSetTreeNode *
MsRunDataSetTree::precursorNodeByProductSpectrumIndex(
  std::size_t product_spectrum_index)
{

  // qDebug();

  // Find the node that holds the mass spectrum that was acquired as the
  // precursor that when fragmented gave a spectrum at spectrum_index;

  // Get the node that contains the product_spectrum_index first.
  MsRunDataSetTreeNode *node = nullptr;
  node                       = findNode(product_spectrum_index);

  // Now get the node that contains the precursor_spectrum_index.

  return findNode(node->mcsp_massSpectrum->getPrecursorSpectrumIndex());
}


std::vector<MsRunDataSetTreeNode *>
MsRunDataSetTree::productNodesByPrecursorSpectrumIndex(
  std::size_t precursor_spectrum_index)
{
  std::vector<MsRunDataSetTreeNode *> nodes;

  // First get the node of the precursor spectrum index.

  MsRunDataSetTreeNode *precursor_node = findNode(precursor_spectrum_index);

  if(precursor_node == nullptr)
    return nodes;

  nodes.assign(precursor_node->m_children.begin(),
               precursor_node->m_children.end());

  return nodes;
}


std::vector<MsRunDataSetTreeNode *>
MsRunDataSetTree::precursorNodesByPrecursorMz(pappso_double mz,
                                              PrecisionPtr precision_ptr)
{

  // Find all the precursor nodes holding a mass spectrum that contained a
  // precursor mz-value.

  if(precision_ptr == nullptr)
    qFatal(
      "Fatal error at %s@%d -- %s(). "
      "precision_ptr cannot be nullptr."
      "Program aborted.",
      __FILE__,
      __LINE__,
      __FUNCTION__);

  std::vector<MsRunDataSetTreeNode *> product_nodes;

  // As a first step, find all the nodes that hold a mass spectrum that was
  // acquired as a fragmentation spectrum of an ion of mz,  that is, search all
  // the product ion nodes for which precursor was mz.

  for(auto &&node : m_rootNodes)
    {
      node->productNodesByPrecursorMz(mz, precision_ptr, product_nodes);
    }

  // Now, for each node found get the precursor node

  std::vector<MsRunDataSetTreeNode *> precursor_nodes;

  for(auto &&node : product_nodes)
    {
      precursor_nodes.push_back(
        findNode(node->mcsp_massSpectrum->getPrecursorSpectrumIndex()));
    }

  return precursor_nodes;
}


void
MsRunDataSetTree::addMassSpectrum(
  QualifiedMassSpectrumCstSPtr mass_spectrum_csp,
  MsRunDataSetTreeNode *parent_p)
{
  // qDebug();

  // We want to add a mass spectrum. Either the parent_p argument is nullptr or
  // not. If it is nullptr, then we just append the mass spectrum to the vector
  // of root nodes. If it is not nullptr, we need to append the mass spectrum to
  // that node.

  MsRunDataSetTreeNode *new_node =
    new MsRunDataSetTreeNode(mass_spectrum_csp, parent_p);

  if(parent_p == nullptr)
    {
      m_rootNodes.push_back(new_node);

      // qDebug() << "Pushed back" << new_node << "to root nodes:" <<
      // &m_rootNodes;
    }
  else
    {
      parent_p->m_children.push_back(new_node);

      // qDebug() << "Pushed back" << new_node << "with parent:" << parent_p;
    }

  ++m_spectrumCount;

  // And now document that addition in the node index map.
  m_indexNodeMap.insert(std::pair<std::size_t, MsRunDataSetTreeNode *>(
    mass_spectrum_csp->getMassSpectrumId().getSpectrumIndex(), new_node));

  // qDebug() << "New index/node map:"
  //<< mass_spectrum_csp->getMassSpectrumId().getSpectrumIndex() << "/"
  //<< new_node;
}


void
MsRunDataSetTree::addMassSpectrum(
  QualifiedMassSpectrumCstSPtr mass_spectrum_csp,
  std::size_t precursor_spectrum_index)
{
  // qDebug();

  // First get the node containing the mass spectrum that was acquired at index
  // precursor_spectrum_index.

  // qDebug() << "Need to find the precursor's mass spectrum node for precursor
  // "
  //"spectrum index:"
  //<< precursor_spectrum_index;

  MsRunDataSetTreeNode *mass_spec_data_node_p =
    findNode(precursor_spectrum_index);

  // qDebug() << "Found node" << mass_spec_data_node_p
  //<< "for precursor index:" << precursor_spectrum_index;

  if(mass_spec_data_node_p == nullptr)
    {
      throw(ExceptionNotPossible(
        "Could not find a a tree node matching the index."));
    }

  // qDebug() << "Calling addMassSpectrum with parent node:"
  //<< mass_spec_data_node_p;

  addMassSpectrum(mass_spectrum_csp, mass_spec_data_node_p);
}


std::size_t
MsRunDataSetTree::depth() const
{
  // We want to know what is the depth of the tree, that is the highest level of
  // MSn, that is, n.

  if(!m_rootNodes.size())
    return 0;

  // qDebug() << "There are" << m_rootNodes.size() << "root nodes";

  // By essence, we are at MS0: only if we have at least one root node do we
  // know we have MS1 data. So we already know that we have at least one child,
  // so start with depth 1.

  std::size_t depth          = 1;
  std::size_t tmp_depth      = 0;
  std::size_t greatest_depth = 0;

  for(auto &node : m_rootNodes)
    {
      tmp_depth = node->depth(depth);

      // qDebug() << "Returned depth:" << tmp_depth;

      if(tmp_depth > greatest_depth)
        greatest_depth = tmp_depth;
    }

  return greatest_depth;
}


std::size_t
MsRunDataSetTree::size() const
{

  std::size_t cumulative_node_count = 0;

  for(auto &node : m_rootNodes)
    {
      node->size(cumulative_node_count);

      // qDebug() << "Returned node_count:" << node_count;
    }

  return cumulative_node_count;
}


std::size_t
MsRunDataSetTree::indexNodeMapSize() const
{
  return m_indexNodeMap.size();
}


std::size_t
MsRunDataSetTree::getSpectrumCount() const
{
  return m_spectrumCount;
}


} // namespace pappso
