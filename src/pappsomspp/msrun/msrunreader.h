/**
 * \file pappsomspp/msrun/msrunreader.h
 * \date 29/05/2018
 * \author Olivier Langella
 * \brief base interface to read MSrun files
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


/////////////////////// StdLib includes
#include <memory>
#include <map>


/////////////////////// Qt includes
#include <QMutex>


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "msrunid.h"
#include "../massspectrum/qualifiedmassspectrum.h"
#include "../msfile/msfilereader.h"


namespace pappso
{

/** @brief interface to collect spectrums from the MsRunReader class
 */
class SpectrumCollectionHandlerInterface
{
  public:
  virtual void
  setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) = 0;

  /** @brief tells if we need the peak list (if we want the binary data) for
   * each spectrum
   */
  virtual bool needPeakList() const = 0;

  /** @brief tells if we need the peak list (if we want the binary data) for
   * each spectrum, given an MS level
   */
  virtual bool needMsLevelPeakList(unsigned int ms_level) const final;

  /** @brief tells if we need the peak list given
   */
  virtual void setNeedMsLevelPeakList(unsigned int ms_level,
                                      bool want_peak_list) final;
  virtual bool shouldStop();
  virtual void loadingEnded();
  virtual void spectrumListHasSize(std::size_t size);


  /** @brief use threads to read a spectrum by batch of batch_size
   * @param is_read_ahead boolean to use threads or not
   */
  virtual void setReadAhead(bool is_read_ahead) final;

  /** @brief tells if we want to read ahead spectrum
   */
  virtual bool isReadAhead() const;

  private:
  bool m_isReadAhead                        = false;
  std::vector<bool> m_needPeakListByMsLevel = {true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true,
                                               true};
};


/** @brief example of interface to count MS levels of all spectrum in an MSrun
 */
class MsRunSimpleStatistics : public SpectrumCollectionHandlerInterface
{
  private:
  std::vector<unsigned long> m_countMsLevelSpectrum;

  public:
  virtual void
  setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) override;
  virtual bool needPeakList() const override;
  virtual void loadingEnded() override;

  unsigned long getMsLevelCount(unsigned int ms_level) const;

  unsigned long getTotalCount() const;
};

/** @brief provides a multimap to find quickly spectrum index from scan number
 */
class MsRunReaderScanNumberMultiMap : public SpectrumCollectionHandlerInterface
{
  private:
  std::multimap<std::size_t, std::size_t> m_mmap_scan2index;

  public:
  MsRunReaderScanNumberMultiMap();
  virtual ~MsRunReaderScanNumberMultiMap();
  virtual void
  setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) override;
  virtual bool needPeakList() const override;

  std::size_t getSpectrumIndexFromScanNumber(std::size_t scan_number) const;
};


class MsRunReader;
typedef std::shared_ptr<MsRunReader> MsRunReaderSPtr;
typedef std::shared_ptr<const MsRunReader> MsRunReaderCstSPtr;

/** @brief base class to read MSrun
 * the only way to build a MsRunReader object is to use the MsRunReaderFactory
 */
class MsRunReader
{

  friend class MsFileAccessor;

  public:
  MsRunReader(MsRunIdCstSPtr &ms_run_id);
  MsRunReader(const MsRunReader &other);
  virtual ~MsRunReader();

  const MsRunIdCstSPtr &getMsRunId() const;

  /** @brief get a MassSpectrumSPtr class given its spectrum index
   */
  virtual MassSpectrumSPtr massSpectrumSPtr(std::size_t spectrum_index) = 0;
  virtual MassSpectrumCstSPtr
  massSpectrumCstSPtr(std::size_t spectrum_index) = 0;

  /** @brief get a QualifiedMassSpectrum class given its scan number
   */
  virtual QualifiedMassSpectrum
  qualifiedMassSpectrum(std::size_t spectrum_index,
                        bool want_binary_data = true) const = 0;

  /** @brief get the totat number of spectrum conained in the MSrun data file
   */
  virtual std::size_t spectrumListSize() const = 0;

  /** @brief function to visit an MsRunReader and get each Spectrum in a
   * spectrum collection handler
   */
  virtual void
  readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) = 0;

  /** @brief if possible, converts a scan number into a spectrum index
   * This is a convenient function to help transition from the old scan number
   * (not implemented by all vendors) to more secure spectrum index (not vendor
   * dependant).
   * It is better to not rely on this function.
   */
  virtual std::size_t scanNumber2SpectrumIndex(std::size_t scan_number);

  /** @brief tells if spectra can be accessed using scan numbers
   * by default, it returns false. Only overrided functions can check if scan
   * numbers are available in the current file
   */
  virtual bool hasScanNumbers() const;


  protected:
  MsRunIdCstSPtr mcsp_msRunId;
  MsRunReaderScanNumberMultiMap *mpa_multiMapScanNumber = nullptr;

  virtual void initialize() = 0;

  /** @brief tells if the reader is able to handle this file
   * must be implemented by private MS run reader, specific of one or more file
   * format
   */
  virtual bool accept(const QString &file_name) const = 0;
};


} // namespace pappso

Q_DECLARE_METATYPE(pappso::MsRunReaderSPtr);
extern int msRunReaderSPtrMetaTypeId;
