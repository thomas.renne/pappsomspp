/**
 * \file pappsomspp/msrun/private/pwizmsrunreader.h
 * \date 29/05/2018
 * \author Olivier Langella
 * \brief MSrun file reader base on proteowizard library
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once

#include <pwiz/data/msdata/MSData.hpp>
#include <pwiz/data/msdata/MSDataFile.hpp>

#include "../../types.h"
#include "../../msfile/msfileaccessor.h"
#include "../../msfile/msfilereader.h"


namespace pappso
{

class PwizMsRunReader : public MsRunReader
{
  friend class MsFileAccessor;

  public:
  PwizMsRunReader(MsRunIdCstSPtr &msrun_id_csp);
  virtual ~PwizMsRunReader();

  virtual MassSpectrumSPtr
  massSpectrumSPtr(std::size_t spectrum_index) override;
  virtual MassSpectrumCstSPtr
  massSpectrumCstSPtr(std::size_t spectrum_index) override;

  virtual QualifiedMassSpectrum
  qualifiedMassSpectrum(std::size_t spectrum_index,
                        bool want_binary_data = true) const override;

  virtual void
  readSpectrumCollection(SpectrumCollectionHandlerInterface &handler) override;

  virtual std::size_t spectrumListSize() const override;

  virtual bool hasScanNumbers() const override;

  protected:
  pwiz::msdata::MSDataPtr msp_msData = nullptr;

  virtual void initialize() override;
  virtual bool accept(const QString &file_name) const override;

  QualifiedMassSpectrum qualifiedMassSpectrumFromPwizMSData(
    std::size_t spectrum_index, bool want_binary_data, bool &ok) const;

  QualifiedMassSpectrum
  qualifiedMassSpectrumFromPwizSpectrumPtr(const MassSpectrumId &massSpectrumId,
                                           pwiz::msdata::Spectrum *spectrum_p,
                                           bool want_binary_data,
                                           bool &ok) const;

  pwiz::msdata::SpectrumPtr
  getPwizSpectrumPtr(pwiz::msdata::SpectrumList *p_spectrum_list,
                     std::size_t spectrum_index,
                     bool want_binary_data) const;

  private:
  bool m_hasScanNumbers = false;
};

} // namespace pappso


// Q_DECLARE_METATYPE(pappso::PwizMsRunReader);
// extern int pwizMsRunReaderMetaTypeId;
