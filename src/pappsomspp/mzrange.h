/**
 * \file pappsomspp/mass_range.h
 * \date 4/3/2015
 * \author Olivier Langella
 * \brief object to handle a mass range (an mz value + or - some delta)
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "types.h"
#include "precision.h"

#include <QString>
#include <map>

namespace pappso
{

class MzRange
{
  private:
  pappso_double m_mz;
  pappso_double m_delta;

  public:
  MzRange(pappso_double mz, PrecisionPtr precision);

  MzRange(pappso_double mz, pappso_double delta);

  MzRange(pappso_double mz,
          PrecisionPtr precision_lower,
          PrecisionPtr precision_upper);

  MzRange(const MzRange &other);

  pappso_double getMz() const;

  bool contains(pappso_double) const;
  QString toString() const;

  pappso_double
  lower() const
  {
    return (m_mz - m_delta);
  };

  pappso_double
  upper() const
  {
    return (m_mz + m_delta);
  };
};

} // namespace pappso
