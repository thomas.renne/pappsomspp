/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef OBOPSIMOD_H
#define OBOPSIMOD_H

#include <QRegExp>
#include "../types.h"

namespace pappso
{

class OboPsiMod;

class OboPsiModTerm
{
  friend OboPsiMod;

  private:
  void parseLine(const QString &line);
  void clearTerm();

  static QRegExp m_firstParse;
  static QRegExp m_findExactPsiModLabel;
  static QRegExp m_findRelatedPsiMsLabel;

  public:
  QString m_accession;
  QString m_name;
  QString m_psiModLabel;
  QString m_psiMsLabel;
  QString m_diffFormula;
  QString m_origin;

  pappso_double m_diffMono;

  bool
  isValid() const
  {
    return (!m_accession.isEmpty());
  };
};

class OboPsiModHandlerInterface
{
  public:
  virtual void setOboPsiModTerm(const OboPsiModTerm &term) = 0;
};

class OboPsiMod
{
  private:
  OboPsiModTerm m_term;
  OboPsiModHandlerInterface &m_handler;

  void parse();

  public:
  OboPsiMod(OboPsiModHandlerInterface &handler);
  ~OboPsiMod();
};
} // namespace pappso
#endif // OBOPSIMOD_H
