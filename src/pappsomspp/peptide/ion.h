/**
 * \file pappsomspp/peptide/ion.h
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief ion interface
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#ifndef ION_H
#define ION_H

#include "../types.h"

namespace pappso
{
class Ion
{
  public:
  Ion();
  ~Ion();

  virtual pappso_double getMass() const = 0;

  virtual pappso_double getMz(unsigned int charge) const final;
};
} // namespace pappso
#endif // ION_H
