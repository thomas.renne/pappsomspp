/**
 * \file pappsomspp/peptide/peptidefragment.h
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief peptide fragment model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "peptide.h"
#include "../pappsoexception.h"

namespace pappso
{


class PeptideFragment;

typedef std::shared_ptr<const PeptideFragment> PeptideFragmentSp;

class PeptideFragment : public PeptideInterface
{

  public:
  PeptideFragment(const PeptideSp &sp_peptide,
                  PeptideDirection direction,
                  unsigned int size);
  PeptideFragment(const PeptideFragment &other);
  PeptideFragment(PeptideFragment &&toCopy); // move constructor
  virtual ~PeptideFragment();

  virtual const PeptideSp &getPeptideSp() const;

  virtual unsigned int size() const override;
  virtual const QString getSequence() const override;

  virtual pappso_double getMass() const override;
  static const QString getPeptideIonDirectionName(PeptideDirection direction);
  PeptideDirection getPeptideIonDirection() const;

  virtual int getNumberOfAtom(AtomIsotopeSurvey atom) const override;
  virtual int getNumberOfIsotope(Isotope isotope) const override;

  virtual bool isPalindrome() const override;


  private:
  const PeptideSp msp_peptide;
  const PeptideDirection m_direction;
  const unsigned int m_size = 0;
  // the aa modification to add on Nter or Cter aa (depending on peptide
  // direction)
  AaModificationP m_nterCterCleavage;

  pappso_double m_mass = 0;
};


} // namespace pappso
