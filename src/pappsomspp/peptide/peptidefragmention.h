/**
 * \file pappsomspp/peptide/peptidefragmention.h
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief peptide ion model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QColor>
#include "peptidefragment.h"


namespace pappso
{


class PeptideFragmentIon;

typedef std::shared_ptr<const PeptideFragmentIon> PeptideFragmentIonSp;


class PeptideFragmentIon : public PeptideInterface
{

  public:
  PeptideFragmentIon(const PeptideFragmentSp &sp_fragment,
                     PeptideIon ion_type,
                     unsigned int number_of_neutral_phospho_loss);
  PeptideFragmentIon(const PeptideFragmentSp &sp_fragment, PeptideIon ion_type);
  PeptideFragmentIon(const PeptideFragmentIon &other);
  PeptideFragmentIon(PeptideFragmentIon &&toCopy); // move constructor
  virtual ~PeptideFragmentIon();

  PeptideFragmentIonSp makePeptideFragmentIonSp() const;

  virtual unsigned int size() const override;
  virtual const QString getSequence() const override;
  virtual const PeptideFragmentSp &getPeptideFragmentSp() const;
  virtual int getNumberOfAtom(AtomIsotopeSurvey atom) const override;

  virtual int getNumberOfIsotope(Isotope isotope) const override;

  virtual const QString getName() const override;

  pappso_double getMass() const override;

  const QString getPeptideIonName() const;

  static PeptideDirection getPeptideIonDirection(PeptideIon ion_type);
  static const QString getPeptideIonName(PeptideIon ion_type);
  static const QColor getPeptideIonColor(PeptideIon ion_type);

  PeptideIon getPeptideIonType() const;
  PeptideDirection getPeptideIonDirection() const;
  unsigned int getNumberOfNeutralPhosphoLoss() const;


  virtual bool isPalindrome() const override;


  private:
  const PeptideFragmentSp msp_fragment;
  const PeptideIon m_ionType;

  pappso_double m_mass = 0;

  // number of neutral phospho loss. this only occurs on MOD:00696
  // it loses MOD:00696 + H2O
  unsigned int m_neutralPhosphoLossNumber = 0;
};


} // namespace pappso
