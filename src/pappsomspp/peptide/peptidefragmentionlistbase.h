/**
 * \file pappsomspp/peptide/peptidefragmentionlistbase.h
 * \date 10/3/2015
 * \author Olivier Langella
 * \brief fragmentation base object
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "peptidefragmention.h"
#include <list>

namespace pappso
{

class PeptideFragmentIonListBase;
typedef std::shared_ptr<const PeptideFragmentIonListBase>
  PeptideFragmentIonListBaseSp;

class PeptideFragmentIonListBase
{
  using IonList = std::list<PeptideIon>;

  protected:
  const PeptideSp msp_peptide;
  std::list<PeptideFragmentIonSp> msp_peptide_fragment_ion_list;
  IonList m_ionList;
  unsigned int m_phosphorylationNumber;

  static const std::list<PeptideFragmentSp>
  getPeptideFragmentList(const PeptideSp &peptide);

  public:
  PeptideFragmentIonListBase(const PeptideSp &peptide, const IonList &ions);
  PeptideFragmentIonListBase(const PeptideFragmentIonListBase &other);
  virtual ~PeptideFragmentIonListBase();
  PeptideFragmentIonListBaseSp makePeptideFragmentIonListBaseSp() const;

  const std::list<PeptideIon> &getIonList() const;
  unsigned int
  getPhosphorylationNumber() const
  {
    return m_phosphorylationNumber;
  };

  virtual const PeptideSp &
  getPeptideSp() const
  {
    return msp_peptide;
  };

  const std::list<PeptideFragmentIonSp>
  getPeptideFragmentIonSp(PeptideIon ion_type) const;
  const PeptideFragmentIonSp &getPeptideFragmentIonSp(PeptideIon ion_type,
                                                      unsigned int size) const;
  const PeptideFragmentIonSp &
  getPeptideFragmentIonSp(PeptideIon ion_type,
                          unsigned int size,
                          unsigned int number_of_neutral_phospho_loss) const;


  std::list<PeptideFragmentIonSp>::const_iterator
  begin() const
  {
    return msp_peptide_fragment_ion_list.begin();
  }

  std::list<PeptideFragmentIonSp>::const_iterator
  end() const
  {
    return msp_peptide_fragment_ion_list.end();
  }


  const std::list<PeptideFragmentIonSp> &
  getPeptideFragmentIonList() const
  {
    return msp_peptide_fragment_ion_list;
  }

  static std::list<PeptideIon> getCIDionList();
  static std::list<PeptideIon> getETDionList();


  unsigned int
  size() const
  {
    return msp_peptide_fragment_ion_list.size();
  };
};


} // namespace pappso
