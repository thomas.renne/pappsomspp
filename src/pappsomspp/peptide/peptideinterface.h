
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <memory>
#include "ion.h"
#include <QString>
#include "../amino_acid/atomnumberinterface.h"
#include "../mzrange.h"


namespace pappso
{

class PeptideInterface;
typedef std::shared_ptr<const PeptideInterface> PeptideInterfaceSp;


class PeptideInterface : public Ion, public AtomNumberInterface
{
  public:
  /** @brief amino acid sequence without modification
   * */
  virtual const QString getSequence() const = 0;


  /** @brief tells if the peptide sequence is a palindrome
   */
  virtual bool isPalindrome() const = 0;

  /** @brief amino acid sequence without modification where L are replaced by I
   * */
  virtual const QString getSequenceLi() const;

  virtual unsigned int size() const = 0;
  virtual const QString
  getName() const
  {
    return QString("unknown");
  };

  virtual const QString getFormula(unsigned int charge) const final;

  virtual bool
  matchPeak(PrecisionPtr precision,
            pappso_double peak_mz,
            unsigned int charge) const final
  {
    return (MzRange((getMass() + (MHPLUS * charge)) / charge, precision)
              .contains(peak_mz));
  }

  // virtual int getNumberOfIsotope(Isotope isotope) const override = 0;
};
} // namespace pappso
