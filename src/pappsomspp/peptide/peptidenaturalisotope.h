/**
 * \file pappsomspp/peptide/peptidenaturalisotope.h
 * \date 8/3/2015
 * \author Olivier Langella
 * \brief peptide natural isotope model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "peptide.h"
namespace pappso
{


class PeptideNaturalIsotope;

typedef std::shared_ptr<const PeptideNaturalIsotope> PeptideNaturalIsotopeSp;

class PeptideNaturalIsotope : public PeptideInterface
{
  public:
  PeptideNaturalIsotope(const PeptideInterfaceSp &peptide,
                        const std::map<Isotope, int> &map_isotope);
  PeptideNaturalIsotope(const PeptideNaturalIsotope &other);
  ~PeptideNaturalIsotope();

  virtual unsigned int size() const override;
  virtual const QString getSequence() const override;
  pappso_double getMass() const override;

  virtual int getNumberOfAtom(AtomIsotopeSurvey atom) const override;
  virtual int getNumberOfIsotope(Isotope isotope) const override;

  pappso_double getIntensityRatio(unsigned int charge) const;
  const std::map<Isotope, int> &getIsotopeMap() const;

  virtual unsigned int getIsotopeNumber() const;

  virtual bool isPalindrome() const override;


  private:
  const PeptideInterfaceSp m_peptide;

  const std::map<Isotope, int> m_mapIsotope;
  pappso_double m_ratio;
  pappso_double m_mass;
};

} // namespace pappso
