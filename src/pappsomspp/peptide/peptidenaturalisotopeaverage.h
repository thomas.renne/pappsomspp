/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <vector>
#include "peptidenaturalisotope.h"

namespace pappso
{

class PeptideNaturalIsotopeList;

class PeptideNaturalIsotopeAverage;

typedef std::shared_ptr<const PeptideNaturalIsotopeAverage>
  PeptideNaturalIsotopeAverageSp;

class PeptideNaturalIsotopeAverage
{
  public:
  PeptideNaturalIsotopeAverage(const PeptideInterfaceSp &peptide,
                               unsigned int askedIsotopeRank,
                               unsigned int isotopeLevel,
                               unsigned int charge,
                               PrecisionPtr precision);
  PeptideNaturalIsotopeAverage(const PeptideNaturalIsotopeList &isotopeList,
                               unsigned int askedIsotopeRank,
                               unsigned int isotopeLevel,
                               unsigned int charge,
                               PrecisionPtr precision);

  PeptideNaturalIsotopeAverage(const PeptideNaturalIsotopeAverage &other);
  ~PeptideNaturalIsotopeAverage();

  PeptideNaturalIsotopeAverageSp makePeptideNaturalIsotopeAverageSp() const;
  pappso_double getMz() const;
  pappso_double getIntensityRatio() const;
  unsigned int getCharge() const;
  unsigned int getIsotopeNumber() const;
  unsigned int getIsotopeRank() const;
  const std::vector<PeptideNaturalIsotopeSp> &getComponents() const;
  const PeptideInterfaceSp &getPeptideInterfaceSp() const;
  PrecisionPtr getPrecision() const;
  virtual bool matchPeak(pappso_double peak_mz) const final;
  bool isEmpty() const;

  private:
  void recursiveDepletion(std::vector<PeptideNaturalIsotopeSp> &v_isotope_list,
                          unsigned int rank);


  private:
  const PeptideInterfaceSp mcsp_peptideSp;
  std::vector<PeptideNaturalIsotopeSp> m_peptideNaturalIsotopeSpList;

  pappso_double m_averageMz;
  pappso_double m_abundanceRatio;
  unsigned int m_isotopeLevel;
  unsigned int m_isotopeRank = 1;
  unsigned int m_z;
  PrecisionPtr mp_precision = nullptr;
};
} // namespace pappso
