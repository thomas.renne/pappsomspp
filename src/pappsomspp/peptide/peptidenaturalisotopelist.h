/**
 * \file pappsomspp/peptide/peptidenaturalisotopelist.h
 * \date 8/3/2015
 * \author Olivier Langella
 * \brief peptide natural isotope model
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <list>
#include "peptidenaturalisotope.h"

#include "peptidenaturalisotopeaverage.h"

namespace pappso
{

class PeptideNaturalIsotopeList;
typedef std::shared_ptr<const PeptideNaturalIsotopeList>
  PeptideNaturalIsotopeListSp;


class PeptideNaturalIsotopeList
{
  private:
  const PeptideInterfaceSp msp_peptide;
  std::list<PeptideNaturalIsotopeSp> msp_peptide_natural_isotope_list;

  public:
  /** @brief compute the list of possible isotopes for a peptide
   * @param peptide the peptide
   * @param minimu_ratio_to_compute the limit under which we stop to compute
   * because the ratio is too thin and not informative (default is 0.001). This
   * limit is the same for each atom to survey : CHNOS
   */
  PeptideNaturalIsotopeList(const PeptideInterfaceSp &peptide,
                            pappso_double minimum_ratio_to_compute = 0.001);
  PeptideNaturalIsotopeList(const PeptideNaturalIsotopeList &other);
  ~PeptideNaturalIsotopeList();
  PeptideNaturalIsotopeListSp makePeptideNaturalIsotopeListSp() const;

  typedef std::list<PeptideNaturalIsotopeSp>::const_iterator const_iterator;

  const_iterator begin() const;

  const_iterator end() const;

  const std::map<unsigned int, pappso_double>
  getIntensityRatioPerIsotopeNumber() const;

  std::vector<PeptideNaturalIsotopeSp>
  getByIsotopeNumber(unsigned int isotopeLevel, unsigned int charge) const;

  /** @brief get the list of natural isotopes representing at least a minimum
   * ratio of the whole isotope pattern
   * @param charge gives the number of H+, important to take into account for
   * isotope ratio
   * @param precision the mass accuracy to take into acount each isotope (C13 !=
   * H2)
   * @param minimum_isotope_pattern_ratio the minimum ratio of the isotope
   * pattern to represent
   */
  std::vector<PeptideNaturalIsotopeAverageSp>
  getByIntensityRatio(unsigned int charge,
                      PrecisionPtr precision,
                      pappso_double minimum_isotope_pattern_ratio) const;


  unsigned int size() const;
  const PeptideInterfaceSp &getPeptideInterfaceSp() const;
};


} // namespace pappso
