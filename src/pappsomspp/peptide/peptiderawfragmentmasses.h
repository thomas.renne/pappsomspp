/**
 * \file pappsomspp/peptide/peptiderawfragmentmasses.h
 * \date 16/7/2016
 * \author Olivier Langella
 * \brief class dedicated to raw mass computations of peptide products
 * (fragments)
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "peptide.h"
#include "../massspectrum/massspectrum.h"

namespace pappso
{


enum class RawFragmentationMode : std::int8_t
{
  full           = 0,
  proline_effect = 1
};


struct SimplePeakIonMatch
{
  DataPoint peak;
  PeptideIon ion_type;
  unsigned int ion_size;
  unsigned int ion_charge;
  pappso_double ion_mz;
};


class PeptideRawFragmentMasses
{
  using ionDeltatMzMassMap = pappso_double[20];

  private:
  static ionDeltatMzMassMap m_ionDeltaMz;

  /** \brief cumulative Nter masses (without internal Nter modification)
   * */
  std::vector<pappso_double> m_cumulativeNterMasses;

  /** \brief cumulative Cter masses (without internal Cter modification)
   * */
  std::vector<pappso_double> m_cumulativeCterMasses;

  public:
  PeptideRawFragmentMasses(const Peptide &peptide, RawFragmentationMode mode);
  ~PeptideRawFragmentMasses();

  void pushBackIonMasses(std::vector<pappso_double> &mass_list,
                         PeptideIon ion_type) const;
  void pushBackIonMz(std::vector<pappso_double> &mass_list,
                     PeptideIon ion_type,
                     unsigned int charge) const;

  void pushBackMatchSpectrum(std::vector<SimplePeakIonMatch> &peak_match_list,
                             const MassSpectrum &spectrum,
                             PrecisionPtr precision,
                             PeptideIon ion_type,
                             unsigned int charge) const;

  static pappso_double getDeltaMass(PeptideIon ion_type);
};


} // namespace pappso
