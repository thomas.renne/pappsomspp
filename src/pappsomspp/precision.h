/**
 * \file pappsomspp/mass_range.h
 * \date 4/3/2015
 * \author Olivier Langella
 * \brief object to handle a mass range (an mz value + or - some delta)
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include <map>

#include <QString>

#include "types.h"

namespace pappso
{

class PrecisionBase
{
  protected:
  const pappso_double m_nominal;

  PrecisionBase(pappso_double nominal) : m_nominal(nominal)
  {
  }

  public:
  virtual PrecisionUnit unit() const = 0;
  virtual pappso_double getNominal() const final;
  virtual pappso_double delta(pappso_double value) const = 0;
  virtual QString toString() const                       = 0;
};


/** \def specific type for a dalton precision
 *
 */
class DaltonPrecision : public PrecisionBase
{
  friend class PrecisionFactory;

  protected:
  DaltonPrecision(pappso_double x);

  public:
  virtual ~DaltonPrecision();

  virtual PrecisionUnit unit() const override;

  virtual pappso_double delta(pappso_double value) const override;

  virtual QString toString() const override;
};

/** \def specific type for a ppm precision
 *
 */
class PpmPrecision : public PrecisionBase
{
  friend class PrecisionFactory;

  protected:
  PpmPrecision(pappso_double x);

  public:
  virtual ~PpmPrecision();

  virtual PrecisionUnit unit() const override;

  virtual pappso_double delta(pappso_double value) const override;

  virtual QString toString() const override;
};


/** \def specific type for a res precision
 *
 */
class ResPrecision : public PrecisionBase
{
  friend class PrecisionFactory;

  protected:
  ResPrecision(pappso_double x);

  public:
  virtual ~ResPrecision();

  virtual PrecisionUnit unit() const override;

  virtual pappso_double delta(pappso_double value) const override;

  virtual QString toString() const override;
};


typedef const PrecisionBase *PrecisionPtr;


// was class Precision
class PrecisionFactory
{
  using MapDaltonPrecision = std::map<pappso_double, DaltonPrecision *>;
  using MapPpmPrecision    = std::map<pappso_double, PpmPrecision *>;
  using MapResPrecision    = std::map<pappso_double, ResPrecision *>;

  private:
  static MapDaltonPrecision m_mapDalton;
  static MapPpmPrecision m_mapPpm;
  static MapResPrecision m_mapRes;

  public:
  static PrecisionPtr fromString(const QString &str);
  static PrecisionPtr getDaltonInstance(pappso_double value);
  static PrecisionPtr getPpmInstance(pappso_double value);
  static PrecisionPtr getResInstance(pappso_double value);
};

} // namespace pappso
