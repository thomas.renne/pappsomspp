
#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// pappsomspp includes


/////////////////////// Local includes
#include "../../trace/maptrace.h"
#include "../filters/filterresample.h"


namespace pappso
{


class MassDataCombinerInterface
{

  public:
  MassDataCombinerInterface(int decimal_places = -1);
  virtual ~MassDataCombinerInterface();

  void setDecimalPlaces(int value);
  int getDecimalPlaces() const;

  void setFilterResampleKeepXRange(const FilterResampleKeepXRange &range);

  virtual MapTrace &combine(MapTrace &map_trace, const Trace &trace) const = 0;
  virtual MapTrace &combine(MapTrace &map_trace_out,
                            const MapTrace &map_trace_in) const            = 0;

  using Iterator = std::vector<const Trace *>::const_iterator;
  virtual MapTrace &combine(MapTrace &map_trace, Iterator begin, Iterator end);

  protected:
  //! Number of decimals to use for the keys (x values)
  int m_decimalPlaces = -1;

  bool m_isApplyXRangeFilter = false;

  FilterResampleKeepXRange m_filterXRange;
};


} // namespace pappso
