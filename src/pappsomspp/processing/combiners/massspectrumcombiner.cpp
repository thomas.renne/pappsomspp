#include <numeric>
#include <limits>
#include <vector>
#include <map>
#include <cmath>

#include <QDebug>
#include <QFile>

#if 0
// For debugging purposes.
#include <QFile>
#endif

#include "massspectrumcombiner.h"
#include "../../types.h"
#include "../../utils.h"
#include "../../pappsoexception.h"
#include "../../exception/exceptionoutofrange.h"
#include "../../exception/exceptionnotpossible.h"


namespace pappso
{


//! Construct an uninitialized instance.
MassSpectrumCombiner::MassSpectrumCombiner()
{
}


MassSpectrumCombiner::MassSpectrumCombiner(std::vector<pappso_double> bins,
                                           int decimalPlaces)
  : MassDataCombinerInterface(decimalPlaces), m_bins(bins)
{
}


MassSpectrumCombiner::MassSpectrumCombiner(int decimal_places)
  : MassDataCombinerInterface(decimal_places)
{
}


MassSpectrumCombiner::MassSpectrumCombiner(const MassSpectrumCombiner &other)
  : MassDataCombinerInterface(other.m_decimalPlaces)
{
  // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";

  m_bins.assign(other.m_bins.begin(), other.m_bins.end());
}


MassSpectrumCombiner::MassSpectrumCombiner(MassSpectrumCombinerCstSPtr other)
  : MassDataCombinerInterface(other->m_decimalPlaces)
{
  // qDebug() << __FILE__ << "@" << __LINE__ << __FUNCTION__ << "()";

  m_bins.assign(other->m_bins.begin(), other->m_bins.end());
}


//! Destruct the instance.
MassSpectrumCombiner::~MassSpectrumCombiner()
{
  // Calls the destructor for each DataPoint object in the vector.
  m_bins.clear();
}


void
MassSpectrumCombiner::setBins(std::vector<pappso_double> bins)
{
  m_bins.assign(bins.begin(), bins.end());

  qDebug() << "After bins assignment, local bins are of this size:"
           << m_bins.size() << "Starting at:" << m_bins.front()
           << "and ending at:" << m_bins.back();
}


const std::vector<pappso_double> &
MassSpectrumCombiner::getBins(std::vector<pappso_double> bins) const
{
  return m_bins;
}


std::vector<pappso_double>::iterator
MassSpectrumCombiner::begin()
{
  return m_bins.begin();
}


std::vector<pappso_double>::iterator
MassSpectrumCombiner::end()
{
  return m_bins.end();
}


std::vector<pappso_double>::const_iterator
MassSpectrumCombiner::begin() const
{
  return m_bins.begin();
}


std::vector<pappso_double>::const_iterator
MassSpectrumCombiner::end() const
{
  return m_bins.end();
}


std::size_t
MassSpectrumCombiner::binCount() const
{
  return m_bins.size();
}


//! Find the bin that will contain \p mz.

std::vector<pappso_double>::iterator
MassSpectrumCombiner::findBin(pappso_double mz)
{
  return std::find_if(m_bins.begin(), m_bins.end(), [mz](pappso_double bin) {
    return (mz <= bin);
  });
}


MapTrace &
MassSpectrumCombiner::combine(MapTrace &map_trace, const Trace &trace) const
{

  // qDebug() << "Thread:" << QThread::currentThreadId()
  //<< "Going to combine a trace this size:" << trace.size();

  if(!trace.size())
    {
      // qDebug() << "Thread:" << QThread::currentThreadId()
      //<< "Returning right away because trace is empty.";
      return map_trace;
    }


  // Let's check if we need to apply a filter to the input data.

  Trace filtered_trace(trace);

  if(m_isApplyXRangeFilter)
    {
      m_filterXRange.filter(filtered_trace);

      if(!filtered_trace.size())
        return map_trace;
    }

  return combineNoFilteringStep(map_trace, filtered_trace);
}


MapTrace &
MassSpectrumCombiner::combine(MapTrace &map_trace_out,
                              const MapTrace &map_trace_in) const
{

  // qDebug() << "Thread:" << QThread::currentThreadId()
  //<< "Going to combine a map_trace_in this size:" << map_trace_in.size();

  if(!map_trace_in.size())
    {
      // qDebug() << "Thread:" << QThread::currentThreadId()
      //<< "Returning right away because map_trace_in is empty.";
      return map_trace_out;
    }

  // Let's check if we need to apply a filter to the input data.

  Trace filtered_trace_in(map_trace_in);

  if(m_isApplyXRangeFilter)
    {
      m_filterXRange.filter(filtered_trace_in);

      if(!filtered_trace_in.size())
        return map_trace_out;
    }

  return combineNoFilteringStep(map_trace_out, filtered_trace_in);
}


} // namespace pappso
