#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../../types.h"
#include "../../massspectrum/massspectrum.h"
#include "massspectrumcombiner.h"

namespace pappso
{

class MassSpectrumMinusCombiner;

typedef std::shared_ptr<const MassSpectrumMinusCombiner>
  MassSpectrumMinusCombinerCstSPtr;
typedef std::shared_ptr<MassSpectrumMinusCombiner>
  MassSpectrumMinusCombinerSPtr;


class MassSpectrumMinusCombiner : public MassSpectrumCombiner
{

  public:
  MassSpectrumMinusCombiner();
  MassSpectrumMinusCombiner(int decimal_places);
  MassSpectrumMinusCombiner(MassSpectrumMinusCombinerCstSPtr other);
  MassSpectrumMinusCombiner(const MassSpectrumMinusCombiner &other);
  MassSpectrumMinusCombiner(const MassSpectrumCombiner &&other);

  virtual ~MassSpectrumMinusCombiner();

  protected:
  private:
  virtual MapTrace &combineNoFilteringStep(MapTrace &map_trace,
                                           const Trace &trace) const;
};


} // namespace pappso
