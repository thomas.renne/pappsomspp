#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../../types.h"
#include "../../massspectrum/massspectrum.h"
#include "massspectrumcombiner.h"

namespace pappso
{

class MassSpectrumPlusCombiner;

typedef std::shared_ptr<const MassSpectrumPlusCombiner>
  MassSpectrumPlusCombinerCstSPtr;

typedef std::shared_ptr<MassSpectrumPlusCombiner> MassSpectrumPlusCombinerSPtr;


class MassSpectrumPlusCombiner : public MassSpectrumCombiner
{

  public:
  MassSpectrumPlusCombiner();
  MassSpectrumPlusCombiner(int decimal_places);
  MassSpectrumPlusCombiner(const MassSpectrumPlusCombiner &other);
  MassSpectrumPlusCombiner(MassSpectrumPlusCombinerCstSPtr other);

  virtual ~MassSpectrumPlusCombiner();

  protected:
  private:
  virtual MapTrace &combineNoFilteringStep(MapTrace &map_trace,
                                           const Trace &trace) const;
};


} // namespace pappso
