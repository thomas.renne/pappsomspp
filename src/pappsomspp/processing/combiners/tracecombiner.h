#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../../types.h"
#include "../../trace/trace.h"
#include "../../trace/maptrace.h"
#include "../../trace/datapoint.h"
#include "../../mzrange.h"
#include "massdatacombinerinterface.h"


namespace pappso
{

class TraceCombiner;

typedef std::shared_ptr<const TraceCombiner> TraceCombinerCstSPtr;
typedef std::shared_ptr<TraceCombiner> TraceCombinerSPtr;


class TraceCombiner : public MassDataCombinerInterface
{

  public:
  TraceCombiner();
  TraceCombiner(int decimal_places);
  TraceCombiner(const TraceCombiner &other);
  TraceCombiner(TraceCombinerCstSPtr other);
  virtual ~TraceCombiner();

  protected:
};


} // namespace pappso
