
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "tracedetectioninterface.h"
#include "../../processing/filters/filtermorpho.h"


namespace pappso
{

class TraceDetectionZivy : public TraceDetectionInterface
{
  private:
  FilterMorphoMean m_smooth;
  FilterMorphoMinMax m_minMax;
  FilterMorphoMaxMin m_maxMin;
  pappso_double m_detectionThresholdOnMinMax;
  pappso_double m_detectionThresholdOnMaxMin;

  public:
  TraceDetectionZivy(unsigned int smoothing_half_window_length,
                     unsigned int minmax_half_window_length,
                     unsigned int maxmin_half_window_length,
                     pappso_double detection_threshold_on_minmax,
                     pappso_double detection_threshold_on_maxmin);
  virtual ~TraceDetectionZivy();


  void setFilterMorphoMean(const FilterMorphoMean &smooth);
  void setFilterMorphoMinMax(const FilterMorphoMinMax &m_minMax);
  void setFilterMorphoMaxMin(const FilterMorphoMaxMin &maxMin);
  void setDetectionThresholdOnMinmax(double detectionThresholdOnMinMax);
  void setDetectionThresholdOnMaxmin(double detectionThresholdOnMaxMin);

  unsigned int getSmoothingHalfEdgeWindows() const;
  unsigned int getMaxMinHalfEdgeWindows() const;

  unsigned int getMinMaxHalfEdgeWindows() const;
  pappso_double getDetectionThresholdOnMinmax() const;
  pappso_double getDetectionThresholdOnMaxmin() const;

  void detect(const Trace &xic,
              TraceDetectionSinkInterface &sink) const override;
};

} // namespace pappso
