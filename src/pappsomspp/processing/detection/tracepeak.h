/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "../../trace/trace.h"
#include "../../types.h"

namespace pappso
{

class TracePeak;
typedef std::shared_ptr<const TracePeak> TracePeakCstSPtr;

/** @/brief Xic Peak stores peak boudaries detected from a Xic
 * */
class TracePeak
{
  protected:
  pappso_double m_area = 0;
  DataPoint m_max;
  DataPoint m_left;
  DataPoint m_right;

  public:
  TracePeak();
  /** @brief construct a peak given a trace, begin and end x coordinate
   */
  TracePeak(std::vector<DataPoint>::const_iterator it_begin,
            std::vector<DataPoint>::const_iterator it_end);
  TracePeak(const TracePeak &other);
  ~TracePeak();


  TracePeakCstSPtr makeTracePeakCstSPtr() const;

  DataPoint &getMaxXicElement();
  const DataPoint &
  getMaxXicElement() const
  {
    return m_max;
  };
  void setMaxXicElement(const DataPoint &max);

  DataPoint &getLeftBoundary();
  const DataPoint &
  getLeftBoundary() const
  {
    return m_left;
  };
  void setLeftBoundary(const DataPoint &left);

  DataPoint &getRightBoundary();
  const DataPoint &getRightBoundary() const;
  void setRightBoundary(const DataPoint &right);
  pappso_double getArea() const;
  void setArea(pappso_double area);

  bool containsRt(pappso::pappso_double rt) const;


  bool
  operator==(const TracePeak &other) const
  {
    return ((m_area == other.m_area) && (m_max == other.m_max) &&
            (m_left == other.m_left) && (m_right == other.m_right));
  };
};


} // namespace pappso
