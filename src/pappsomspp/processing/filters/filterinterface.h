/**
 * \file pappsomspp/filers/filterinterface.h
 * \date 26/04/2019
 * \author Olivier Langella
 * \brief generic interface to filter any trace
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once
#include <memory>

namespace pappso
{
class Trace;

/** @brief generic interface to apply a filter on a trace
 */
class FilterInterface
{
  public:
  virtual Trace &filter(Trace &data_points) const = 0;
};

typedef std::shared_ptr<FilterInterface> FilterInterfaceSPtr;
typedef std::shared_ptr<const FilterInterface> FilterInterfaceCstSPtr;

class MassSpectrum;
/** @brief generic interface to apply a filter on a MassSpectrum
 * This is the same as FilterInterface, but some filter are only relevant if
 * they are used on MassSpectrum using this interface means the filter can only
 * be applied on MassSpectrum and not on Trace
 */
class MassSpectrumFilterInterface
{
  public:
  virtual MassSpectrum &filter(MassSpectrum &spectrum) const = 0;
};
} // namespace pappso
