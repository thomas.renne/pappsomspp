/**
 * \file pappsomspp/filers/filtermorpho.cpp
 * \date 02/05/2019
 * \author Olivier Langella
 * \brief collection of morphological filters
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filtermorpho.h"
#include "../../trace/trace.h"
#include <QDebug>
#include "../../exception/exceptionoutofrange.h"

using namespace pappso;

FilterMorphoWindowBase::FilterMorphoWindowBase(std::size_t half_window_size)
  : m_half_window_size(half_window_size)
{
}
FilterMorphoWindowBase::FilterMorphoWindowBase(
  const FilterMorphoWindowBase &other)
  : m_half_window_size(other.m_half_window_size)
{
}
std::size_t
FilterMorphoWindowBase::getHalfWindowSize() const
{
  return m_half_window_size;
}
Trace &
FilterMorphoWindowBase::filter(Trace &data_points) const
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
           << m_half_window_size << " data_points.size()" << data_points.size();
  if(m_half_window_size == 0)
    return data_points;
  Trace old_trace(data_points);
  auto it        = old_trace.begin();
  auto itend     = old_trace.end() - m_half_window_size - 1;
  auto it_target = data_points.begin();


  std::size_t loop_begin = 0;
  while((it != itend) && (loop_begin < m_half_window_size))
    {
      // maxYDataPoint(it_begin, it + m_half_window_size + 1);
      // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
      //         << it->x << " " << m_half_window_size;
      // it_target->x = it->x;
      it_target->y =
        getWindowValue(old_trace.begin(), it + m_half_window_size + 1);
      it++;
      it_target++;
      loop_begin++;
    }
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  while(it != itend)
    {
      // it_target->x = it->x;
      // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
      //        << it->x;
      it_target->y =
        getWindowValue(it - m_half_window_size, it + m_half_window_size + 1);
      it++;
      it_target++;
    }
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  while(it != old_trace.end())
    {
      // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
      //        << it->x;
      // it_target->x = it->x;
      it_target->y = getWindowValue(it - m_half_window_size, old_trace.end());
      it++;
      it_target++;
    }
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  // problem with move or swap : this lead to segmentation faults in some cases
  // data_points = std::move(new_trace);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return data_points;
}

FilterMorphoSum::FilterMorphoSum(std::size_t half_window_size)
  : FilterMorphoWindowBase(half_window_size)
{
}
FilterMorphoSum::FilterMorphoSum(const FilterMorphoSum &other)
  : FilterMorphoWindowBase(other.m_half_window_size)
{
}
double
FilterMorphoSum::getWindowValue(
  std::vector<DataPoint>::const_iterator begin,
  std::vector<DataPoint>::const_iterator end) const
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return sumYTrace(begin, end, 0);
}

FilterMorphoMax::FilterMorphoMax(std::size_t half_window_size)
  : FilterMorphoWindowBase(half_window_size)
{
}
FilterMorphoMax::FilterMorphoMax(const FilterMorphoMax &other)
  : FilterMorphoWindowBase(other.m_half_window_size)
{
}
double
FilterMorphoMax::getWindowValue(
  std::vector<DataPoint>::const_iterator begin,
  std::vector<DataPoint>::const_iterator end) const
{

  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return maxYDataPoint(begin, end)->y;
}

std::size_t
FilterMorphoMax::getMaxHalfEdgeWindows() const
{
  return m_half_window_size;
}

FilterMorphoMin::FilterMorphoMin(std::size_t half_window_size)
  : FilterMorphoWindowBase(half_window_size)
{
}
FilterMorphoMin::FilterMorphoMin(const FilterMorphoMin &other)
  : FilterMorphoWindowBase(other.m_half_window_size)
{
}


double
FilterMorphoMin::getWindowValue(
  std::vector<DataPoint>::const_iterator begin,
  std::vector<DataPoint>::const_iterator end) const
{
  return minYDataPoint(begin, end)->y;
}

std::size_t
FilterMorphoMin::getMinHalfEdgeWindows() const
{
  return m_half_window_size;
}

FilterMorphoMinMax::FilterMorphoMinMax(std::size_t half_window_size)
  : m_filter_max(half_window_size), m_filter_min(half_window_size)
{
}
FilterMorphoMinMax::FilterMorphoMinMax(const FilterMorphoMinMax &other)
  : m_filter_max(other.m_filter_max), m_filter_min(other.m_filter_min)
{
}
Trace &
FilterMorphoMinMax::filter(Trace &data_points) const
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  m_filter_max.filter(data_points);
  m_filter_min.filter(data_points);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return data_points;
}
std::size_t
FilterMorphoMinMax::getMinMaxHalfEdgeWindows() const
{
  return ((FilterMorphoMax)m_filter_max).getMaxHalfEdgeWindows();
}


FilterMorphoMaxMin::FilterMorphoMaxMin(std::size_t half_window_size)
  : m_filter_min(half_window_size), m_filter_max(half_window_size)
{
}
FilterMorphoMaxMin::FilterMorphoMaxMin(const FilterMorphoMaxMin &other)
  : m_filter_min(other.m_filter_min), m_filter_max(other.m_filter_max)
{
}
Trace &
FilterMorphoMaxMin::filter(Trace &data_points) const
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  m_filter_min.filter(data_points);
  m_filter_max.filter(data_points);
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  return data_points;
}
std::size_t
FilterMorphoMaxMin::getMaxMinHalfEdgeWindows() const
{
  return ((FilterMorphoMax)m_filter_max).getMaxHalfEdgeWindows();
}

FilterMorphoAntiSpike::FilterMorphoAntiSpike(std::size_t half_window_size)
  : m_half_window_size(half_window_size)
{
}
FilterMorphoAntiSpike::FilterMorphoAntiSpike(const FilterMorphoAntiSpike &other)
  : m_half_window_size(other.m_half_window_size)
{
}

std::size_t
FilterMorphoAntiSpike::getHalfWindowSize() const
{
  return m_half_window_size;
}
Trace &
FilterMorphoAntiSpike::filter(Trace &data_points) const
{
  if(m_half_window_size == 0)
    return data_points;
  Trace old_trace(data_points);
  auto it        = old_trace.begin();
  auto it_target = data_points.begin();
  auto itw       = old_trace.begin();

  auto itend = old_trace.end() - m_half_window_size - 1;
  // new_trace.reserve(data_points.size());

  while((it != old_trace.end()) &&
        (std::distance(old_trace.begin(), it) < (int)m_half_window_size))
    {
      // no anti spike at the begining of the signal
      it++;
      it_target++;
    }
  while(it != itend)
    {
      auto itwend = it + m_half_window_size + 1;
      itw         = findDifferentYvalue(it - m_half_window_size, it + 1, 0);
      if(itw == it)
        {
          itw = findDifferentYvalue(it + 1, itwend, 0);
          if(itw == itwend)
            {
              it_target->y = 0;
            }
        }

      it++;
      it_target++;
    }

  return data_points;
}


FilterMorphoMedian::FilterMorphoMedian(std::size_t half_window_size)
  : FilterMorphoWindowBase(half_window_size)
{
}
FilterMorphoMedian::FilterMorphoMedian(const FilterMorphoMedian &other)
  : FilterMorphoWindowBase(other.m_half_window_size)
{
}
double
FilterMorphoMedian::getWindowValue(
  std::vector<DataPoint>::const_iterator begin,
  std::vector<DataPoint>::const_iterator end) const
{
  return medianYTrace(begin, end);
}


FilterMorphoMean::FilterMorphoMean(std::size_t half_window_size)
  : FilterMorphoWindowBase(half_window_size)
{
}
FilterMorphoMean::FilterMorphoMean(const FilterMorphoMean &other)
  : FilterMorphoWindowBase(other.m_half_window_size)
{
}
std::size_t
FilterMorphoMean::getMeanHalfEdgeWindows() const
{
  return m_half_window_size;
}

double
FilterMorphoMean::getWindowValue(
  std::vector<DataPoint>::const_iterator begin,
  std::vector<DataPoint>::const_iterator end) const
{
  return meanYTrace(begin, end);
}


FilterMorphoBackground::FilterMorphoBackground(
  std::size_t median_half_window_size, std::size_t minmax_half_window_size)
  : m_filter_morpho_median(median_half_window_size),
    m_filter_morpho_minmax(minmax_half_window_size)
{
}

FilterMorphoBackground::FilterMorphoBackground(
  const FilterMorphoBackground &other)
  : m_filter_morpho_median(other.m_filter_morpho_median),
    m_filter_morpho_minmax(other.m_filter_morpho_minmax)
{
}
Trace &
FilterMorphoBackground::filter(Trace &data_points) const
{
  m_filter_morpho_median.filter(data_points);
  m_filter_morpho_minmax.filter(data_points);

  // finally filter negative values
  for(DataPoint &point : data_points)
    {
      if(point.y < 0)
        {
          point.y = 0;
        }
    }
  return data_points;
}
const FilterMorphoMedian &
FilterMorphoBackground::getFilterMorphoMedian() const
{
  return m_filter_morpho_median;
}
const FilterMorphoMinMax &
FilterMorphoBackground::getFilterMorphoMinMax() const
{
  return m_filter_morpho_minmax;
}
