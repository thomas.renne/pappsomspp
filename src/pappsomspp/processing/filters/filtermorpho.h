/**
 * \file pappsomspp/filers/filtermorpho.h
 * \date 02/05/2019
 * \author Olivier Langella
 * \brief collection of morphological filters
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filtersuite.h"
#include <cstddef>

namespace pappso
{

struct DataPoint;

/** @brief base class that apply a signal treatment based on a window
 */
class FilterMorphoWindowBase : public FilterInterface
{
  protected:
  std::size_t m_half_window_size = 0;

  virtual double
  getWindowValue(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end) const = 0;

  public:
  FilterMorphoWindowBase(std::size_t half_window_size);
  FilterMorphoWindowBase(const FilterMorphoWindowBase &other);
  virtual ~FilterMorphoWindowBase(){};
  virtual Trace &filter(Trace &data_points) const override;

  virtual std::size_t getHalfWindowSize() const;
};

/** @brief test purpose
 */
class FilterMorphoSum : public FilterMorphoWindowBase
{

  public:
  FilterMorphoSum(std::size_t half_window_size);
  FilterMorphoSum(const FilterMorphoSum &other);
  virtual ~FilterMorphoSum(){};
  double
  getWindowValue(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end) const override;
};

/** @brief transform the trace into its maximum over a window
 */
class FilterMorphoMax : public FilterMorphoWindowBase
{

  public:
  FilterMorphoMax(std::size_t half_window_size);
  FilterMorphoMax(const FilterMorphoMax &other);
  virtual ~FilterMorphoMax(){};
  double
  getWindowValue(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end) const override;

  std::size_t getMaxHalfEdgeWindows() const;
};

/** @brief transform the trace into its minimum over a window
 */
class FilterMorphoMin : public FilterMorphoWindowBase
{

  public:
  FilterMorphoMin(std::size_t half_window_size);
  FilterMorphoMin(const FilterMorphoMin &other);
  virtual ~FilterMorphoMin(){};
  double
  getWindowValue(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end) const override;

  std::size_t getMinHalfEdgeWindows() const;
};

/** @brief transform the trace with the minimum of the maximum
 * equivalent of the dilate filter for pictures
 */
class FilterMorphoMinMax : public FilterInterface
{
  private:
  FilterMorphoMax m_filter_max;
  FilterMorphoMin m_filter_min;

  public:
  FilterMorphoMinMax(std::size_t half_window_size);
  FilterMorphoMinMax(const FilterMorphoMinMax &other);
  virtual ~FilterMorphoMinMax(){};
  Trace &filter(Trace &data_points) const override;

  std::size_t getMinMaxHalfEdgeWindows() const;
};

/** @brief transform the trace with the maximum of the minimum
 * equivalent of the erode filter for pictures
 */
class FilterMorphoMaxMin : public FilterInterface
{
  private:
  FilterMorphoMin m_filter_min;
  FilterMorphoMax m_filter_max;

  public:
  FilterMorphoMaxMin(std::size_t half_window_size);
  FilterMorphoMaxMin(const FilterMorphoMaxMin &other);
  virtual ~FilterMorphoMaxMin(){};
  Trace &filter(Trace &data_points) const override;

  std::size_t getMaxMinHalfEdgeWindows() const;
};

/** @brief anti spike filter
 * set to zero alone values inside the window
 */
class FilterMorphoAntiSpike : public FilterInterface
{
  private:
  std::size_t m_half_window_size = 0;

  public:
  FilterMorphoAntiSpike(std::size_t half_window_size);
  FilterMorphoAntiSpike(const FilterMorphoAntiSpike &other);
  virtual ~FilterMorphoAntiSpike(){};
  Trace &filter(Trace &data_points) const override;

  std::size_t getHalfWindowSize() const;
};


/** @brief median filter
 * apply median of y values inside the window
 */
class FilterMorphoMedian : public FilterMorphoWindowBase
{

  public:
  FilterMorphoMedian(std::size_t half_window_size);
  FilterMorphoMedian(const FilterMorphoMedian &other);
  virtual ~FilterMorphoMedian(){};
  double
  getWindowValue(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end) const override;
};


/** @brief mean filter
 * apply mean of y values inside the window : this results in a kind of
 * smoothing
 */
class FilterMorphoMean : public FilterMorphoWindowBase
{

  public:
  FilterMorphoMean(std::size_t half_window_size);
  FilterMorphoMean(const FilterMorphoMean &other);
  virtual ~FilterMorphoMean(){};
  double
  getWindowValue(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end) const override;

  std::size_t getMeanHalfEdgeWindows() const;
};

/** @brief compute background of a trace
 * compute background noise on a trace
 */
class FilterMorphoBackground : public FilterInterface
{
  private:
  FilterMorphoMedian m_filter_morpho_median;
  FilterMorphoMinMax m_filter_morpho_minmax;

  public:
  FilterMorphoBackground(std::size_t median_half_window_size,
                         std::size_t minmax_half_window_size);
  FilterMorphoBackground(const FilterMorphoBackground &other);
  virtual ~FilterMorphoBackground(){};

  const FilterMorphoMedian &getFilterMorphoMedian() const;
  const FilterMorphoMinMax &getFilterMorphoMinMax() const;

  Trace &filter(Trace &data_points) const override;
};
} // namespace pappso
