/**
 * \file pappsomspp/filers/filterpass.h
 * \date 26/04/2019
 * \author Olivier Langella
 * \brief collection of filters concerned by Y selection
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"
#include <cstddef>

namespace pappso
{

class FilterLowPass : public FilterInterface
{
  private:
  double m_y_pass = 0;

  public:
  FilterLowPass(double y_pass);
  FilterLowPass(const FilterLowPass &other);
  virtual ~FilterLowPass(){};
  Trace &filter(Trace &data_points) const override;
};

class FilterHighPass : public FilterInterface
{
  private:
  double m_y_pass = 0;

  public:
  FilterHighPass(double y_pass);
  FilterHighPass(const FilterHighPass &other);
  virtual ~FilterHighPass(){};
  Trace &filter(Trace &data_points) const override;
};


class FilterHighPassPercentage : public FilterInterface
{
  private:
  double m_y_pass_ratio = 0;

  public:
  FilterHighPassPercentage(double y_ratio);
  FilterHighPassPercentage(const FilterHighPassPercentage &other);
  virtual ~FilterHighPassPercentage(){};
  Trace &filter(Trace &data_points) const override;
};


class FilterGreatestY : public FilterInterface
{
  private:
  std::size_t m_number_of_points = 0;

  public:
  FilterGreatestY(std::size_t number_of_points = 0);
  FilterGreatestY(const FilterGreatestY &other);
  virtual ~FilterGreatestY(){};
  Trace &filter(Trace &data_points) const override;

  std::size_t getNumberOfPoints() const;
};

class MassSpectrumFilterGreatestItensities : public MassSpectrumFilterInterface
{
  private:
  const FilterGreatestY m_filterGreatestY;

  public:
  MassSpectrumFilterGreatestItensities(std::size_t number_of_points = 0);
  MassSpectrumFilterGreatestItensities(
    const MassSpectrumFilterGreatestItensities &other);
  virtual ~MassSpectrumFilterGreatestItensities(){};
  MassSpectrum &filter(MassSpectrum &spectrum) const override;
};

class FilterFloorY : public FilterInterface
{

  public:
  FilterFloorY();
  FilterFloorY(const FilterFloorY &other);
  virtual ~FilterFloorY(){};
  Trace &filter(Trace &data_points) const override;
};


class FilterRoundY : public FilterInterface
{

  public:
  FilterRoundY();
  FilterRoundY(const FilterRoundY &other);
  virtual ~FilterRoundY(){};
  Trace &filter(Trace &data_points) const override;
};

/** @brief rescales Y values into a dynamic range
 * if the dynamic range is set to 0, this filter is ignored
 */
class FilterRescaleY : public FilterInterface
{
  private:
  double m_dynamic = 0;

  public:
  FilterRescaleY(double dynamic);
  FilterRescaleY(const FilterRescaleY &other);
  virtual ~FilterRescaleY(){};
  Trace &filter(Trace &data_points) const override;

  double getDynamicRange() const;
};


/** @brief rescales Y values given a tranformation factor
 */
class FilterScaleFactorY : public FilterInterface
{
  private:
  double m_factor = 0;

  public:
  FilterScaleFactorY(double m_factor);
  FilterScaleFactorY(const FilterScaleFactorY &other);
  virtual ~FilterScaleFactorY(){};
  Trace &filter(Trace &data_points) const override;

  double getScaleFactorY() const;
};

} // namespace pappso
