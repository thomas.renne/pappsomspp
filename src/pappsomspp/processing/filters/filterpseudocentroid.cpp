/**
 * \file pappsomspp/filers/filterpseudocentroid.cpp
 * \date 7/12/2019
 * \author Olivier Langella
 * \brief apply a pseudo centroid algorithm on a tims tof spectrum
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filterpseudocentroid.h"
#include <pwiz/utility/findmf/base/ms/peakpickerqtof.hpp>

using namespace pappso;

FilterPseudoCentroid::FilterPseudoCentroid(double resolution,
                                           double smoothwidth,
                                           double integrationWidth,
                                           double intensityThreshold,
                                           bool area,
                                           uint32_t maxnumberofpeaks)
{

  m_resolution         = resolution;       //!< instrument resolution
  m_smoothwidth        = smoothwidth;      //!< smoothwidth
  m_integrationWidth   = integrationWidth; //! integration width
  m_intensityThreshold = intensityThreshold;
  m_area               = area; //!< do you want to store are or intensity
  m_maxnumberofpeaks =
    maxnumberofpeaks; //!< max number of peaks returned by picker
}

FilterPseudoCentroid::FilterPseudoCentroid(const FilterPseudoCentroid &other)
{
  m_resolution         = other.m_resolution;       //!< instrument resolution
  m_smoothwidth        = other.m_smoothwidth;      //!< smoothwidth
  m_integrationWidth   = other.m_integrationWidth; //! integration width
  m_intensityThreshold = other.m_intensityThreshold;
  m_area = other.m_area; //!< do you want to store are or intensity
  m_maxnumberofpeaks =
    other.m_maxnumberofpeaks; //!< max number of peaks returned by picker
}

FilterPseudoCentroid::~FilterPseudoCentroid()
{
}

Trace &
FilterPseudoCentroid::filter(Trace &data_points) const
{
  /*
QTOFPeakPickerFilter(
      const pwiz::msdata::SpectrumListPtr & inner, //!< spectrum list
      double resolution, //!< instrument resolution
      double smoothwidth = 2., //!< smoothwidth
      double integrationWidth = 4, //! integration width
      double intensityThreshold = 10.,
      bool area = true, //!< do you want to store are or intensity
uint32_t maxnumberofpeaks = 0 //!< max number of peaks returned by picker
      )
      */
  std::pair<double, double> range =
    std::make_pair(data_points.front().x, data_points.back().x);
  // construct peak picker
  ralab::base::ms::PeakPicker<double, ralab::base::ms::SimplePeakArea> pp(
    m_resolution,
    range,
    m_smoothwidth,
    m_integrationWidth,
    m_intensityThreshold,
    m_area,
    m_maxnumberofpeaks);

  std::vector<double> mzs         = data_points.xValues();
  std::vector<double> intensities = data_points.yValues();
  pp(mzs.begin(), mzs.end(), intensities.begin());

  data_points.clear();
  auto itmz  = pp.getPeakMass().begin();
  auto itint = pp.getPeakArea().begin();
  while(itmz != pp.getPeakMass().end())
    {
      data_points.push_back({*itmz, *itint});
      itmz++;
      itint++;
    }
  return data_points;
}
