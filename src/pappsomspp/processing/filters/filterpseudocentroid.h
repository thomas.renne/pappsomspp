/**
 * \file pappsomspp/filers/filterpseudocentroid.h
 * \date 7/12/2019
 * \author Olivier Langella
 * \brief apply a pseudo centroid algorithm on a tims tof spectrum
 * actually this class is a wrapper for proteowizard ralab::base::ms::PeakPicker
 * class
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"
#include "../../trace/trace.h"

namespace pappso
{
/**
 * @todo write docs
 */
class FilterPseudoCentroid : public FilterInterface
{
  public:
  /**
   * Default constructor
   */
  FilterPseudoCentroid(
    double resolution,              //!< instrument resolution
    double smoothwidth        = 2., //!< smoothwidth
    double integrationWidth   = 4,  //! integration width
    double intensityThreshold = 10.,
    bool area                 = true, //!< do you want to store are or intensity
    uint32_t maxnumberofpeaks = 0 //!< max number of peaks returned by picker

  );

  /**
   * Copy constructor
   *
   * @param other TODO
   */
  FilterPseudoCentroid(const FilterPseudoCentroid &other);

  /**
   * Destructor
   */
  ~FilterPseudoCentroid();

  Trace &filter(Trace &data_points) const override;

  private:
  double m_resolution;              //!< instrument resolution
  double m_smoothwidth        = 2.; //!< smoothwidth
  double m_integrationWidth   = 4;  //! integration width
  double m_intensityThreshold = 10.;
  bool m_area                 = true; //!< do you want to store are or intensity
  uint32_t m_maxnumberofpeaks = 0; //!< max number of peaks returned by picker
};

} // namespace pappso
