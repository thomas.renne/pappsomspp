/**
 * \file pappsomspp/filers/filterresample.h
 * \date 28/04/2019
 * \author Olivier Langella
 * \brief collection of filters concerned by X selection
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"
#include <cstddef>
#include "../../mzrange.h"

namespace pappso
{

class FilterResampleKeepSmaller : public FilterInterface
{
  private:
  double m_value;

  public:
  FilterResampleKeepSmaller(double x_value);
  FilterResampleKeepSmaller(const FilterResampleKeepSmaller &other);
  virtual ~FilterResampleKeepSmaller(){};
  Trace &filter(Trace &trace) const override;
};

class FilterResampleKeepGreater : public FilterInterface
{
  private:
  double m_value;

  public:
  FilterResampleKeepGreater(double x_value);
  FilterResampleKeepGreater(const FilterResampleKeepGreater &other);
  virtual ~FilterResampleKeepGreater(){};
  Trace &filter(Trace &trace) const override;

  double getThresholdX() const;
};

class FilterResampleRemoveXRange : public FilterInterface
{
  private:
  double m_min_x;
  double m_max_x;

  public:
  FilterResampleRemoveXRange(double min_x, double max_x);
  FilterResampleRemoveXRange(const FilterResampleRemoveXRange &other);
  virtual ~FilterResampleRemoveXRange(){};
  Trace &filter(Trace &trace) const override;
};

class FilterResampleKeepXRange : public FilterInterface
{
  private:
  double m_min_x;
  double m_max_x;

  public:
  FilterResampleKeepXRange(double min_x = 0, double max_x = 0);
  FilterResampleKeepXRange(const FilterResampleKeepXRange &other);
  virtual ~FilterResampleKeepXRange(){};
  Trace &filter(Trace &trace) const override;
};

class MassSpectrumFilterResampleRemoveMzRange
  : public MassSpectrumFilterInterface
{
  private:
  const FilterResampleRemoveXRange m_filterRange;

  public:
  MassSpectrumFilterResampleRemoveMzRange(const MzRange &mz_range);
  MassSpectrumFilterResampleRemoveMzRange(
    const MassSpectrumFilterResampleRemoveMzRange &other);
  virtual ~MassSpectrumFilterResampleRemoveMzRange(){};
  MassSpectrum &filter(MassSpectrum &spectrum) const override;
};


class MassSpectrumFilterResampleKeepMzRange : public MassSpectrumFilterInterface
{
  private:
  const FilterResampleKeepXRange m_filterRange;

  public:
  MassSpectrumFilterResampleKeepMzRange(const MzRange &mz_range);
  MassSpectrumFilterResampleKeepMzRange(
    const MassSpectrumFilterResampleKeepMzRange &other);
  virtual ~MassSpectrumFilterResampleKeepMzRange(){};
  MassSpectrum &filter(MassSpectrum &spectrum) const override;
};
} // namespace pappso
