/**
 * \file pappsomspp/filers/filtertandemremovec13.h
 * \date 26/04/2019
 * \author Olivier Langella
 * \brief new implementation of the X!Tandem filter to remove isotopes in an MS2
 * signal
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include "filterinterface.h"
#include <cstddef>

namespace pappso
{


/** \brief clean probable C13 isotopes as in X!Tandem algorithm
 * */
class FilterTandemRemoveC13 : public MassSpectrumFilterInterface
{
  private:
  double m_arbitrary_range_between_isotopes = 1.5;
  double m_arbitrary_minimum_mz             = 200.0;

  public:
  FilterTandemRemoveC13();
  FilterTandemRemoveC13(const FilterTandemRemoveC13 &other);
  MassSpectrum &filter(MassSpectrum &data_points) const override;
};


/** @brief Deisotope the mass spectrum
 * \c this mass spectrum is iterated over and according to a data point-based
 * moving window progression does the following tests:
 *
 * - any data point having a x value (m/z value) less than 200 is conserved;
 *
 * - any data point having a x value greater than the previous data point's x
 *   value by at least 0.95 is conserved;
 *
 *
remove isotopes as in X!Tandem algorithm this method doesn't
   * really remove isotopes: it cleans up multiple intensities within one
   * Dalton of each other.
   * */

class FilterTandemDeisotope : public MassSpectrumFilterInterface
{
  private:
  double m_arbitrary_range_between_isotopes = 0.95;
  double m_arbitrary_minimum_mz             = 200.0;

  public:
  FilterTandemDeisotope();
  FilterTandemDeisotope(const FilterTandemDeisotope &other);
  MassSpectrum &filter(MassSpectrum &data_points) const override;
};
} // namespace pappso
