/**
 * \file pappsomspp/filers/filtertriangle.h
 * \date 27/10/2019
 * \author Olivier Langella
 * \brief sum peaks under a triangle base
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "filtertriangle.h"
#include <QDebug>

using namespace pappso;

FilterTriangle::FilterTriangle()
{
}

FilterTriangle::FilterTriangle(const FilterTriangle &other)
{
  m_triangleSlope = other.m_triangleSlope;
}

FilterTriangle::~FilterTriangle()
{
}

double
FilterTriangle::setTriangleSlope(double intensity, double mz)
{
  m_maxMzRange    = mz;
  m_triangleSlope = mz / intensity;
  return m_triangleSlope;
}

DataPoint
FilterTriangle::sumAndRemove(Trace &trace, const DataPoint &max_intensity) const
{

  // qDebug();
  // double sum      = max_intensity.y;
  // double wmean    = max_intensity.y * max_intensity.x;
  double mz_range = max_intensity.y * m_triangleSlope;
  if(mz_range > m_maxMzRange)
    {
      mz_range = m_maxMzRange;
    }
  double min_range = max_intensity.x - mz_range;
  double max_range = max_intensity.x + mz_range;

  DataPoint new_point;
  new_point.y = 0;
  new_point.x = 0;
  // Trace new_trace;

  for(DataPoint &old_point : trace)
    {
      if((old_point.x < min_range) || (old_point.x > max_range))
        {
          // new_trace.push_back(old_point);
        }
      else
        {
          new_point.y += old_point.y;
          new_point.x += old_point.y * old_point.x;
          old_point.y = 0;
        }
    }

  // trace       = std::move(new_trace);
  new_point.x = new_point.x / new_point.y; // weighted mean on mz
  return new_point;
}

Trace &
FilterTriangle::filter(Trace &data_points) const
{
  // qDebug() << data_points.size();
  Trace new_trace;

  std::vector<DataPoint>::iterator it_max =
    maxYDataPoint(data_points.begin(), data_points.end());
  // qDebug();
  while((it_max != data_points.end()) && (it_max->y > 0))
    {
      //qDebug();
      new_trace.push_back(sumAndRemove(data_points, *it_max));
      //qDebug();
      it_max = maxYDataPoint(data_points.begin(), data_points.end());
    }

  new_trace.sortX();
  // qDebug() << new_trace.size();
  data_points = std::move(new_trace);
  // qDebug() << data_points.size();
  return data_points;
}
