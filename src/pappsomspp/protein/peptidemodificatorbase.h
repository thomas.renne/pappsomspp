/**
 * \file protein/peptidemodificatorbase.h
 * \date 6/12/2016
 * \author Olivier Langella
 * \brief base class for all peptide modification builders
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "enzymeproductinterface.h"

namespace pappso
{


class PeptideModificatorBase : public PeptideModificatorInterface,
                               public PeptideSpSinkInterface
{
  protected:
  QRegExp m_pattern;

  virtual void
  getModificationPositionList(std::vector<unsigned int> &position_list,
                              const QString &peptide_str) final;
  virtual void
  getModificationPositionList(std::vector<unsigned int> &position_list,
                              const Peptide *p_peptide,
                              AaModificationP mod,
                              unsigned int modification_counter);

  public:
  PeptideModificatorBase();
  ~PeptideModificatorBase();


  /** \brief set the pattern on which the modification will be applied (usually
   * the list of concerned AA) \details the pattern is a standard regular
   * expression : it describes the target site to modify. This standard regular
   * expression is used to make a replacement of all motifs describes by an
   * underscore ("_") The underscore is taken as a landmark to apply the
   * modification. \param pattern the regular expression pattern ( [YST] to
   * replace Y, S and T with _ and modify it ). The pattern can also contain the
   * / separator to eventually specify the replacement motif. This can be used
   * to described very complex modification patterns
   *
   */
  // :
  virtual void setModificationPattern(QString &pattern) final;
};


} // namespace pappso
