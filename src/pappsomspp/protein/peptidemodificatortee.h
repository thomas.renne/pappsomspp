
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "enzymeproductinterface.h"
#include "../pappsoexception.h"

namespace pappso
{


class PeptideModificatorTee : public PeptideModificatorInterface
{
  private:
  std::list<PeptideModificatorInterface *> m_peptideModPtrList;

  public:
  PeptideModificatorTee();
  PeptideModificatorTee(const PeptideModificatorTee &other);
  ~PeptideModificatorTee();
  void setPeptideSp(std::int8_t sequence_database_id,
                    const ProteinSp &protein_sp,
                    bool is_decoy,
                    const PeptideSp &peptide_sp_original,
                    unsigned int start,
                    bool is_nter,
                    unsigned int missed_cleavage_number,
                    bool semi_enzyme) override;

  void addModificator(PeptideModificatorInterface *p_peptide_mod);
};


} // namespace pappso
