
/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "enzymeproductinterface.h"


namespace pappso
{


/** \brief cut a regular peptide obtained with an enzyme at any position from
 * Nter or Cter
 *
 * WORK IN PROGRESS
 * */
class PeptideSemiEnzyme : public EnzymeProductInterface, PeptideSinkInterface
{
  private:
  EnzymeProductInterface *m_sink = nullptr;

  public:
  PeptideSemiEnzyme();
  ~PeptideSemiEnzyme();

  // EnzymeProductInterface
  void setPeptide(std::int8_t sequence_database_id,
                  const ProteinSp &protein_sp,
                  bool is_decoy,
                  const QString &peptide,
                  unsigned int start,
                  bool is_nter,
                  unsigned int missed_cleavage_number,
                  bool semi_enzyme) override;

  // PeptideSinkInterface
  void
  setSink(EnzymeProductInterface *sink) override
  {
    m_sink = sink;
  };
};


} // namespace pappso
