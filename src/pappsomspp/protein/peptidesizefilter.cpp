
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "peptidesizefilter.h"

namespace pappso
{
PeptideSizeFilter::PeptideSizeFilter(unsigned int min_size,
                                     unsigned int max_size)
  : m_minSize(min_size), m_maxSize(max_size)
{
}

PeptideSizeFilter::~PeptideSizeFilter()
{
}

void
PeptideSizeFilter::setPeptide(std::int8_t sequence_database_id,
                              const ProteinSp &protein_sp,
                              bool is_decoy,
                              const QString &peptide,
                              unsigned int start,
                              bool is_nter,
                              unsigned int missed_cleavage_number,
                              bool semi_enzyme)
{
  if((peptide.size() >= m_minSize) && (peptide.size() <= m_maxSize))
    {
      m_sink->setPeptide(sequence_database_id,
                         protein_sp,
                         is_decoy,
                         peptide,
                         start,
                         is_nter,
                         missed_cleavage_number,
                         semi_enzyme);
    }
}
} // namespace pappso
