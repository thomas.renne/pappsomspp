/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "peptidemodificatorbase.h"

namespace pappso
{


/** \brief Modify a peptide shared pointer with a variable modification on one
 * AA
 * */
class PeptideVariableModificationBuilder : public PeptideModificatorBase
{
  private:
  QString m_aaModificationList;

  protected:
  static bool next_combination(const std::vector<unsigned int>::iterator first,
                               std::vector<unsigned int>::iterator k,
                               const std::vector<unsigned int>::iterator last);

  PeptideModificatorInterface *m_sink = nullptr;
  AaModificationP mp_mod;
  // minimum number of positions to modify
  unsigned int m_minNumberMod = 0;
  // maximum number of positions to modify
  unsigned int m_maxNumberMod = 30000;
  // modification counter per site
  unsigned int m_modificationCount = 0;

  // protein Nter modification
  bool m_isProtNterMod = true;
  // protein Cter modification
  bool m_isProtCterMod = true;

  bool m_isProtElseMod = true;

  public:
  PeptideVariableModificationBuilder(AaModificationP mod);
  ~PeptideVariableModificationBuilder();
  void setPeptideSp(std::int8_t sequence_database_id,
                    const ProteinSp &protein_sp,
                    bool is_decoy,
                    const PeptideSp &peptide_sp_original,
                    unsigned int start,
                    bool is_nter,
                    unsigned int missed_cleavage_number,
                    bool semi_enzyme) override;

  void addAa(char aa);

  void
  setMaxNumberMod(unsigned int max_num)
  {
    m_maxNumberMod = max_num;
  };
  void
  setMinNumberMod(unsigned int min_num)
  {
    m_minNumberMod = min_num;
  };
  void
  setModificationCounter(unsigned int counter)
  {
    m_modificationCount = counter;
  };

  void
  setSink(PeptideModificatorInterface *sink) override
  {
    m_sink = sink;
  };


  /** \brief this modification concerns the Nter peptide
   * */
  void
  setProtNter(bool arg1)
  {
    m_isProtNterMod = arg1;
  };
  /** \brief this modification concerns the Cter peptide
   * */
  void
  setProtCter(bool arg1)
  {
    m_isProtCterMod = arg1;
  };
  /** \brief this modification concerns all peptides between Nter and Cter
   * */
  void
  setProtElse(bool arg1)
  {
    m_isProtElseMod = arg1;
  };
};


} // namespace pappso
