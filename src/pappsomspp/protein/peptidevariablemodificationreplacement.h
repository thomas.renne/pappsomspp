/**
 * \file protein/peptidevariablemodificationreplacement.h
 * \date 1/12/2016
 * \author Olivier Langella
 * \brief potential replacement of a modification by an other
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "peptidevariablemodificationbuilder.h"

namespace pappso
{


class PeptideVariableModificationReplacement
  : public PeptideVariableModificationBuilder
{
  private:
  void replaceModificationsAtPosition(Peptide &new_peptide,
                                      unsigned int position);

  AaModificationP mp_modAfter;

  public:
  PeptideVariableModificationReplacement(AaModificationP mod_before,
                                         AaModificationP mod_after);
  ~PeptideVariableModificationReplacement();

  void setPeptideSp(std::int8_t sequence_database_id,
                    const ProteinSp &protein_sp,
                    bool is_decoy,
                    const PeptideSp &peptide_sp_original,
                    unsigned int start,
                    bool is_nter,
                    unsigned int missed_cleavage_number,
                    bool semi_enzyme) override;
};


} // namespace pappso
