/**
 * \file pappsomspp/psm/experimental/ionisotoperatioscore.h
 * \date 18/05/2019
 * \author Olivier Langella
 * \brief psm score computed using ion isotopes
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../../massspectrum/massspectrum.h"
#include "../../peptide/peptide.h"

namespace pappso
{
class IonIsotopeRatioScore
{
  public:
  IonIsotopeRatioScore(const MassSpectrum &spectrum,
                       const PeptideSp &peptide_sp,
                       unsigned int parent_charge,
                       PrecisionPtr precision,
                       std::vector<PeptideIon> ion_list);
  virtual ~IonIsotopeRatioScore();

  pappso::pappso_double getIonIsotopeRatioScore() const;

  private:
  pappso::pappso_double m_ionIsotopeRatioScore;
};
} // namespace pappso
