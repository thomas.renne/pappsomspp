/**
 * \file pappsomspp/psm/morpheusscore.h
 * \date 16/7/2016
 * \author Olivier Langella
 * \brief computation of Morpheus score
 * https://github.com/cwenger/Morpheus/blob/master/Morpheus/Morpheus/PeptideSpectrumMatch.cs
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../massspectrum/massspectrum.h"
#include "../../peptide/peptide.h"
#include "../../peptide/peptiderawfragmentmasses.h"

namespace pappso
{
class MorpheusScore
{
  public:
  MorpheusScore(const MassSpectrum &spectrum,
                pappso::PeptideSp peptideSp,
                unsigned int parent_charge,
                PrecisionPtr precision,
                std::vector<PeptideIon> ion_list,
                RawFragmentationMode fragmentation_mode);
  ~MorpheusScore();

  pappso::pappso_double getMorpheusScore() const;

  private:
  pappso::pappso_double _morpheus_score;
};
} // namespace pappso
