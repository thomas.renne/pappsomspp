/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/

#pragma once

#include "peakionmatch.h"
#include "../peptide/peptidenaturalisotopeaverage.h"

namespace pappso
{
class PeakIonIsotopeMatch;
typedef std::shared_ptr<const PeakIonIsotopeMatch> PeakIonIsotopeMatchCstSPtr;

class PeakIonIsotopeMatch : public PeakIonMatch
{
  public:
  PeakIonIsotopeMatch(
    const DataPoint &peak,
    const PeptideNaturalIsotopeAverageSp &naturalIsotopeAverageSp,
    const PeptideFragmentIonSp &ion_sp);
  PeakIonIsotopeMatch(const PeakIonIsotopeMatch &other);
  virtual ~PeakIonIsotopeMatch();

  virtual const PeptideNaturalIsotopeAverageSp &
  getPeptideNaturalIsotopeAverageSp() const;

  private:
  PeptideNaturalIsotopeAverageSp _naturalIsotopeAverageSp;
};


} // namespace pappso

Q_DECLARE_METATYPE(pappso::PeakIonIsotopeMatchCstSPtr);
