/**
 * \file pappsomspp/psm/peakionmatch.h
 * \date 4/4/2015
 * \author Olivier Langella
 * \brief associate a peak and a peptide + charge
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../massspectrum/massspectrum.h"
#include "../peptide/peptidefragmention.h"

namespace pappso
{
class PeakIonMatch
{
  public:
  PeakIonMatch(const DataPoint &peak,
               const PeptideFragmentIonSp &ion_sp,
               unsigned int charge);
  PeakIonMatch(const PeakIonMatch &other);
  virtual ~PeakIonMatch();

  virtual const PeptideFragmentIonSp &
  getPeptideFragmentIonSp() const
  {
    return _ion_sp;
  };

  const DataPoint &
  getPeak() const
  {
    return _peak;
  };

  unsigned int
  getCharge() const
  {
    return _charge;
  };

  PeptideIon
  getPeptideIonType() const
  {
    return _ion_sp.get()->getPeptideIonType();
  };
  PeptideDirection
  getPeptideIonDirection() const
  {
    return _ion_sp.get()
      ->getPeptideFragmentSp()
      .get()
      ->getPeptideIonDirection();
  };

  private:
  DataPoint _peak;
  PeptideFragmentIonSp _ion_sp;
  unsigned int _charge;
};
} // namespace pappso
