/*
 * *******************************************************************************
 * * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 * *
 * * This file is part of MassChroqPRM.
 * *
 * *     MassChroqPRM is free software: you can redistribute it and/or modify
 * *     it under the terms of the GNU General Public License as published by
 * *     the Free Software Foundation, either version 3 of the License, or
 * *     (at your option) any later version.
 * *
 * *     MassChroqPRM is distributed in the hope that it will be useful,
 * *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 * *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * *     GNU General Public License for more details.
 * *
 * *     You should have received a copy of the GNU General Public License
 * *     along with MassChroqPRM.  If not, see <http://www.gnu.org/licenses/>.
 * *
 * * Contributors:
 * *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 * implementation
 * ******************************************************************************/


#pragma once


#include "../massspectrum/massspectrum.h"
#include "../mzrange.h"
#include "../peptide/peptidefragmention.h"
#include "../peptide/peptidefragmentionlistbase.h"
#include "peakionisotopematch.h"

namespace pappso
{

class PeptideIsotopeSpectrumMatch
{
  public:
  /** @brief annotate spectrum with peptide ions and isotopes
   * @param spectrum the spectrum to annotate
   * @param peptide_sp peptide to fragment
   * @param parent_charge charge of the ion parent
   * @param precision MS2 mass measurement precision
   * @param ion_list ion types to compute fragments
   * @param max_isotope_number maximum isotope number to compute (0 means only
   * monoisotope is computed)
   * @param max_isotope_rank maximum rank inside isotope level to compute
   */
  PeptideIsotopeSpectrumMatch(const MassSpectrum &spectrum,
                              const PeptideSp &peptide_sp,
                              unsigned int parent_charge,
                              PrecisionPtr precision,
                              std::list<PeptideIon> ion_list,
                              unsigned int max_isotope_number,
                              unsigned int max_isotope_rank);
  PeptideIsotopeSpectrumMatch(
    const MassSpectrum &spectrum,
    std::vector<PeptideNaturalIsotopeAverageSp> v_peptideIsotopeList,
    std::vector<PeptideFragmentIonSp> v_peptideIonList,
    PrecisionPtr precision);
  PeptideIsotopeSpectrumMatch(const PeptideIsotopeSpectrumMatch &other);

  ~PeptideIsotopeSpectrumMatch();

  const std::list<PeakIonIsotopeMatch> &getPeakIonIsotopeMatchList() const;


  typedef std::list<PeakIonIsotopeMatch>::const_iterator const_iterator;

  unsigned int
  size() const
  {
    return _peak_ion_match_list.size();
  }
  const_iterator
  begin() const
  {
    return _peak_ion_match_list.begin();
  }
  const_iterator
  end() const
  {
    return _peak_ion_match_list.end();
  }

  private:
  virtual std::list<DataPoint>::iterator
  getBestPeakIterator(std::list<DataPoint> &peak_list,
                      const PeptideNaturalIsotopeAverage &ion) const;

  PrecisionPtr _precision;
  std::list<PeakIonIsotopeMatch> _peak_ion_match_list;
};


} // namespace pappso
