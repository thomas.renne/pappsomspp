/**
 * \file pappsomspp/psm/xtandem/xtandemhyperscore.h
 * \date 19/3/2015
 * \author Olivier Langella
 * \brief computation of the X!Tandem hyperscore
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include "../../massspectrum/massspectrum.h"
#include "../../mzrange.h"
#include "../../peptide/peptidefragmention.h"

namespace pappso
{

class XtandemHyperscore
{
  public:
  using AaFactorMap = std::map<char, pappso_double>;
  XtandemHyperscore(const MassSpectrum &spectrum,
                    pappso::PeptideSp peptideSp,
                    unsigned int parent_charge,
                    PrecisionPtr precision,
                    std::list<PeptideIon> ion_list,
                    bool refine_spectrum_synthesis);
  ~XtandemHyperscore();

  pappso_double getHyperscore() const;
  unsigned int getMatchedIons(PeptideIon ion_type) const;

  private:
  bool _refine_spectrum_synthesis;

  unsigned int
  getXtandemPredictedIonIntensityFactor(const QString &sequence,
                                        PeptideDirection ion_direction,
                                        unsigned int ion_size) const;

  pappso_double _proto_hyperscore;
  std::map<PeptideIon, unsigned int> _ion_count;

  static AaFactorMap _aa_ion_factor_y;
  static AaFactorMap _aa_ion_factor_b;
};


} // namespace pappso
