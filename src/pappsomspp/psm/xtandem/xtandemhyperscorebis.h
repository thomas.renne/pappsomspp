/**
 * \file pappsomspp/psm/xtandem/xtandemhyperscorebis.h
 * \date 8/9/2016
 * \author Olivier Langella
 * \brief computation of the X!Tandem hyperscore
 */

/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include "../../massspectrum/massspectrum.h"
#include "../../peptide/peptide.h"

namespace pappso
{

class XtandemHyperscoreBis
{
  using AaFactorMap = std::map<char, pappso_double>;

  private:
  unsigned int getXtandemPredictedIonIntensityFactor(const QString &sequence,
                                                     PeptideIon ion_type,
                                                     unsigned int size) const;
  bool m_isRefineSpectrumSynthesis;
  PrecisionPtr mp_precision;
  std::vector<PeptideIon> m_ionList;

  unsigned int m_totalMatchedIons                 = 0;
  unsigned int m_ionCount[PEPTIDE_ION_TYPE_COUNT] = {0};


  pappso_double m_protoHyperscore;

  static AaFactorMap m_aaIonFactorY;
  static AaFactorMap m_aaIonFactorBb;

  public:
  XtandemHyperscoreBis(bool refine_spectrum_synthesis,
                       PrecisionPtr precision,
                       const std::vector<PeptideIon> &ion_list);
  ~XtandemHyperscoreBis();

  pappso_double getHyperscore() const;
  unsigned int getMatchedIons(PeptideIon ion_type) const;


  void reset();

  bool computeXtandemHyperscore(const MassSpectrum &spectrum,
                                const Peptide &peptide,
                                unsigned int parent_charge);
  unsigned int getTotalMatchedIons() const;
};


} // namespace pappso
