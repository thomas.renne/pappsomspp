/**
 * \file pappsomspp/psm/xtandem/xtandemhyperscore.h
 * \date 16/8/2016
 * \author Olivier Langella
 * \brief process spectrum to compute X!Tandem hyperscore
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "../../massspectrum/massspectrum.h"
#include "../../peptide/peptide.h"
#include "../../processing/filters/filterpass.h"
#include "../../processing/filters/filterresample.h"
#include "../../processing/filters/filtertandemremovec13.h"

namespace pappso
{
class XtandemSpectrumProcess
{
  private:
  //    MassSpectrum spectrum_simple =
  //    _spectrum.getOriginalSpectrumSp()->applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);
  // pappso_double m_minimumMz = 150;
  FilterResampleKeepGreater m_filter_keep_greater;
  FilterGreatestY m_n_most_intense;
  FilterRescaleY m_filter_rescale;
  FilterTandemRemoveC13 m_filter_remove_c13;
  FilterFloorY m_filter_floor;
  FilterHighPass m_filter_highpass;
  bool m_isRemoveIsotope                               = true;
  bool m_isExcludeParent                               = false;
  pappso::pappso_double m_isExcludeParent_lower_dalton = 2;
  pappso::pappso_double m_isExcludeParent_upper_dalton = 2;
  bool m_isExcludeParent_neutral_loss                  = false;
  pappso::pappso_double m_neutralLossMass              = MASSH2O;
  pappso::pappso_double m_neutralLossWindowDalton      = 0.5;
  bool m_isRefineSpectrumModel                         = true;
  bool _y_ions                                         = true; // PeptideIon::y
  bool _ystar_ions = false; // PeptideIon::ystar
  bool _b_ions     = true;  // PeptideIon::b
  bool _bstar_ions = false; // PeptideIon::bstar
  bool _c_ions     = false; // PeptideIon::ystar
  bool _z_ions     = false; // PeptideIon::z
  bool _a_ions     = false; // PeptideIon::a
  bool _x_ions     = false; // CO2
  bool _astar_ions = false; // PeptideIon::a
  bool _ao_ions    = false; // PeptideIon::ao
  bool _bo_ions    = false; // PeptideIon::bo
  bool _yo_ions    = false; // PeptideIon::yo

  public:
  XtandemSpectrumProcess();
  XtandemSpectrumProcess(const XtandemSpectrumProcess &copy);
  ~XtandemSpectrumProcess();

  /** \brief process raw spectrum to prepare hyperscore computation
   */
  MassSpectrum process(const MassSpectrum &spectrum,
                       pappso_double parent_ion_mass,
                       unsigned int parent_charge) const;
  void setMinimumMz(pappso_double minimum_mz);
  void setNmostIntense(unsigned int nmost_intense);
  void setDynamicRange(pappso::pappso_double dynamic_range);
  void setRemoveIsotope(bool remove_isotope);
  void setExcludeParent(bool exclude_parent);
  void setExcludeParentNeutralLoss(bool neutral_loss);
  void setNeutralLossMass(pappso::pappso_double neutral_loss_mass);
  void setNeutralLossWindowDalton(pappso::pappso_double neutral_loss_precision);
  void setRefineSpectrumModel(bool refine);
  void setIonScore(PeptideIon ion_type, bool compute_score);

  pappso_double getMinimumMz() const;
  unsigned int getNmostIntense() const;
  pappso::pappso_double getDynamicRange() const;
  bool getRemoveIsotope() const;
  bool getExcludeParent() const;
  bool getExcludeParentNeutralLoss() const;
  pappso::pappso_double getNeutralLossMass() const;
  pappso::pappso_double getNeutralLossWindowDalton() const;
  bool getRefineSpectrumModel() const;
  bool getIonScore(PeptideIon ion_type) const;
};
} // namespace pappso
