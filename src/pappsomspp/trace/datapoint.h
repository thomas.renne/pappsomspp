#pragma once

#include <vector>
#include <limits>
#include <memory>

#include <QDataStream>

#include "../types.h"


namespace pappso
{
struct DataPoint;
typedef std::shared_ptr<const DataPoint> DataPointCstSPtr;

struct DataPoint
{
  pappso_double x = -1;
  pappso_double y = 0;

  DataPoint();
  DataPoint(const DataPoint &other);
  DataPoint(pappso_double x, pappso_double y);
  DataPoint(std::pair<pappso_double, pappso_double> pair);

  // For debugging purposes.
  //~DataPoint();

  DataPointCstSPtr makeDataPointCstSPtr() const;

  void initialize(pappso_double x, pappso_double y);
  void initialize(const DataPoint &other);
  bool initialize(const QString &text);

  void reset();

  void incrementX(pappso_double value);
  void incrementY(pappso_double value);

  bool operator==(const DataPoint &other) const;

  bool isValid() const;

  QString toString() const;
};

QDataStream &operator<<(QDataStream &out, const DataPoint &dataPoint);
QDataStream &operator>>(QDataStream &out, DataPoint &dataPoint);
} // namespace pappso

Q_DECLARE_METATYPE(pappso::DataPoint);
extern int dataPointMetaTypeId;

Q_DECLARE_METATYPE(pappso::DataPointCstSPtr);
extern int dataPointCstSPtrMetaTypeId;
