/**
 * \file utils/linearregression.h
 * \date 17/9/2016
 * \author Olivier Langella
 * \brief compute linear regression
 */

/*******************************************************************************
 * Copyright (c) 2016 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of peptider.
 *
 *     peptider is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     peptider is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with peptider.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/
#pragma once
#include "trace.h"

namespace pappso
{
class LinearRegression
{
  public:
  LinearRegression(const Trace &data);
  double getYfromX(double score) const;
  double getIntercept() const;
  double getSlope() const;

  /** @brief get Root-Mean-Square Deviation
   */
  double getRmsd(const Trace &data) const;
  /** @brief get Normalized Root-Mean-Square Deviation
   */
  double getNrmsd(const Trace &data) const;
  /** @brief get Coefficient of determination (R2)
   */
  double getCoefficientOfDetermination(const Trace &data) const;

  private:
  double _slope     = 0;
  double _intercept = 0;
};
} // namespace pappso
