#pragma once

#include <vector>
#include <memory>

#include <QDataStream>

#include "../types.h"
#include "datapoint.h"
#include "../mzrange.h"
#include "../processing/filters/filterinterface.h"

namespace pappso
{


class Trace;
QDataStream &operator<<(QDataStream &out, const Trace &trace);
QDataStream &operator>>(QDataStream &out, Trace &trace);

/** @brief find the first element in which X is equal or greater than the value
 * searched important : it implies that Trace is sorted by X
 * */
std::vector<DataPoint>::iterator
findFirstEqualOrGreaterX(std::vector<DataPoint>::iterator begin,
                         std::vector<DataPoint>::iterator end,
                         const double &value);

std::vector<DataPoint>::const_iterator
findFirstEqualOrGreaterX(std::vector<DataPoint>::const_iterator begin,
                         std::vector<DataPoint>::const_iterator end,
                         const double &value);

/** @brief find the first element in which Y is different of value
 * */
std::vector<DataPoint>::iterator
findDifferentYvalue(std::vector<DataPoint>::iterator begin,
                    std::vector<DataPoint>::iterator end,
                    const double &y_value);

std::vector<DataPoint>::const_iterator
findDifferentYvalue(std::vector<DataPoint>::const_iterator begin,
                    std::vector<DataPoint>::const_iterator end,
                    const double &y_value);

/** @brief find the first element in which X is greater than the value
 * searched important : it implies that Trace is sorted by X
 * */
std::vector<DataPoint>::iterator
findFirstGreaterX(std::vector<DataPoint>::iterator begin,
                  std::vector<DataPoint>::iterator end,
                  const double &value);

std::vector<DataPoint>::const_iterator
findFirstGreaterX(std::vector<DataPoint>::const_iterator begin,
                  std::vector<DataPoint>::const_iterator end,
                  const double &value);

/** @brief find the element with the smallest Y value (intensity)
 * */
std::vector<DataPoint>::const_iterator
minYDataPoint(std::vector<DataPoint>::const_iterator begin,
              std::vector<DataPoint>::const_iterator end);

/** @brief find the element with the greatest Y value (intensity)
 * */
std::vector<DataPoint>::iterator
maxYDataPoint(std::vector<DataPoint>::iterator begin,
              std::vector<DataPoint>::iterator end);
std::vector<DataPoint>::const_iterator
maxYDataPoint(std::vector<DataPoint>::const_iterator begin,
              std::vector<DataPoint>::const_iterator end);

/** @brief Move right to the lower value
 * */
std::vector<DataPoint>::const_iterator
moveLowerYRigthDataPoint(const Trace &trace,
                         std::vector<DataPoint>::const_iterator begin);
/** @brief Move left to the lower value
 * */
std::vector<DataPoint>::const_iterator
moveLowerYLeftDataPoint(const Trace &trace,
                        std::vector<DataPoint>::const_iterator begin);

/** @brief calculate the sum of y value of a trace
 * */
double sumYTrace(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end,
                 double init);

/** @brief calculate the mean of y value of a trace
 * */
double meanYTrace(std::vector<DataPoint>::const_iterator begin,
                  std::vector<DataPoint>::const_iterator end);

/** @brief calculate the median of y value of a trace
 * */
double medianYTrace(std::vector<DataPoint>::const_iterator begin,
                    std::vector<DataPoint>::const_iterator end);

/** @brief calculate the area of a trace
 * */
double areaTrace(std::vector<DataPoint>::const_iterator begin,
                 std::vector<DataPoint>::const_iterator end);

Trace flooredLocalMaxima(std::vector<DataPoint>::const_iterator begin,
                         std::vector<DataPoint>::const_iterator end,
                         double y_floor);

typedef std::shared_ptr<Trace> TraceSPtr;
typedef std::shared_ptr<const Trace> TraceCstSPtr;

class MapTrace;
class TraceCombiner;
class TracePlusCombiner;
class TraceMinusCombiner;

/**
 * \class Trace
 * \brief A simple container of DataPoint instances
 */
class Trace : public std::vector<DataPoint>
{

  friend class TraceCombiner;
  friend class TraceMinusCombiner;
  friend class TracePlusCombiner;

  friend class MassSpectrumCombinerInterface;

  public:
  Trace();
  Trace(const std::vector<std::pair<pappso_double, pappso_double>> &dataPoints);
  Trace(const std::vector<DataPoint> &dataPoints);
  Trace(const std::vector<DataPoint> &&dataPoints);
  explicit Trace(const MapTrace &map_trace);
  Trace(const Trace &other);
  Trace(const Trace &&other); // move constructor
  virtual ~Trace();

  size_t initialize(const std::vector<pappso_double> &xVector,
                    const std::vector<pappso_double> &yVector);

  size_t initialize(const Trace &other);

  size_t initialize(const std::map<pappso_double, pappso_double> &map);

  virtual Trace &operator=(const Trace &x);
  virtual Trace &operator=(Trace &&x);

  TraceSPtr makeTraceSPtr() const;
  TraceCstSPtr makeTraceCstSPtr() const;

  std::vector<pappso_double> xToVector() const;
  std::vector<pappso_double> yToVector() const;
  std::map<pappso_double, pappso_double> toMap() const;

  DataPoint containsX(pappso_double value,
                      PrecisionPtr precision_p = nullptr) const;

  // const Peak & Spectrum::getLowestIntensity() const;
  const DataPoint &minYDataPoint() const;

  // was const Peak & Spectrum::getMaxIntensity() const;
  const DataPoint &maxYDataPoint() const;

  pappso_double minY() const;
  pappso_double maxY() const;
  pappso_double sumY() const;
  pappso_double sumY(double mzStart, double mzEnd) const;

  // was void Spectrum::sortByMz();
  void sortX();
  void unique();


  std::vector<pappso_double> xValues();
  std::vector<pappso_double> yValues();

  /** @brief apply a filter on this trace
   * @param filter to process the signal
   * @return reference on the modified Trace
   */
  virtual Trace &filter(const FilterInterface &filter) final;
  QString toString() const;

  protected:
  //! Return a reference to the DataPoint instance that has its y member equal
  //! to \p value.
  // const DataPoint &dataPointWithX(pappso_double value) const;
  std::size_t dataPointIndexWithX(pappso_double value) const;
  std::vector<DataPoint>::iterator dataPointIteratorxWithX(pappso_double value);
  std::vector<DataPoint>::const_iterator
  dataPointCstIteratorxWithX(pappso_double value) const;
};


} // namespace pappso

Q_DECLARE_METATYPE(pappso::Trace);
Q_DECLARE_METATYPE(pappso::Trace *);

extern int traceMetaTypeId;
extern int tracePtrMetaTypeId;
