/**
 * \file pappsomspp/vendors/tims/timsbindec.h
 * \date 23/08/2019
 * \author Olivier Langella
 * \brief binary file handler of Bruker's TimsTof raw data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "timsbindec.h"
#include <pappsomspp/pappsoexception.h>
#include <QDataStream>
#include <zstd.h>

using namespace pappso;

TimsBinDec::TimsBinDec(const QFileInfo &timsBinFile, int timsCompressionType)
  : m_timsBinFile(timsBinFile.absoluteFilePath())
{

  if(timsCompressionType != 2)
    {
      throw pappso::PappsoException(
        QObject::tr("compression type %1 not handled by this library")
          .arg(timsCompressionType));
    }
  if(m_timsBinFile.isEmpty())
    {
      throw pappso::PappsoException(
        QObject::tr("No TIMS binary file name specified"));
    }
  QFile file(m_timsBinFile);
  if(!file.open(QIODevice::ReadOnly))
    {
      throw PappsoException(
        QObject::tr("ERROR opening TIMS binary file %1 for read")
          .arg(timsBinFile.absoluteFilePath()));
    }
}


TimsBinDec::TimsBinDec(const TimsBinDec &other)
  : m_timsBinFile(other.m_timsBinFile)
{
}

TimsBinDec::~TimsBinDec()
{
}


void
TimsBinDec::indexingFile()
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  QFile file(m_timsBinFile);
  QDataStream bin_in(&file);
  bin_in.setByteOrder(QDataStream::ByteOrder::LittleEndian);
  // m_indexArray.push_back(0);

  // QChar first_char;
  // txt_in >> first_char;
  quint32 number_in;
  qint64 position(0);
  qint8 trash;
  while(!bin_in.atEnd())
    {
      while((!bin_in.atEnd()) && (position % 4 != 0))
        {
          bin_in >> trash;
          position += 1;
          if(trash != 0)
            { /*
               throw PappsoException(
                 QObject::tr("ERROR reading TIMS frame %1 TIMS binary file %2: "
                             "trash%3 != 0")
                   .arg(m_indexArray.size())
                   .arg(m_timsBinFile.fileName())
                   .arg(trash));*/
            }
        }
      quint32 frame_len(0);
      while((!bin_in.atEnd()) && (frame_len == 0))
        {
          bin_in >> frame_len;
          // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
          //      << " i=" << i;
          position += 4;
        }

      // m_indexArray.push_back(position - 4);
      /*
            qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                     << " frame_len=" << frame_len
                     << " frameid=" << m_indexArray.size() - 1
                     << " back=" << m_indexArray.back() << " position=" <<
         position;


            quint64 next_frame = m_indexArray.back() + frame_len;
      */

      bin_in >> number_in;
      position += 4;
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " number_in(nb scans)=" << number_in
               << " position=" << position;
      ;
      /*

            while((!bin_in.atEnd()) && (position < next_frame))
              {
                bin_in >> trash;
                // qDebug() << __FILE__ << " " << __FUNCTION__ << " " <<
         __LINE__
                //      << " i=" << i;
                position += 1;
              }*/
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

TimsFrameSPtr
TimsBinDec::getTimsFrameSPtrByOffset(std::size_t timsId,
                                     std::size_t timsOffset) const
{
  qDebug() << "timsId:" << timsId << "timsOffset:" << timsOffset;

  // QMutexLocker locker(&m_mutex);
  QFile file(m_timsBinFile);
  if(!file.open(QIODevice::ReadOnly))
    {
      throw PappsoException(
        QObject::tr("ERROR opening TIMS binary file %1 for read")
          .arg(m_timsBinFile));
    }

  bool seekpos_ok = file.seek(timsOffset);
  if(!seekpos_ok)
    {
      throw PappsoException(
        QObject::tr("ERROR reading TIMS frame %1 TIMS binary file %2: "
                    "m_timsBinFile.seek(%3) failed")
          .arg(timsId)
          .arg(m_timsBinFile)
          .arg(timsOffset));
    }

  quint32 frame_length;
  file.read((char *)&frame_length, 4);
  // frame_length = qToBigEndian(frame_length);

  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //       << " frame_length=" << frame_length;

  quint32 scan_number;
  file.read((char *)&scan_number, 4);
  // scan_number = qToBigEndian(scan_number);
  frame_length -= 8;

  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //        << " pos=" << m_timsBinFile.pos();

  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //       << " scan_number=" << scan_number;
  // m_timsBinFile.seek(m_indexArray.at(timsId) + 8);


  qDebug();

  QByteArray frame_byte_array = file.read((qint64)frame_length + 2);

  file.close();

  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //        << " +frame_length-1="
  //      << (quint8) * (frame_byte_array.constData() + frame_length - 1);
  // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
  //       << " +frame_length="
  //     << (quint8) * (frame_byte_array.constData() + frame_length);
  // m_timsBinFile.seek(m_indexArray.at(timsId) + 8);

  if(frame_byte_array.size() - 2 != (qint64)frame_length)
    {
      throw PappsoException(
        QObject::tr("ERROR reading TIMS frame %1 TIMS binary file %2: "
                    "frame_byte_array.size()%3 != %4frame_length")
          .arg(timsId)
          .arg(m_timsBinFile)
          .arg(frame_byte_array.size())
          .arg(frame_length));
    }
  TimsFrameSPtr frame_sptr;
  if(frame_length > 0)
    {
      auto decompressed_size2 =
        ZSTD_getFrameContentSize(frame_byte_array.constData(), frame_length);

      if(decompressed_size2 == ZSTD_CONTENTSIZE_UNKNOWN)
        {
          throw PappsoException(
            QObject::tr("ERROR reading TIMS frame %1 TIMS binary file %2: "
                        " decompressed_size2 == ZSTD_CONTENTSIZE_UNKNOWN, "
                        "frame_length=%3")
              .arg(timsId)
              .arg(m_timsBinFile)
              .arg(frame_length));
        }

      if(decompressed_size2 == ZSTD_CONTENTSIZE_ERROR)
        {
          throw PappsoException(
            QObject::tr(
              "ERROR reading TIMS frame %1 TIMS binary file %2: "
              " decompressed_size2 == ZSTD_CONTENTSIZE_ERROR, frame_length=%3")
              .arg(timsId)
              .arg(m_timsBinFile)
              .arg(frame_length));
        }
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " decompressed_size2=" << decompressed_size2;

      char *uncompress = new char[decompressed_size2 + 10];
      std::size_t decompressed_size =
        ZSTD_decompress(uncompress,
                        decompressed_size2 + 10,
                        frame_byte_array.constData(),
                        frame_length);
      qDebug();

      if(decompressed_size != decompressed_size2)
        {
          throw PappsoException(
            QObject::tr("ERROR reading TIMS frame %1 TIMS binary file %2: "
                        "decompressed_size != decompressed_size2")
              .arg(timsId)
              .arg(m_timsBinFile)
              .arg(decompressed_size)
              .arg(decompressed_size2));
        }

      qDebug();

      frame_sptr = std::make_shared<TimsFrame>(
        timsId, scan_number, uncompress, decompressed_size);

      delete[] uncompress;
    }
  else
    {
      frame_sptr = std::make_shared<TimsFrame>(timsId, scan_number, nullptr, 0);
    }
  return frame_sptr;
}
/*
TimsFrameCstSPtr
TimsBinDec::getTimsFrameCstSPtr(std::size_t timsId)
{
  return getTimsFrameCstSPtrByOffset(timsId, m_indexArray[timsId]);
}
*/
