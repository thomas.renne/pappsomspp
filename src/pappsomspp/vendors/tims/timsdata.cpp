/**
 * \file pappsomspp/vendors/tims/timsdata.cpp
 * \date 27/08/2019
 * \author Olivier Langella
 * \brief main Tims data handler
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#include "timsdata.h"
#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotfound.h>
#include <pappsomspp/processing/combiners/tracepluscombiner.h>
#include <pappsomspp/processing/filters/filtertriangle.h>
#include <pappsomspp/processing/filters/filterpseudocentroid.h>
#include <pappsomspp/processing/filters/filterpass.h>
#include <QDebug>
#include <solvers.h>
#include <QSqlError>
#include <QSqlQuery>
#include <QMutexLocker>
#include <QThread>
#include <set>

using namespace pappso;

TimsData::TimsData(QDir timsDataDirectory)
  : m_timsDataDirectory(timsDataDirectory)
{

  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
  if(!m_timsDataDirectory.exists())
    {
      throw PappsoException(
        QObject::tr("ERROR TIMS data directory %1 not found")
          .arg(m_timsDataDirectory.absolutePath()));
    }

  if(!QFileInfo(m_timsDataDirectory.absoluteFilePath("analysis.tdf")).exists())
    {

      throw PappsoException(
        QObject::tr("ERROR TIMS data directory, %1 sqlite file not found")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf")));
    }

  // Open the database
  QSqlDatabase qdb = openDatabaseConnection();


  QSqlQuery q(qdb);
  if(!q.exec("select Key, Value from GlobalMetadata where "
             "Key='TimsCompressionType';"))
    {

      qDebug();
      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                    "command %2:\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(q.lastQuery())
          .arg(q.lastError().databaseText())
          .arg(q.lastError().driverText())
          .arg(q.lastError().nativeErrorCode()));
    }


  int compression_type = 0;
  if(q.next())
    {
      compression_type = q.value(1).toInt();
    }
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
           << " compression_type=" << compression_type;
  mpa_timsBinDec = new TimsBinDec(
    QFileInfo(m_timsDataDirectory.absoluteFilePath("analysis.tdf_bin")),
    compression_type);


  // get number of precursors
  if(!q.exec("SELECT COUNT( DISTINCT Id) FROM Precursors;"))
    {
      qDebug();
      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                    "command %2:\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  if(q.next())
    {
      m_totalNumberOfPrecursors = q.value(0).toLongLong();
    }


  // get number of scans
  if(!q.exec("SELECT SUM(NumScans) FROM Frames"))
    {
      qDebug();
      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                    "command %2:\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  if(q.next())
    {
      m_totalNumberOfScans = q.value(0).toLongLong();
    }

  /*
  std::shared_ptr<FilterTriangle> ms2filter =
    std::make_shared<FilterTriangle>();
  ms2filter.get()->setTriangleSlope(50, 0.02);
  mcsp_ms2Filter = ms2filter;
*/

  std::shared_ptr<pappso::FilterPseudoCentroid> ms2filter =
    std::make_shared<pappso::FilterPseudoCentroid>(20000, 0.05, 0.5, 0.1);
  mcsp_ms2Filter = ms2filter;


  std::shared_ptr<FilterTriangle> ms1filter =
    std::make_shared<FilterTriangle>();
  ms1filter.get()->setTriangleSlope(50, 0.01);
  mcsp_ms1Filter = ms1filter;
}

QSqlDatabase
TimsData::openDatabaseConnection() const
{
  QString database_connection_name = QString("%1_%2")
                                       .arg(m_timsDataDirectory.absolutePath())
                                       .arg((quintptr)QThread::currentThread());
  // Open the database
  QSqlDatabase qdb = QSqlDatabase::database(database_connection_name);
  if(!qdb.isValid())
    {
      qDebug() << database_connection_name;
      qdb = QSqlDatabase::addDatabase("QSQLITE", database_connection_name);
      qdb.setDatabaseName(m_timsDataDirectory.absoluteFilePath("analysis.tdf"));
    }


  if(!qdb.open())
    {
      qDebug();
      throw PappsoException(
        QObject::tr("ERROR opening TIMS sqlite database file %1, database name "
                    "%2 :\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(database_connection_name)
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  return qdb;
}

TimsData::TimsData(const pappso::TimsData &other)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
}

TimsData::~TimsData()
{
  // m_qdb.close();
  if(mpa_timsBinDec != nullptr)
    {
      delete mpa_timsBinDec;
    }
}

std::pair<std::size_t, std::size_t>
TimsData::getScanCoordinateFromRawIndex(std::size_t raw_index) const
{

  QSqlDatabase qdb = openDatabaseConnection();

  QSqlQuery q =
    qdb.exec(QString("SELECT Id, NumScans FROM "
                     "Frames ORDER BY Id"));
  if(q.lastError().isValid())
    {

      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                    "command %2:\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  pappso::TimsFrameSPtr tims_frame;
  bool index_found = false;
  std::size_t timsId;
  std::size_t numberScans;
  std::size_t offset = 0;
  while(q.next() && (!index_found))
    {
      timsId      = q.value(0).toUInt();
      numberScans = q.value(1).toUInt();

      if(raw_index < (offset + numberScans))
        {
          return std::pair<std::size_t, std::size_t>(timsId,
                                                     raw_index - offset);
        }

      offset += numberScans;
    }

  throw ExceptionNotFound(
    QObject::tr("ERROR raw index %1 not found").arg(raw_index));
}


std::size_t
TimsData::getRawIndexFromCoordinate(std::size_t frame_id,
                                    std::size_t scan_num) const
{


  QSqlDatabase qdb = openDatabaseConnection();
  QSqlQuery q =
    qdb.exec(QString("SELECT Id, NumScans FROM "
                     "Frames ORDER BY Id"));
  if(q.lastError().isValid())
    {

      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                    "command %2:\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  bool index_found = false;
  std::size_t timsId;
  std::size_t numberScans;
  std::size_t offset = 0;
  while(q.next() && (!index_found))
    {
      timsId      = q.value(0).toUInt();
      numberScans = q.value(1).toUInt();

      if(timsId == frame_id)
        {
          return offset + scan_num;
        }

      offset += numberScans;
    }

  throw ExceptionNotFound(
    QObject::tr("ERROR raw index with frame=%1 scan=%2 not found")
      .arg(frame_id)
      .arg(scan_num));
}

/** @brief get a mass spectrum given its spectrum index
 * @param raw_index a number begining at 0, corresponding to a Tims Scan in
 * the order they lies in the binary data file
 */
pappso::MassSpectrumCstSPtr
TimsData::getMassSpectrumCstSPtrByRawIndex(std::size_t raw_index)
{

  auto coordinate = getScanCoordinateFromRawIndex(raw_index);
  return getMassSpectrumCstSPtr(coordinate.first, coordinate.second);
}


TimsFrameBaseCstSPtr
TimsData::getTimsFrameBaseCstSPtr(std::size_t timsId) const
{

  qDebug() << " timsId=" << timsId;

  QSqlDatabase qdb = openDatabaseConnection();
  QSqlQuery q      = qdb.exec(
    QString("SELECT Frames.TimsId, Frames.AccumulationTime, "
            " MzCalibration.DigitizerTimebase, MzCalibration.DigitizerDelay, "
            "MzCalibration.C0, MzCalibration.C1, MzCalibration.C2, "
            "MzCalibration.C3, MzCalibration.T1, MzCalibration.T2, "
            "MzCalibration.dC1, MzCalibration.dC2, Frames.T1, Frames.T2, "
            "Frames.Time, Frames.MsMsType, TimsCalibration.ModelType, "
            "TimsCalibration.C0, TimsCalibration.C1, TimsCalibration.C2, "
            "TimsCalibration.C3, TimsCalibration.C4, TimsCalibration.C5, "
            "TimsCalibration.C6, TimsCalibration.C7, TimsCalibration.C8, "
            "TimsCalibration.C9   FROM "
            "Frames INNER JOIN MzCalibration ON "
            "Frames.MzCalibration=MzCalibration.Id INNER JOIN TimsCalibration "
            "ON Frames.TimsCalibration=TimsCalibration.Id where "
            "Frames.Id=%1;")
      .arg(timsId));
  if(q.lastError().isValid())
    {

      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, database name %2, "
                    "executing SQL "
                    "command %3:\n%4\n%5\n%6")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(qdb.databaseName())
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  pappso::TimsFrameBaseSPtr tims_frame;
  if(q.next())
    {


      double T1_ref  = q.value(8).toDouble();
      double T2_ref  = q.value(9).toDouble();
      double factor1 = q.value(10).toDouble();
      double factor2 = q.value(11).toDouble();

      double T1_frame = q.value(12).toDouble();
      double T2_frame = q.value(13).toDouble();


      double temperature_correction =
        factor1 * (T1_ref - T1_frame) + factor2 * (T2_ref - T2_frame);
      temperature_correction = (double)1.0 + (temperature_correction / 1.0e6);

      tims_frame = std::make_shared<TimsFrameBase>(
        TimsFrameBase(timsId, q.value(0).toUInt()));

      tims_frame.get()->setMzCalibration(temperature_correction,
                                         q.value(2).toDouble(),
                                         q.value(3).toDouble(),
                                         q.value(4).toDouble(),
                                         q.value(5).toDouble(),
                                         q.value(6).toDouble(),
                                         q.value(7).toDouble());

      tims_frame.get()->setAccumulationTime(q.value(1).toDouble());

      tims_frame.get()->setTime(q.value(14).toDouble());
      tims_frame.get()->setMsMsType(q.value(15).toUInt());

      tims_frame.get()->setTimsCalibration(q.value(16).toInt(),
                                           q.value(17).toDouble(),
                                           q.value(18).toDouble(),
                                           q.value(19).toDouble(),
                                           q.value(20).toDouble(),
                                           q.value(21).toDouble(),
                                           q.value(22).toDouble(),
                                           q.value(23).toDouble(),
                                           q.value(24).toDouble(),
                                           q.value(25).toDouble(),
                                           q.value(26).toDouble());
      return tims_frame;
    }

  throw ExceptionNotFound(QObject::tr("ERROR timsId %1 not found").arg(timsId));
  // return TimsFrameCstSPtr;
}

TimsFrameCstSPtr
TimsData::getTimsFrameCstSPtr(std::size_t timsId) const
{

  qDebug() << " timsId=" << timsId;

  QSqlDatabase qdb = openDatabaseConnection();
  QSqlQuery q      = qdb.exec(
    QString("SELECT Frames.TimsId, Frames.AccumulationTime, "
            " MzCalibration.DigitizerTimebase, MzCalibration.DigitizerDelay, "
            "MzCalibration.C0, MzCalibration.C1, MzCalibration.C2, "
            "MzCalibration.C3, MzCalibration.T1, MzCalibration.T2, "
            "MzCalibration.dC1, MzCalibration.dC2, Frames.T1, Frames.T2, "
            "Frames.Time, Frames.MsMsType, TimsCalibration.ModelType, "
            "TimsCalibration.C0, TimsCalibration.C1, TimsCalibration.C2, "
            "TimsCalibration.C3, TimsCalibration.C4, TimsCalibration.C5, "
            "TimsCalibration.C6, TimsCalibration.C7, TimsCalibration.C8, "
            "TimsCalibration.C9   FROM "
            "Frames INNER JOIN MzCalibration ON "
            "Frames.MzCalibration=MzCalibration.Id INNER JOIN TimsCalibration "
            "ON Frames.TimsCalibration=TimsCalibration.Id where "
            "Frames.Id=%1;")
      .arg(timsId));
  if(q.lastError().isValid())
    {

      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, database name %2, "
                    "executing SQL "
                    "command %3:\n%4\n%5\n%6")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(qdb.databaseName())
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  pappso::TimsFrameSPtr tims_frame;
  if(q.next())
    {


      double T1_ref  = q.value(8).toDouble();
      double T2_ref  = q.value(9).toDouble();
      double factor1 = q.value(10).toDouble();
      double factor2 = q.value(11).toDouble();

      double T1_frame = q.value(12).toDouble();
      double T2_frame = q.value(13).toDouble();


      double temperature_correction =
        factor1 * (T1_ref - T1_frame) + factor2 * (T2_ref - T2_frame);
      temperature_correction = (double)1.0 + (temperature_correction / 1.0e6);

      tims_frame =
        mpa_timsBinDec->getTimsFrameSPtrByOffset(timsId, q.value(0).toUInt());

      tims_frame.get()->setMzCalibration(temperature_correction,
                                         q.value(2).toDouble(),
                                         q.value(3).toDouble(),
                                         q.value(4).toDouble(),
                                         q.value(5).toDouble(),
                                         q.value(6).toDouble(),
                                         q.value(7).toDouble());

      tims_frame.get()->setAccumulationTime(q.value(1).toDouble());

      tims_frame.get()->setTime(q.value(14).toDouble());
      tims_frame.get()->setMsMsType(q.value(15).toUInt());

      tims_frame.get()->setTimsCalibration(q.value(16).toInt(),
                                           q.value(17).toDouble(),
                                           q.value(18).toDouble(),
                                           q.value(19).toDouble(),
                                           q.value(20).toDouble(),
                                           q.value(21).toDouble(),
                                           q.value(22).toDouble(),
                                           q.value(23).toDouble(),
                                           q.value(24).toDouble(),
                                           q.value(25).toDouble(),
                                           q.value(26).toDouble());
      return tims_frame;
    }

  throw ExceptionNotFound(QObject::tr("ERROR timsId %1 not found").arg(timsId));
  // return TimsFrameCstSPtr;
}


pappso::MassSpectrumCstSPtr
TimsData::getMassSpectrumCstSPtr(std::size_t timsId, std::size_t scanNum)
{
  pappso::TimsFrameCstSPtr frame = getTimsFrameCstSPtrCached(timsId);

  return frame->getMassSpectrumCstSPtr(scanNum);
}

std::size_t
TimsData::getTotalNumberOfScans() const
{
  return m_totalNumberOfScans;
}


std::size_t
TimsData::getTotalNumberOfPrecursors() const
{
  return m_totalNumberOfPrecursors;
}

unsigned int
TimsData::getMsLevelBySpectrumIndex(std::size_t spectrum_index)
{
  auto coordinate = getScanCoordinateFromRawIndex(spectrum_index);
  auto tims_frame = getTimsFrameCstSPtrCached(coordinate.first);
  return tims_frame.get()->getMsLevel();
}


QualifiedMassSpectrum
TimsData::getQualifiedMassSpectrumByRawIndex(std::size_t spectrum_index,
                                             bool want_binary_data)
{
  auto coordinate = getScanCoordinateFromRawIndex(spectrum_index);
  TimsFrameBaseCstSPtr tims_frame;
  if(want_binary_data)
    {
      tims_frame = getTimsFrameCstSPtrCached(coordinate.first);
    }
  else
    {
      tims_frame = getTimsFrameBaseCstSPtrCached(coordinate.first);
    }
  QualifiedMassSpectrum mass_spectrum;
  MassSpectrumId spectrum_id;

  spectrum_id.setSpectrumIndex(spectrum_index);
  spectrum_id.setNativeId(QString("frame=%1 scan=%2 index=%3")
                            .arg(coordinate.first)
                            .arg(coordinate.second)
                            .arg(spectrum_index));

  mass_spectrum.setMassSpectrumId(spectrum_id);

  mass_spectrum.setMsLevel(tims_frame.get()->getMsLevel());
  mass_spectrum.setRtInSeconds(tims_frame.get()->getTime());

  mass_spectrum.setPrecursorCharge(0);
  mass_spectrum.setPrecursorMz(0);
  mass_spectrum.setPrecursorIntensity(0);

  mass_spectrum.setDtInMilliSeconds(
    tims_frame.get()->getTimeOfFlight(coordinate.second));
  // 1/K0
  mass_spectrum.setParameterValue(
    QualifiedMassSpectrumParameter::OneOverK0,
    tims_frame.get()->getOneOverK0Transformation(coordinate.second));

  mass_spectrum.setEmptyMassSpectrum(true);
  if(want_binary_data)
    {
      mass_spectrum.setMassSpectrumSPtr(
        tims_frame.get()->getMassSpectrumSPtr(coordinate.second));
      if(mass_spectrum.size() > 0)
        {
          mass_spectrum.setEmptyMassSpectrum(false);
        }
    }
  else
    {
      // if(tims_frame.get()->getNbrPeaks(coordinate.second) > 0)
      //{
      mass_spectrum.setEmptyMassSpectrum(false);
      // }
    }
  if(tims_frame.get()->getMsLevel() > 1)
    {

      QSqlDatabase qdb = openDatabaseConnection();
      QSqlQuery q      = qdb.exec(
        QString(
          "SELECT PasefFrameMsMsInfo.*, Precursors.* FROM "
          "PasefFrameMsMsInfo INNER JOIN Precursors ON "
          "PasefFrameMsMsInfo.Precursor=Precursors.Id where "
          "PasefFrameMsMsInfo.Frame=%1 and (PasefFrameMsMsInfo.ScanNumBegin "
          "<= %2 and PasefFrameMsMsInfo.ScanNumEnd >= %2);")
          .arg(coordinate.first)
          .arg(coordinate.second));
      if(q.lastError().isValid())
        {
          throw PappsoException(
            QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                        "command %2:\n%3\n%4\n%5")
              .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
              .arg(q.lastQuery())
              .arg(qdb.lastError().databaseText())
              .arg(qdb.lastError().driverText())
              .arg(qdb.lastError().nativeErrorCode()));
        }
      if(q.next())
        {
          mass_spectrum.setPrecursorCharge(q.value(11).toInt());
          mass_spectrum.setPrecursorMz(q.value(10).toDouble());
          mass_spectrum.setPrecursorIntensity(q.value(13).toDouble());
          // mass_spectrum.setPrecursorSpectrumIndex();


          MassSpectrumId spectrum_id;
          std::size_t prec_spectrum_index = getRawIndexFromCoordinate(
            q.value(14).toDouble(), coordinate.second);

          mass_spectrum.setPrecursorSpectrumIndex(prec_spectrum_index);
          mass_spectrum.setPrecursorNativeId(
            QString("frame=%1 scan=%2 index=%3")
              .arg(q.value(14).toDouble())
              .arg(coordinate.second)
              .arg(prec_spectrum_index));

          mass_spectrum.setParameterValue(
            QualifiedMassSpectrumParameter::IsolationMz, q.value(3).toDouble());
          mass_spectrum.setParameterValue(
            QualifiedMassSpectrumParameter::IsolationWidth,
            q.value(4).toDouble());

          mass_spectrum.setParameterValue(
            QualifiedMassSpectrumParameter::CollisionEnergy,
            q.value(5).toFloat());
          mass_spectrum.setParameterValue(
            QualifiedMassSpectrumParameter::BrukerPrecursorIndex,
            q.value(6).toInt());
        }
    }

  return mass_spectrum;
}


QualifiedMassSpectrum
TimsData::getQualifiedMs1MassSpectrumByPrecursorId(std::size_t ms2_index,
                                                   std::size_t precursor_index,
                                                   bool want_binary_data)
{
  QualifiedMassSpectrum mass_spectrum;


  QSqlDatabase qdb = openDatabaseConnection();
  mass_spectrum.setMsLevel(1);
  mass_spectrum.setPrecursorCharge(0);
  mass_spectrum.setPrecursorMz(0);
  mass_spectrum.setPrecursorIntensity(0);
  mass_spectrum.setPrecursorSpectrumIndex(0);
  mass_spectrum.setEmptyMassSpectrum(true);
  QSqlQuery q =
    qdb.exec(QString("SELECT PasefFrameMsMsInfo.*, Precursors.* FROM "
                     "PasefFrameMsMsInfo INNER JOIN Precursors ON "
                     "PasefFrameMsMsInfo.Precursor=Precursors.Id where "
                     "Precursors.Id=%1;")
               .arg(precursor_index));
  if(q.lastError().isValid())
    {

      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                    "command %2:\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }
  if(q.size() == 0)
    {

      throw ExceptionNotFound(
        QObject::tr("ERROR in getQualifiedMassSpectrumByPrecursorId, precursor "
                    "id=%1 not found")
          .arg(precursor_index));
    }
  else
    {
      TracePlusCombiner combiner;
      MapTrace combiner_result;


      bool first                      = true;
      std::size_t scan_mobility_start = 0;
      std::size_t scan_mobility_end   = 0;
      std::set<std::size_t> tims_frame_list;
      while(q.next())
        {
          // get MS1 frame
          tims_frame_list.insert(q.value(14).toLongLong());
          if(first)
            {


              MassSpectrumId spectrum_id;

              spectrum_id.setSpectrumIndex(precursor_index);
              spectrum_id.setNativeId(
                QString("frame=%1 begin=%2 end=%3 precursor=%4 idxms2=%5")
                  .arg(q.value(0).toLongLong())
                  .arg(q.value(1).toLongLong())
                  .arg(q.value(2).toLongLong())
                  .arg(precursor_index)
                  .arg(ms2_index));


              mass_spectrum.setMassSpectrumId(spectrum_id);


              scan_mobility_start = q.value(1).toLongLong();
              scan_mobility_end   = q.value(2).toLongLong();

              first = false;
            }
        }

      first = true;
      for(std::size_t tims_id : tims_frame_list)
        {
          TimsFrameBaseCstSPtr tims_frame = getTimsFrameCstSPtrCached(tims_id);
          if(first)
            {
              mass_spectrum.setRtInSeconds(tims_frame.get()->getTime());

              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::OneOverK0begin,
                tims_frame.get()->getOneOverK0Transformation(
                  scan_mobility_start));

              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::OneOverK0end,
                tims_frame.get()->getOneOverK0Transformation(
                  scan_mobility_end));

              first = false;
            }


          if(want_binary_data)
            {
              combiner.combine(combiner_result,
                               tims_frame.get()->cumulateScanToTrace(
                                 scan_mobility_start, scan_mobility_end));
            }
          else
            {
              break;
            }
        }


      if(first == true)
        {
          throw ExceptionNotFound(
            QObject::tr(
              "ERROR in getQualifiedMassSpectrumByPrecursorId, precursor "
              "id=%1 not found")
              .arg(precursor_index));
        }


      if(want_binary_data)
        {

          pappso::Trace trace(combiner_result);
          qDebug();

          if(trace.size() > 0)
            {
              if(mcsp_ms1Filter != nullptr)
                {
                  mcsp_ms1Filter->filter(trace);
                }

              qDebug();
              mass_spectrum.setMassSpectrumSPtr(
                MassSpectrum(trace).makeMassSpectrumSPtr());
              mass_spectrum.setEmptyMassSpectrum(false);
            }
          else
            {
              mass_spectrum.setMassSpectrumSPtr(nullptr);
              mass_spectrum.setEmptyMassSpectrum(true);
            }
        }
    }
  return mass_spectrum;
}


void
TimsData::getQualifiedMs2MassSpectrumByPrecursorId(
  QualifiedMassSpectrum &mass_spectrum,
  std::size_t ms2_index,
  std::size_t precursor_index,
  bool want_binary_data)
{
  QSqlDatabase qdb = openDatabaseConnection();
  MassSpectrumId spectrum_id;

  spectrum_id.setSpectrumIndex(precursor_index);
  spectrum_id.setNativeId(
    QString("precursor=%1 idxms2=%2").arg(precursor_index).arg(ms2_index));

  mass_spectrum.setMassSpectrumId(spectrum_id);

  mass_spectrum.setMsLevel(2);
  mass_spectrum.setPrecursorCharge(0);
  mass_spectrum.setPrecursorMz(0);
  mass_spectrum.setPrecursorIntensity(0);
  mass_spectrum.setPrecursorSpectrumIndex(ms2_index - 1);

  mass_spectrum.setEmptyMassSpectrum(true);

  qdb = openDatabaseConnection();
  // m_mutex.lock();
  QSqlQuery q =
    qdb.exec(QString("SELECT PasefFrameMsMsInfo.*, Precursors.* FROM "
                     "PasefFrameMsMsInfo INNER JOIN Precursors ON "
                     "PasefFrameMsMsInfo.Precursor=Precursors.Id where "
                     "Precursors.Id=%1;")
               .arg(precursor_index));
  if(q.lastError().isValid())
    {
      qDebug();
      throw PappsoException(
        QObject::tr("ERROR in TIMS sqlite database file %1, executing SQL "
                    "command %2:\n%3\n%4\n%5")
          .arg(m_timsDataDirectory.absoluteFilePath("analysis.tdf"))
          .arg(q.lastQuery())
          .arg(qdb.lastError().databaseText())
          .arg(qdb.lastError().driverText())
          .arg(qdb.lastError().nativeErrorCode()));
    }

  // m_mutex.unlock();
  if(q.size() == 0)
    {

      throw ExceptionNotFound(
        QObject::tr("ERROR in getQualifiedMassSpectrumByPrecursorId, precursor "
                    "id=%1 not found")
          .arg(precursor_index));
    }
  else
    {
      //  qDebug() << " q.size()="<< q.size();

      bool first                      = true;
      std::size_t scan_mobility_start = 0;
      std::size_t scan_mobility_end   = 0;
      std::vector<std::size_t> tims_frame_list;

      while(q.next())
        {
          tims_frame_list.push_back(q.value(0).toLongLong());
          if(first)
            {
              mass_spectrum.setPrecursorCharge(q.value(11).toInt());
              mass_spectrum.setPrecursorMz(q.value(10).toDouble());
              mass_spectrum.setPrecursorIntensity(q.value(13).toDouble());

              mass_spectrum.setPrecursorNativeId(
                QString("frame=%1 begin=%2 end=%3 precursor=%4 idxms2=%5")
                  .arg(q.value(14).toLongLong())
                  .arg(q.value(1).toLongLong())
                  .arg(q.value(2).toLongLong())
                  .arg(precursor_index)
                  .arg(ms2_index - 1));
              // mass_spectrum.setPrecursorSpectrumIndex();

              scan_mobility_start = q.value(1).toLongLong();
              scan_mobility_end   = q.value(2).toLongLong();

              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::IsolationMz,
                q.value(3).toDouble());
              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::IsolationWidth,
                q.value(4).toDouble());

              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::CollisionEnergy,
                q.value(5).toFloat());
              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::BrukerPrecursorIndex,
                q.value(6).toInt());

              first = false;
            }
        }
      // QMutexLocker locker(&m_mutex_spectrum);

      pappso::TimsFrameCstSPtr tims_frame;
      TracePlusCombiner combiner;
      MapTrace combiner_result;
      first = true;
      for(std::size_t tims_id : tims_frame_list)
        {
          tims_frame = getTimsFrameCstSPtrCached(tims_id);
          if(first)
            {
              mass_spectrum.setRtInSeconds(tims_frame.get()->getTime());

              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::OneOverK0begin,
                tims_frame.get()->getOneOverK0Transformation(
                  scan_mobility_start));

              mass_spectrum.setParameterValue(
                QualifiedMassSpectrumParameter::OneOverK0end,
                tims_frame.get()->getOneOverK0Transformation(
                  scan_mobility_end));

              first = false;
            }


          if(want_binary_data)
            {
              combiner.combine(combiner_result,
                               tims_frame.get()->cumulateScanToTrace(
                                 scan_mobility_start, scan_mobility_end));
            }
        }
      qDebug() << " precursor_index=" << precursor_index
               << " num_rows=" << tims_frame_list.size()
               << " sql=" << q.lastQuery() << " "
               << (long)QThread::currentThreadId();
      if(first == true)
        {
          throw ExceptionNotFound(
            QObject::tr(
              "ERROR in getQualifiedMassSpectrumByPrecursorId, precursor "
              "id=%1 not found")
              .arg(precursor_index));
        }
      if(want_binary_data)
        {
          qDebug();
          // peak_pick.filter(trace);

          pappso::Trace trace(combiner_result);
          qDebug();

          if(trace.size() > 0)
            {
              qDebug() << trace.size() << " "
                       << (long)QThread::currentThreadId();

              if(mcsp_ms2Filter != nullptr)
                {
                  // FilterTriangle filter;
                  // filter.setTriangleSlope(50, 0.02);
                  // filter.filter(trace);
                  mcsp_ms2Filter->filter(trace);
                }

              // FilterScaleFactorY filter_scale((double)1 /
              //                                 (double)tims_frame_list.size());
              // filter_scale.filter(trace);
              qDebug();
              mass_spectrum.setMassSpectrumSPtr(
                MassSpectrum(trace).makeMassSpectrumSPtr());
              mass_spectrum.setEmptyMassSpectrum(false);
            }
          else
            {
              mass_spectrum.setMassSpectrumSPtr(nullptr);
              mass_spectrum.setEmptyMassSpectrum(true);
            }

          qDebug();
        }
      qDebug();
    }
  qDebug();
}


TimsFrameBaseCstSPtr
TimsData::getTimsFrameBaseCstSPtrCached(std::size_t timsId)
{
  QMutexLocker locker(&m_mutex);
  for(auto &tims_frame : m_timsFrameBaseCache)
    {
      if(tims_frame.get()->getId() == timsId)
        {
          m_timsFrameBaseCache.push_back(tims_frame);
          return tims_frame;
        }
    }

  m_timsFrameBaseCache.push_back(getTimsFrameBaseCstSPtr(timsId));
  if(m_timsFrameBaseCache.size() > m_cacheSize)
    m_timsFrameBaseCache.pop_front();
  return m_timsFrameBaseCache.back();
}

TimsFrameCstSPtr
TimsData::getTimsFrameCstSPtrCached(std::size_t timsId)
{
  QMutexLocker locker(&m_mutex);
  for(auto &tims_frame : m_timsFrameCache)
    {
      if(tims_frame.get()->getId() == timsId)
        {
          m_timsFrameCache.push_back(tims_frame);
          return tims_frame;
        }
    }

  m_timsFrameCache.push_back(getTimsFrameCstSPtr(timsId));
  if(m_timsFrameCache.size() > m_cacheSize)
    m_timsFrameCache.pop_front();
  return m_timsFrameCache.back();
}

void
TimsData::setMs2FilterCstSPtr(pappso::FilterInterfaceCstSPtr &filter)
{
  mcsp_ms2Filter = filter;
}
void
TimsData::setMs1FilterCstSPtr(pappso::FilterInterfaceCstSPtr &filter)
{
  mcsp_ms1Filter = filter;
}
