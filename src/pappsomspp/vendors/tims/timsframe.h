/**
 * \file pappsomspp/vendors/tims/timsframe.h
 * \date 23/08/2019
 * \author Olivier Langella
 * \brief handle a single Bruker's TimsTof frame
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <memory>
#include <QByteArray>
#include <vector>
#include <pappsomspp/massspectrum/massspectrum.h>
#include "timsframebase.h"

namespace pappso
{

class TimsFrame;
typedef std::shared_ptr<TimsFrame> TimsFrameSPtr;
typedef std::shared_ptr<const TimsFrame> TimsFrameCstSPtr;

class TimsBinDec;

/**
 * @todo write docs
 */
class TimsFrame : public TimsFrameBase
{
  public:
  /**
   * Default constructor
   */
  TimsFrame(std::size_t timsId,
            quint32 scanNum,
            char *p_bytes,
            std::size_t len);
  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TimsFrame(const TimsFrame &other);

  /**
   * Destructor
   */
  ~TimsFrame();


  virtual std::size_t getNbrPeaks(std::size_t scanNum) const override;

  /** @brief cumulate scan list into a trace
   */
  virtual Trace cumulateScanToTrace(std::size_t scanNumBegin,
                                    std::size_t scanNumEnd) const override;

  /** @brief get raw index list for one given scan
   * index are not TOF nor m/z, just index on digitizer
   */
  std::vector<quint32> getScanIndexList(std::size_t scanNum) const;

  /** @brief get raw intensities without transformation from one scan
   * it needs intensity normalization
   */
  std::vector<quint32> getScanIntensities(std::size_t scanNum) const;

  /** @brief get the mass spectrum corresponding to a scan number
   * @param scanNum the scan number to retrieve
   * */
  pappso::MassSpectrumCstSPtr getMassSpectrumCstSPtr(std::size_t scanNum) const;
  virtual pappso::MassSpectrumSPtr
  getMassSpectrumSPtr(std::size_t scanNum) const override;


  /** @brief transform accumulation of raw scans into a real mass spectrum
   */
  pappso::Trace getTraceFromCumulatedScans(
    std::map<quint32, quint32> &accumulated_scans) const;


  private:
  void unshufflePacket(const char *src);

  std::size_t getScanOffset(std::size_t scanNum) const;


  /** @brief cumulate a scan into a map
   */
  void cumulateScan(std::size_t scanNum,
                    std::map<quint32, quint32> &accumulate_into) const;


  private:
  QByteArray m_timsDataFrame;
};
} // namespace pappso
