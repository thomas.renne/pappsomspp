/**
 * \file pappsomspp/vendors/tims/timsframebase.h
 * \date 16/12/2019
 * \author Olivier Langella
 * \brief handle a single Bruker's TimsTof frame without binary data
 */

/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once

#include <memory>
#include <vector>
#include <QtGlobal>
#include <pappsomspp/massspectrum/massspectrum.h>

namespace pappso
{

class TimsFrameBase;
typedef std::shared_ptr<TimsFrameBase> TimsFrameBaseSPtr;
typedef std::shared_ptr<const TimsFrameBase> TimsFrameBaseCstSPtr;


/**
 * @todo write docs
 */
class TimsFrameBase
{
  public:
  /** @brief constructor for binary independant tims frame
   * @param timsId tims frame identifier in the database
   * @param scanNum the total number of scans contained in this frame
   */
  TimsFrameBase(std::size_t timsId, quint32 scanNum);
  /**
   * Copy constructor
   *
   * @param other TODO
   */
  TimsFrameBase(const TimsFrameBase &other);

  /**
   * Destructor
   */
  ~TimsFrameBase();


  virtual std::size_t getNbrPeaks(std::size_t scanNum) const;
  virtual MassSpectrumSPtr getMassSpectrumSPtr(std::size_t scanNum) const;
  virtual Trace cumulateScanToTrace(std::size_t scanNumBegin,
                                    std::size_t scanNumEnd) const;

  bool checkScanNum(std::size_t scanNum) const;


  void setAccumulationTime(double accumulation_time_ms);
  void setMzCalibration(double temperature_correction,
                        double digitizerTimebase,
                        double digitizerDelay,
                        double C0,
                        double C1,
                        double C2,
                        double C3);
  void setTimsCalibration(int tims_model_type,
                          double C0,
                          double C1,
                          double C2,
                          double C3,
                          double C4,
                          double C5,
                          double C6,
                          double C7,
                          double C8,
                          double C9);
  void setTime(double time);
  void setMsMsType(quint8 type);
  unsigned int getMsLevel() const;
  double getTime() const;

  std::size_t getId() const;

  /** @brief get time of flight of a scan number in milliseconds
   * */
  double getTimeOfFlight(std::size_t scanNum) const;
  double getOneOverK0Transformation(std::size_t scanNum) const;

  protected:
  double getVoltageTransformation(std::size_t scanNum) const;
  /** @brief get time of flight from raw index
   */
  double getTofFromIndex(quint32 index) const;

  /** @brief get time of flight from double index
   */
  double getTofFromIndex(double index) const;


  /** @brief get m/z from time of flight
   */
  double getMzFromTof(double tof) const;

  protected:
  /** @brief total number of scans contained in this frame
   */
  quint32 m_scanNumber;

  /** @brief Tims frame database id (the SQL identifier of this frame)
   * @warning in sqlite, there is another field called TimsId : this is not
   * that, because it is in fact an offset in bytes in the binary file.
   * */
  std::size_t m_timsId;

  /** @brief accumulation time in milliseconds
   */
  double m_accumulationTime = 0;

  double m_digitizerTimebase = 0;
  double m_digitizerDelay    = 0;

  /** @brief MZ calibration parameters
   */
  std::vector<double> m_mzCalibrationArr;

  quint8 m_msMsType = 0;
  double m_time     = 0;

  double m_timsDvStart = 0; // C2 from TimsCalibration
  double m_timsSlope =
    0; // (dv_end - dv_start) / ncycles  //C3 from TimsCalibration // C2 from
       // TimsCalibration // C1 from TimsCalibration
  double m_timsTtrans = 0; // C4 from TimsCalibration
  double m_timsNdelay = 0; // C0 from TimsCalibration
  double m_timsVmin   = 0; // C8 from TimsCalibration
  double m_timsVmax   = 0; // C9 from TimsCalibration
  double m_timsC6     = 0;
  double m_timsC7     = 0;
};
} // namespace pappso
