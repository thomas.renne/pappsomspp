/**
 * \file pappsomspp/widget/graphicdevicewidget.cpp
 * \date 01/02/2018
 * \author Olivier Langella
 * \brief common graphic functions
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#include "graphicdevicewidget.h"
#include <QSvgGenerator>

GraphicDeviceWidget::GraphicDeviceWidget(QWidget *parent) : QWidget(parent)
{
}
GraphicDeviceWidget::~GraphicDeviceWidget()
{
}
void
GraphicDeviceWidget::toSvgFile(const QString &filename,
                               const QString &title,
                               const QString &description,
                               const QSize &size)
{
  QSvgGenerator generator;
  // generator.setOutputDevice(&buffer);
  generator.setFileName(filename);
  generator.setSize(size);
  generator.setViewBox(QRect(0, 0, size.width(), size.height()));
  generator.setTitle(title);
  generator.setDescription(description);

  this->toQPaintDevice(&generator, size);
}
