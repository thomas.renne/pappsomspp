/**
 * \file pappsomspp/widget/spectrumwidget/qcpspectrum.h
 * \date 31/12/2017
 * \author Olivier Langella
 * \brief Custom plot derivative to plot a spectrum
 */


/*******************************************************************************
 * Copyright (c) 2017 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/


#pragma once


#include <qcustomplot.h>
#include "../../psm/peakionisotopematch.h"
#include "../../types.h"

namespace pappso
{

class MassSpectrumWidget;

class QCPSpectrum : public QCustomPlot
{
  Q_OBJECT
  protected:
  friend class MassSpectrumWidget;
  QCPSpectrum(MassSpectrumWidget *parent, bool visible);
  ~QCPSpectrum();

  protected:
  void clearData();
  void rescale();
  void setSpectrumP(const MassSpectrum *spectrum);
  void addMassDelta(const PeakIonIsotopeMatch &peak_ion_match);
  void
  addMs1IsotopePattern(const std::vector<pappso::PeptideNaturalIsotopeAverageSp>
                         &isotope_mass_list,
                       pappso_double intensity);
  void addPeakIonIsotopeMatch(const PeakIonIsotopeMatch &peak_ion_match);
  virtual void mouseMoveEvent(QMouseEvent *event);
  virtual void mousePressEvent(QMouseEvent *event);
  virtual void mouseReleaseEvent(QMouseEvent *event);
  virtual void keyPressEvent(QKeyEvent *event);
  virtual void keyReleaseEvent(QKeyEvent *event);

  private:
  void getNearestPeakBetween(pappso_double mz, pappso_double mouse_mz_range);

  private:
  Q_SLOT void setMzRangeChanged(QCPRange range);

  private:
  MassSpectrumWidget *_parent;
  const MassSpectrum *_p_spectrum = nullptr;
  QCPRange _mz_range;
  QCPRange _intensity_range;
  QCPRange _mass_delta_range;
  QCPBars *_p_peak_bars;
  QCPBars *_p_peak_bars_isotope;
  std::map<PeptideIon, QCPBars *> _map_ion_type_bars;
  QCPAxisRect *_p_delta_axis_rect;
  QCPGraph *_p_delta_graph;
  double _bar_width = 0.5;
  bool _click       = false;
  bool _control_key = false;
  pappso::pappso_double _old_x;
  pappso::pappso_double _old_y;
};


} // namespace pappso
