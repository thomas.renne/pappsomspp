/* This code comes right from the msXpertSuite software project.
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QBrush>
#include <QColor>
#include <QVector>


/////////////////////// QCustomPlot
#include <qcustomplot.h>


/////////////////////// Local includes
#include "baseplotwidget.h"
#include "colormapplotconfig.h"
#include "../../trace/trace.h"


namespace pappso
{


class BaseColorMapPlotWidget;

typedef std::shared_ptr<BaseColorMapPlotWidget> BaseColorMapPlotWidgetSPtr;
typedef std::shared_ptr<const BaseColorMapPlotWidget>
  BaseColorMapPlotWidgetCstSPtr;

class BaseColorMapPlotWidget : public BasePlotWidget
{
  Q_OBJECT;

  public:
  explicit BaseColorMapPlotWidget(QWidget *parent,
                                  const QString &x_axis_label,
                                  const QString &y_axis_label);

  virtual ~BaseColorMapPlotWidget();

  virtual void
  setColorMapPlotConfig(const ColorMapPlotConfig &color_map_config);
  virtual const ColorMapPlotConfig &getColorMapPlotConfig();

  virtual QCPColorMap *addColorMap(
    std::shared_ptr<std::map<double, MapTrace>> double_map_trace_map_sp,
    ColorMapPlotConfig color_map_plot_config,
    const QColor &color);

  signals:

  protected:
  ColorMapPlotConfig m_colorMapPlotConfig;
};


} // namespace pappso
