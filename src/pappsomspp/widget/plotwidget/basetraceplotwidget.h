/* This code comes right from the msXpertSuite software project.
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


#pragma once

/////////////////////// StdLib includes


/////////////////////// Qt includes
#include <QObject>
#include <QString>
#include <QWidget>
#include <QBrush>
#include <QColor>
#include <QVector>


/////////////////////// QCustomPlot
#include <qcustomplot.h>


/////////////////////// Local includes
#include "baseplotwidget.h"
#include "../../trace/trace.h"


namespace pappso
{


class BaseTracePlotWidget;

typedef std::shared_ptr<BaseTracePlotWidget> BaseTracePlotWidgetSPtr;
typedef std::shared_ptr<const BaseTracePlotWidget> BaseTracePlotWidgetCstSPtr;

class BaseTracePlotWidget : public BasePlotWidget
{
  Q_OBJECT;

  public:
  explicit BaseTracePlotWidget(QWidget *parent,
                 const QString &x_axis_label,
                 const QString &y_axis_label);

  virtual ~BaseTracePlotWidget();

  virtual void setGraphData(int graph_index,
                            const std::vector<double> &keys,
                            const std::vector<double> &values);

  virtual void setGraphData(QCPGraph *graph_p,
                            const std::vector<double> &keys,
                            const std::vector<double> &values);

  virtual void clearGraphData(int graph_index);

  virtual void axisDoubleClickHandler(QCPAxis *axis,
                                      QCPAxis::SelectablePart part,
                                      QMouseEvent *event) override;

	// All these need to be overridden because of some special treatment in case
	// of Trace plots (graphs, specifically, and not color maps, for example).
  virtual void axisRescale() override;
  virtual void axisReframe() override;
  virtual void axisZoom() override;
  virtual void axisPan() override;

  virtual QCPGraph *addTrace(const pappso::Trace &trace, const QColor &color);

  virtual bool
  findIntegrationLowerRangeForKey(int index, double key, QCPRange &range);

  std::vector<double> getValuesX(int index) const;
  std::vector<double> getValuesY(int index) const;

  QCPRange getValueRangeOnKeyRange(QCPAbstractPlottable *plottable_p, bool &ok);
  QCPRange getValueRangeOnKeyRange(int index, bool &ok);

  double getYatX(double x, QCPGraph *graph_p);
  Q_INVOKABLE double getYatX(double x, int index = -1);

  pappso::Trace toTrace(int index) const;
  pappso::Trace toTrace(const QCPGraph *graph_p) const;
  pappso::Trace toTrace(const QCPRange &x_axis_range, int index) const;
  pappso::Trace toTrace(const QCPRange &x_axis_range,
                        const QCPGraph *graph_p) const;

  signals:

  protected:
};


} // namespace pappso
