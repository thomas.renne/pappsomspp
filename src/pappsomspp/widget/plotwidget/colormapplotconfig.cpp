// Copyright Filippo Rusconi, GPLv3+

/////////////////////// StdLib includes


/////////////////////// Qt includes


/////////////////////// Local includes
#include "colormapplotconfig.h"


namespace pappso
{

ColorMapPlotConfig::ColorMapPlotConfig()
{
}

ColorMapPlotConfig::ColorMapPlotConfig(std::size_t key_cell_count,
                                       std::size_t mz_cell_count,
                                       double min_key_value,
                                       double max_key_value,
                                       double min_mz_value,
                                       double max_mz_value)
  : keyCellCount(key_cell_count),
    mzCellCount(mz_cell_count),
    minKeyValue(min_key_value),
    maxKeyValue(max_key_value),
    minMzValue(min_mz_value),
    maxMzValue(max_mz_value)
{
}


QString
ColorMapPlotConfig::toString() const
{
  QString text = QString(
                   "keyCellCount: %1 - mzCellCount: %2 - minKeyValue: %3 - "
                   "maxKeyValue: %4 - "
                   "minMzValue: %5 - maxMzValue: %6")
                   .arg(keyCellCount)
                   .arg(mzCellCount)
                   .arg(minKeyValue)
                   .arg(maxKeyValue)
                   .arg(minMzValue)
                   .arg(maxMzValue);

  return text;
}

} // namespace pappso
