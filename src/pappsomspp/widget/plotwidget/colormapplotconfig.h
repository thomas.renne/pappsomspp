// Copyright Filippo Rusconi, GPLv3+

/////////////////////// StdLib includes
#include <limits>


/////////////////////// Qt includes
#include<QString>


/////////////////////// Local includes


#pragma once

namespace pappso
{

struct ColorMapPlotConfig
{
  std::size_t keyCellCount = 0;
  std::size_t mzCellCount  = 0;

  double minKeyValue = std::numeric_limits<double>::max();
  double maxKeyValue = std::numeric_limits<double>::min();

  double minMzValue = std::numeric_limits<double>::max();
  double maxMzValue = std::numeric_limits<double>::min();

  ColorMapPlotConfig();
  ColorMapPlotConfig(std::size_t key_cell_count,
                     std::size_t mz_cell_count,
                     double min_key_value,
                     double max_key_value,
                     double min_mz_value,
                     double max_mz_value);
	QString toString() const;
};


} // namespace pappso
