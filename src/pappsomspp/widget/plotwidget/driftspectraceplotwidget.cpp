/* This code comes right from the msXpertSuite software project.
 *
 * msXpertSuite - mass spectrometry software suite
 * -----------------------------------------------
 * Copyright(C) 2009,...,2018 Filippo Rusconi
 *
 * http://www.msxpertsuite.org
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * END software license
 */


/////////////////////// StdLib includes
#include <vector>


/////////////////////// Qt includes
#include <QVector>


/////////////////////// Local includes
#include "driftspectraceplotwidget.h"
#include "../pappsoexception.h"


namespace pappso
{


DriftSpecTracePlotWidget::DriftSpecTracePlotWidget(QWidget *parent,
                                         const QString &x_axis_label,
                                         const QString &y_axis_label)
  : BaseTracePlotWidget(parent, x_axis_label, y_axis_label)
{
  qDebug();
}


DriftSpecTracePlotWidget::~DriftSpecTracePlotWidget()
{
}


//! Set the \c m_pressedKeyCode to the key code in \p event.
void
DriftSpecTracePlotWidget::keyPressEvent(QKeyEvent *event)
{
  BasePlotWidget::keyPressEvent(event);
}


//! Handle specific key codes and trigger respective actions.
void
DriftSpecTracePlotWidget::keyReleaseEvent(QKeyEvent *event)
{
  BasePlotWidget::keyReleaseEvent(event);
}


//! Handle mouse movements, in particular record all the last visited points.
/*!

  This function is reponsible for storing at each time the last visited point
  in the graph. Here, point is intended as any x/y coordinate in the plot
  widget viewport, not a graph point.

  The stored values are then the basis for a large set of calculations
  throughout all the plot widget.

  \param pointer to QMouseEvent from which to retrieve the coordinates of the
  visited viewport points.
  */
void
DriftSpecTracePlotWidget::mouseMoveHandler(QMouseEvent *event)
{
  BasePlotWidget::mouseMoveHandler(event);
}


void
DriftSpecTracePlotWidget::mouseMoveHandlerNotDraggingCursor()
{
  BasePlotWidget::mouseMoveHandlerNotDraggingCursor();
}


void
DriftSpecTracePlotWidget::mouseMoveHandlerDraggingCursor()
{
  BasePlotWidget::mouseMoveHandlerDraggingCursor();
}


//! Record the clicks of the mouse.
void
DriftSpecTracePlotWidget::mousePressHandler(QMouseEvent *event)
{
  BasePlotWidget::mousePressHandler(event);
}


//! React to the release of the mouse buttons.
void
DriftSpecTracePlotWidget::mouseReleaseHandler(QMouseEvent *event)
{
  BasePlotWidget::mouseReleaseHandler(event);
}


} // namespace pappso
