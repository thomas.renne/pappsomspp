/**
 * \file pappsomspp/widget/precision/precisionwidget.h
 * \date 5/1/2018
 * \author Olivier Langella
 * \brief edit presicion in ppm or dalton
 */


/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once


#include <QWidget>
#include <QComboBox>
#include <QDoubleSpinBox>
#include "../../mzrange.h"

namespace pappso
{

class PrecisionWidget : public QWidget
{
  Q_OBJECT

  private:
  QComboBox *mp_unitComboBox;

  QDoubleSpinBox *mp_ppmValueSpinBox;
  QDoubleSpinBox *mp_resValueSpinBox;
  QDoubleSpinBox *mp_daltonValueSpinBox;

  PrecisionPtr mp_precisionDalton;
  PrecisionPtr mp_precisionPpm;
  PrecisionPtr mp_precisionRes;

  int m_oldIndex;

  Q_SLOT void setCurrentIndex(int);
  Q_SLOT void setPpmValueChanged(double);
  Q_SLOT void setResValueChanged(double);
  Q_SLOT void setDaltonValueChanged(double);

  public:
  PrecisionWidget(QWidget *parent = 0);
  ~PrecisionWidget();

  void setPrecision(PrecisionPtr precision);
  const PrecisionPtr &getPrecision() const;

  signals:
  void precisionChanged(pappso::PrecisionPtr precision) const;
};


} // namespace pappso
