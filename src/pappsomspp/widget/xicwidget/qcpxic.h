/**
 * \file pappsomspp/widget/xicwidget/qcpxic.h
 * \date 12/1/2018
 * \author Olivier Langella
 * \brief custom plot XIC
 */


/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef QCPXIC_H
#define QCPXIC_H

#include <qcustomplot.h>
#include "xicwidget.h"

namespace pappso
{
class QCPXic : public QCustomPlot
{
  Q_OBJECT
  protected:
  friend class XicWidget;
  QCPXic(XicWidget *parent);
  ~QCPXic();


  protected:
  void clear();
  void rescale();
  void addXicP(const Xic *xic_p);
  void addMsMsEvent(const Xic *xic_p, pappso::pappso_double rt);
  void
  addXicPeakList(const Xic *xic_p,
                 const std::vector<pappso::TracePeakCstSPtr> &xic_peak_list);
  void setName(const Xic *xic_p, const QString &name);
  void drawXicPeakBorders(unsigned int i,
                          const Xic *xic_p,
                          const pappso::TracePeak *p_xic_peak);
  void clearXicPeakBorders();

  virtual void mouseMoveEvent(QMouseEvent *event) override;
  virtual void mousePressEvent(QMouseEvent *event) override;
  virtual void mouseReleaseEvent(QMouseEvent *event) override;
  virtual void keyPressEvent(QKeyEvent *event) override;
  virtual void keyReleaseEvent(QKeyEvent *event) override;

  private:
  Q_SLOT void setRtRangeChanged(QCPRange range);

  pappso::pappso_double
  getRetentionTimeFromSecondsToLocal(pappso::pappso_double rt) const;
  pappso::pappso_double xAxisToSeconds(pappso::pappso_double rt) const;

  private:
  XicWidget *_parent;
  QCPRange _rt_range;
  QCPRange _intensity_range;
  bool _click       = false;
  bool _mouse_move  = false;
  bool _control_key = false;
  pappso::pappso_double _old_x;
  pappso::pappso_double _old_y;
  QCPItemTracer *_current_ms2_event = nullptr;
  std::map<const Xic *, QCPGraph *> _map_xic_graph;
  QColor _graph_color;
  std::vector<QColor> _colours;
  std::vector<QCPGraph *> _graph_peak_surface_list;
  std::vector<QCPItemTracer *> _graph_peak_border_list;
};
} // namespace pappso
#endif // QCPXIC_H
