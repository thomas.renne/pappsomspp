/**
 * \file pappsomspp/widget/xicwidget/xicwidget.h
 * \date 12/1/2018
 * \author Olivier Langella
 * \brief plot a XIC
 */


/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#ifndef XICWIDGET_H
#define XICWIDGET_H

#include <QWidget>
#include "../../xic/xic.h"
#include "../../processing/detection/tracepeak.h"
#include "../graphicdevicewidget.h"


namespace pappso
{
class QCPXic;

typedef std::vector<std::pair<XicCstSPtr, TracePeakCstSPtr>> XicXicPeakPairList;

class XicWidget : public GraphicDeviceWidget
{
  Q_OBJECT
  public:
  XicWidget(QWidget *parent = 0);
  ~XicWidget();


  void addXicSp(XicCstSPtr &xic_sp);
  void addMsMsEvent(const Xic *xic_p, pappso::pappso_double rt);
  void
  addXicPeakList(const Xic *xic_p,
                 const std::vector<pappso::TracePeakCstSPtr> &xic_peak_list);
  void setName(const Xic *xic_p, const QString &name);
  const QString &getName(const Xic *xic_p) const;
  void clear();
  void plot();
  void rescale();
  void toQPaintDevice(QPaintDevice *device, const QSize &size) override;
  void setRetentionTimeInSeconds();
  void setRetentionTimeInMinutes();
  void drawXicPeakBorders(pappso::TracePeakCstSPtr xic_peak);
  void clearXicPeakBorders();

  signals:
  /** @brief announce the current retention time (under mouse) in seconds
   */
  void rtChanged(double rt) const;
  void xicPeakListChanged(pappso::XicXicPeakPairList xic_peak_list) const;
  /** @brief announce mouse position on click (rt in seconds, intensity)
   */
  void clicked(double rt, double intensity) const;

  protected:
  friend class QCPXic;
  void rtChangeEvent(pappso::pappso_double rt) const;
  void xicClickEvent(pappso::pappso_double rt,
                     pappso::pappso_double intensity) const;
  void replotAll();
  XicCstSPtr getXicCstSPtr(const Xic *xic_p) const;

  protected:
  bool _rt_in_seconds = true;

  private:
  std::map<const Xic *, QString> _map_xic_name;
  std::map<const Xic *, pappso::pappso_double> _map_xic_msms_event;
  std::vector<XicCstSPtr> _xic_sp_list;
  XicXicPeakPairList _xic_peak_sp_list;
  QCPXic *_qcp_xic;
};
} // namespace pappso

#endif // XICWIDGET_H
