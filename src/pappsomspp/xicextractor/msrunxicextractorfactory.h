/**
 * \file pappsomspp/xicextractor/msrunxicextractorfactory.h
 * \date 07/05/2018
 * \author Olivier Langella
 * \brief factory to build XIC extractor on an MsRun file
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#pragma once

#include "msrunxicextractorinterface.h"


namespace pappso
{
/** @brief factory to build different kinds of XIC extractors
 */
class MsRunXicExtractorFactory
{
  private:
  static MsRunXicExtractorFactory m_instance;
  QString m_tmpDirName;

  MsRunXicExtractorFactory();
  virtual ~MsRunXicExtractorFactory();

  public:
  /** @brief singleton to get the only instance of the factory
   */
  static MsRunXicExtractorFactory &getInstance();

  /** @brief build a simple XIC extractor that directly uses Proeowizard library
   * to read and extract XICs building the xic extractor is fast, but extracting
   * each XIC is slow
   * @param msrun_reader the MsRun reader on which the XIC extractor will run
   */
  MsRunXicExtractorInterfaceSp
  buildMsRunXicExtractorSp(MsRunReaderSPtr &msrun_reader) const;

  /** @brief build Xic extractor that first read the whole MsRun, put it on disk
   * and extract XICs more quickly This needs some space on disk to store slices
   * (1 dalton each) building the XIC extractor is slow extracting XIC from
   * slices is a very quick operation
   * @param msrun_reader the MsRun reader on which the XIC extractor will run
   */
  MsRunXicExtractorInterfaceSp
  buildMsRunXicExtractorDiskSp(MsRunReaderSPtr &msrun_reader) const;

  /** @brief build Xic extractor that first read the whole MsRun, put it on disk
   * using a write cache and extract XICs more quickly This needs some space on
   * disk to store slices (1 dalton each) building the XIC extractor is slower
   * than buildMsRunXicExtractorPwizSp, but faster than
   * buildMsRunXicExtractorDiskSp extracting XIC from slices is a very quick
   * operation
   * @param msrun_reader the MsRun reader on which the XIC extractor will run
   */
  MsRunXicExtractorInterfaceSp
  buildMsRunXicExtractorDiskBufferSp(MsRunReaderSPtr &msrun_reader) const;

  /// set the temporary working directory
  void setTmpDir(const QString &dir_name);
};

} // namespace pappso
