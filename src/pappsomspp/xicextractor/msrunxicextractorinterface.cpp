/**
 * \file pappsomspp/xicextractor/msrunxicextractor.cpp
 * \date 07/05/2018
 * \author Olivier Langella
 * \brief base interface to build XICs on an MsRun file
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "msrunxicextractorinterface.h"

namespace pappso
{


MsRunXicExtractorInterface::MsRunXicExtractorInterface(
  MsRunReaderSPtr &msrun_reader)
  : msp_msrun_reader(msrun_reader)
{
}


MsRunXicExtractorInterface::MsRunXicExtractorInterface(
  const MsRunXicExtractorInterface &other)
  : msp_msrun_reader(other.msp_msrun_reader)
{
  m_xicExtractMethod = other.m_xicExtractMethod;
}

MsRunXicExtractorInterface::~MsRunXicExtractorInterface()
{
}

XicCstSPtr
MsRunXicExtractorInterface::getXicCstSPtr(const MzRange &mz_range)
{
  return getXicCstSPtr(mz_range, 0, 100000000);
}

void
MsRunXicExtractorInterface::setXicExtractMethod(XicExtractMethod method)
{
  m_xicExtractMethod = method;
}
const MsRunIdCstSPtr &
MsRunXicExtractorInterface::getMsRunId() const
{
  return msp_msrun_reader.get()->getMsRunId();
}
} // namespace pappso
