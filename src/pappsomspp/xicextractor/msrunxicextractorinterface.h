/**
 * \file pappsomspp/xicextractor/msrunxicextractor.h
 * \date 07/05/2018
 * \author Olivier Langella
 * \brief base interface to build XICs on an MsRun file
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/

#pragma once


#include "../msrun/msrunreader.h"
#include <memory>
#include <vector>
#include "../mzrange.h"
#include "../xic/xic.h"


namespace pappso
{

class MsRunXicExtractorInterface;
typedef std::shared_ptr<MsRunXicExtractorInterface>
  MsRunXicExtractorInterfaceSp;

class MsRunXicExtractorInterface
{
  protected:
  struct MsRunXicExtractorPoints
  {
    std::size_t spectrum_index;
    double rt;
  };

  /** @brief class to read retention time points of MsRun
   */
  class MsRunXicExtractorReadPoints : public SpectrumCollectionHandlerInterface
  {
    private:
    std::vector<MsRunXicExtractorPoints> &m_msrun_points;

    public:
    MsRunXicExtractorReadPoints(
      std::vector<MsRunXicExtractorPoints> &msrun_points)
      : m_msrun_points(msrun_points){};

    virtual void
    setQualifiedMassSpectrum(const QualifiedMassSpectrum &spectrum) override
    {
      if(spectrum.getMsLevel() == 1)
        {
          m_msrun_points.push_back(
            {spectrum.getMassSpectrumId().getSpectrumIndex(),
             spectrum.getRtInSeconds()});
        }
    }
    virtual bool
    needPeakList() const override
    {
      return false;
    }
    virtual void
    loadingEnded() override
    {
    }
  };

  MsRunReaderSPtr msp_msrun_reader;
  XicExtractMethod m_xicExtractMethod = XicExtractMethod::max;

  /** @brief constructor is private, use the MsRunXicExtractorFactory
   */
  MsRunXicExtractorInterface(MsRunReaderSPtr &msrun_reader);
  MsRunXicExtractorInterface(const MsRunXicExtractorInterface &other);
  virtual ~MsRunXicExtractorInterface();

  public:
  /** @brief set the XIC extraction method
   */
  void setXicExtractMethod(XicExtractMethod method); // sum or max

  /** @brief get a XIC on this MsRun at the given mass range
   * @param mz_range mz range to extract
   */
  virtual XicCstSPtr getXicCstSPtr(const MzRange &mz_range) final;

  /** @brief get a XIC on this MsRun at the given mass range
   * @param mz_range mz range to extract
   * @param rt_begin begining of the XIC in seconds
   * @param rt_end end of the XIC in seconds
   */
  virtual XicCstSPtr getXicCstSPtr(const MzRange &mz_range,
                                   pappso::pappso_double rt_begin,
                                   pappso::pappso_double rt_end) = 0;

  /** @brief extract a list of XIC given a list of mass to extract
   */
  virtual std::vector<XicCstSPtr>
  getXicCstSPtrList(const std::vector<MzRange> &mz_range_list) = 0;

  const MsRunIdCstSPtr &getMsRunId() const;
};


} // namespace pappso
