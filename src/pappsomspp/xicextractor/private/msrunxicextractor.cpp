/**
 * \file pappsomspp/xicextractor/private/msrunxicextractorpwiz.cpp
 * \date 07/05/2018
 * \author Olivier Langella
 * \brief simple proteowizard based XIC extractor
 */

/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

#include "msrunxicextractor.h"
#include <QDebug>
#include "../../pappsoexception.h"
#include "../../processing/filters/filterresample.h"

namespace pappso
{


MsRunXicExtractor::MsRunXicExtractor(MsRunReaderSPtr &msrun_reader)
  : pappso::MsRunXicExtractorInterface(msrun_reader)
{

  MsRunXicExtractorReadPoints get_msrun_points(m_msrun_points);
  msp_msrun_reader.get()->readSpectrumCollection(get_msrun_points);

  std::sort(m_msrun_points.begin(),
            m_msrun_points.end(),
            [](const MsRunXicExtractorPoints &a,
               const MsRunXicExtractorPoints &b) { return a.rt < b.rt; });


  if(m_msrun_points.size() == 0)
    {
      throw pappso::PappsoException(
        QObject::tr("error extracting XIC: no MS level 1 in data file"));
    }
}
MsRunXicExtractor::~MsRunXicExtractor()
{
}


MsRunXicExtractor::MsRunXicExtractor(const MsRunXicExtractor &other)
  : MsRunXicExtractorInterface(other)
{
  m_msrun_points = other.m_msrun_points;
}

XicCstSPtr
MsRunXicExtractor::getXicCstSPtr(const MzRange &mz_range,
                                 pappso::pappso_double rt_begin,
                                 pappso::pappso_double rt_end)
{
  FilterResampleKeepXRange keep_range(mz_range.lower(), mz_range.upper());
  std::shared_ptr<Xic> msrunxic_sp = std::make_shared<Xic>(Xic());


  auto itpoints = m_msrun_points.begin();

  while((itpoints != m_msrun_points.end()) && (itpoints->rt < rt_begin))
    {
      itpoints++;
    }
  MassSpectrumSPtr spectrum;
  DataPoint peak;
  while((itpoints != m_msrun_points.end()) && (itpoints->rt <= rt_end))
    {
      spectrum =
        msp_msrun_reader.get()->massSpectrumSPtr(itpoints->spectrum_index);
      // qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__ << " "
      // << spectrum->size(); spectrum->debugPrintValues();

      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " spectrum->size()=" << spectrum->size();
      keep_range.filter(*(spectrum.get()));
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " spectrum->size()=" << spectrum->size();

      peak.x = itpoints->rt;

      if(m_xicExtractMethod == XicExtractMethod::max)
        {
          peak.y = 0;
          if(spectrum->size() > 0)
            {
              peak.y = maxYDataPoint(spectrum->begin(), spectrum->end())->y;

              qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
                       << " peak.y=" << peak.y
                       << " spectrum->size()=" << spectrum->size();
            }
        }
      else
        {
          peak.y = sumYTrace(spectrum->begin(), spectrum->end(), 0);
        }
      msrunxic_sp->push_back(peak);

      itpoints++;
    }


  return (msrunxic_sp);
}

std::vector<XicCstSPtr>
MsRunXicExtractor::getXicCstSPtrList(const std::vector<MzRange> &mz_range_list)
{

  std::vector<std::shared_ptr<Xic>> xic_list_return;
  std::vector<Xic *> xic_list;
  for(std::size_t i = 0; i < mz_range_list.size(); i++)
    {
      xic_list_return.push_back(std::make_shared<Xic>(Xic()));
      xic_list.push_back(xic_list_return.back().get());
      xic_list_return.back().get()->reserve(m_msrun_points.size());
    }
  getXicFromPwizMSDataFile(xic_list, mz_range_list, 0, 100000000);
  std::vector<XicCstSPtr> xic_list_return_b;
  for(auto &xic : xic_list_return)
    {
      xic_list_return_b.push_back(xic);
    }
  return xic_list_return_b;
}

void
MsRunXicExtractor::getXicFromPwizMSDataFile(
  std::vector<Xic *> &xic_list,
  const std::vector<MzRange> &mass_range_list,
  pappso::pappso_double rt_begin,
  pappso::pappso_double rt_end)
{
  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  std::vector<DataPoint> peak_for_mass;
  for(const MzRange &mass_range : mass_range_list)
    {
      peak_for_mass.push_back(DataPoint());
      qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__
               << " mass_range=" << mass_range.getMz();
    }


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;

  auto itpoints = m_msrun_points.begin();

  while((itpoints != m_msrun_points.end()) && (itpoints->rt < rt_begin))
    {
      itpoints++;
    }

  MassSpectrumCstSPtr spectrum;
  while((itpoints != m_msrun_points.end()) && (itpoints->rt <= rt_end))
    {
      spectrum =
        msp_msrun_reader.get()->massSpectrumCstSPtr(itpoints->spectrum_index);

      for(DataPoint &peak : peak_for_mass)
        {
          peak.x = itpoints->rt;
          peak.y = 0;
        }


      // iterate through the m/z-intensity pairs
      for(auto &&spectrum_point : *(spectrum.get()))
        {
          // qDebug() << "getXicFromPwizMSDataFile it->mz " << it->mz <<
          // " it->intensity" << it->intensity;
          for(std::size_t i = 0; i < mass_range_list.size(); i++)
            {
              if(mass_range_list[i].contains(spectrum_point.x))
                {
                  if(m_xicExtractMethod == XicExtractMethod::max)
                    {
                      if(peak_for_mass[i].y < spectrum_point.y)
                        {
                          peak_for_mass[i].y = spectrum_point.y;
                        }
                    }
                  else
                    {
                      peak_for_mass[i].y += spectrum_point.y;
                    }
                }
            }
        }

      for(std::size_t i = 0; i < mass_range_list.size(); i++)
        {
          // qDebug() << "getXicFromPwizMSDataFile push_back " <<
          // peak_for_mass[i].rt;
          xic_list[i]->push_back(peak_for_mass[i]);
        }

      itpoints++;
    }


  qDebug() << __FILE__ << " " << __FUNCTION__ << " " << __LINE__;
} // namespace pappso


} // namespace pappso
