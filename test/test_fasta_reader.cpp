//
// File: test_hyperscore.cpp
// Created by: Olivier Langella
// Created on: 13/3/2015
//
/*******************************************************************************
 * Copyright (c) 2015 Olivier Langella <Olivier.Langella@moulon.inra.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@moulon.inra.fr> - initial API and
 *implementation
 ******************************************************************************/

// make test ARGS="-V -I 1,1"

#include <iostream>
#include <pappsomspp/fasta/fastareader.h>
#include <pappsomspp/fasta/fastafileindexer.h>
#include <pappsomspp/exception/exceptionoutofrange.h>
#include <QFileInfo>
//#include "common.h"
#include "config.h"

using namespace std;
// using namespace pwiz::msdata;

class FastaSeq : public pappso::FastaHandlerInterface
{
  public:
  const QString &
  getDescription() const
  {
    return description;
  };
  const QString &
  getSequence() const
  {
    return sequence;
  };
  void
  setSequence(const QString &description_in,
              const QString &sequence_in) override
  {
    cout << endl << "begin description=" << description_in.toStdString();
    cout << endl << "sequence=" << sequence_in.toStdString();
    cout << endl << "end" << endl;
    description = description_in;
    sequence    = sequence_in;
  };

  private:
  QString description;
  QString sequence;
};


int
main()
{

  cout << endl << "..:: Test Fasta reader ::.." << endl;

  QFile fastaFile(
    QString(CMAKE_SOURCE_DIR).append("/test/data/asr1_digested_peptides.txt"));
  FastaSeq seq;
  pappso::FastaReader reader(seq);
  reader.parse(fastaFile);

  /*
 Spectrum spectrum_low_masses(spectrum_parent.applyCutOff(150));
 if (! spectrum_low_masses.equals(sremove_low_masses, precision)) {
     cerr << "spectrum_low_masses() != tandem"<< endl;
     return 1;
 }*/
  cout << endl << "..:: Test Fasta file indexer ::.." << endl;
  QFileInfo file(
    QString(CMAKE_SOURCE_DIR).append("/test/data/asr1_digested_peptides.txt"));
  pappso::FastaFileIndexer fasta_file_indexer(file);

  fasta_file_indexer.getSequenceByIndex(seq, 0);

  fasta_file_indexer.getSequenceByIndex(seq, 2);


  try
    {
      fasta_file_indexer.getSequenceByIndex(seq, 13);
    }
  catch(pappso::ExceptionOutOfRange &error)
    {
      cerr << endl << "ERROR: " << error.qwhat().toStdString() << endl;
    }

  cout << endl << " ExceptionOutOfRange is OK " << endl;


  try
    {
      fasta_file_indexer.getSequenceByIndex(seq, 12);

      if(seq.getSequence() !=
         "HNMLGGCPK HHHHHLFHHK HNMLGGCPKER YEEHLYER RIEAIPQIDK LTQSMAIIR "
         "AEISMLEGAVLDIRYGVSR AEISMLEGAVLDIR IAYSKDFETLK YEEHLYERDEGDK GLVQPTR "
         "YIAWPLQGWQATFGGGDHPPK FELGLEFPNLPYYIDGDVK "
         "TYLNGDHVTHPDFMLYDALDVVLYMDPMCLDAFPK MSPILGYWKIK DFETLKVDFLSK "
         "GLVQPTRLLLEYLEEK IKGLVQPTR MSPILGYWK IEAIPQIDK LLLEYLEEK "
         "YIADKHNMLGGCPK "
         "SSKYIAWPLQGWQATFGGGDHPPK SDLEVLFQGPLGSMAEEK "
         "FELGLEFPNLPYYIDGDVKLTQSMAIIR "
         "IEAIPQIDKYLK YGVSRIAYSK VDFLSKLPEMLK MFEDRLCHK LPEMLKMFEDR DEGDKWR "
         "LLLEYLEEKYEEHLYER LTQSMAIIRYIADK KFELGLEFPNLPYYIDGDVK "
         "ERAEISMLEGAVLDIR")
        {

          cerr << "sequene 12 is not OK" << endl;
          return 1;
        }

      cout << endl << " getSequenceByIndex(seq, 12) is OK " << endl;
    }
  catch(pappso::ExceptionOutOfRange &error)
    {
      cerr << endl << "ERROR: " << error.qwhat().toStdString() << endl;
      return 1;
    }

  cout << endl << " getSequenceByIndex(seq, 2)" << endl;
  fasta_file_indexer.getSequenceByIndex(seq, 2);


#if USEPAPPSOTREE == 1
  cout << endl << "..:: Test Fasta big file indexer ::.." << endl;
  QFileInfo file2("/gorgone/pappso/moulon/database/Genome_Z_mays_5a.fasta");
  pappso::FastaFileIndexer big_fasta_file_indexer(file2);
  big_fasta_file_indexer.getSequenceByIndex(seq, 12);

  cout << endl << seq.getDescription().toStdString() << endl;

  // GRMZM2G147579_P01
  // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
  // seq=translation; coord=5:217415249..217417029:-1;
  // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

  if(seq.getDescription() !=
     "GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271 "
     "seq=translation; coord=5:217415249..217417029:-1; "
     "parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579")
    {

      cerr << "sequence 12 in Genome_Z_mays_5a is not OK" << endl;
      return 1;
    }

#endif

  cout << endl << "..:: Test fuzzy fasta file indexer ::.." << endl;
  QFileInfo fuzzy_file(
    QString(CMAKE_SOURCE_DIR).append("/test/data/fuzzy.fasta"));
  pappso::FastaFileIndexer fuzzy_fasta_file_indexer(fuzzy_file);
  fuzzy_fasta_file_indexer.getSequenceByIndex(seq, 0);

  cout << endl << seq.getDescription().toStdString() << endl;

  // GRMZM2G147579_P01
  // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
  // seq=translation; coord=5:217415249..217417029:-1;
  // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

  if(seq.getDescription() != "YGR254W ") // YGR254W
    {
      cerr << "sequence 0 in fuzzy.fasta is not OK seq.getDescription() != "
              "YGR254W "
           << seq.getDescription().toStdString() << endl;
      return 1;
    }
  fuzzy_fasta_file_indexer.getSequenceByIndex(seq, 2);

  cout << endl << seq.getDescription().toStdString() << endl;

  // GRMZM2G147579_P01
  // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
  // seq=translation; coord=5:217415249..217417029:-1;
  // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

  if(seq.getDescription() != "YGR254Wb") // YGR254Wb
    {
      cerr << "sequence 2 in fuzzy.fasta is not OK seq.getDescription() != "
              "YGR254Wb "
           << seq.getDescription().toStdString() << endl;
      return 1;
    }
  /*
    QFileInfo fuzzy_macos_file(
      QString(CMAKE_SOURCE_DIR).append("/test/data/fuzzy_macos.fasta"));
    pappso::FastaFileIndexer fuzzy_macos_fasta_file_indexer(fuzzy_macos_file);
    fuzzy_macos_fasta_file_indexer.getSequenceByIndex(seq, 0);

    cout << endl << "sequence0: " << seq.getDescription().toStdString() << endl;

    // GRMZM2G147579_P01
    // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
    // seq=translation; coord=5:217415249..217417029:-1;
    // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

    if(seq.getDescription() != "YGR254W ") // YGR254W
      {
        cerr
          << "sequence 0 in fuzzy_macos.fasta is not OK seq.getDescription() !=
    " "YGR254W "
          << seq.getDescription().toStdString() << endl;
        return 1;
      }
    fuzzy_macos_fasta_file_indexer.getSequenceByIndex(seq, 2);

    cout << endl << "sequence2: "<< seq.getDescription().toStdString() << endl;

    // GRMZM2G147579_P01
    // GRMZM2G147579_P01 NP_001159186 hypothetical protein LOC100304271
    // seq=translation; coord=5:217415249..217417029:-1;
    // parent_transcript=GRMZM2G147579_T01; parent_gene=GRMZM2G147579

    if(seq.getDescription() != "YGR254Wb") // YGR254Wb
      {
        cerr
          << "sequence 2 in fuzzy_macos.fasta is not OK seq.getDescription() !=
    " "YGR254Wb "
          << seq.getDescription().toStdString() << endl;
        return 1;
      }*/
  return 0;
}
