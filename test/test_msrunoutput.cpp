
/*******************************************************************************
 * Copyright (c) 2019 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 ******************************************************************************/


// make test ARGS="-V -I 21,21"
// cmake .. -DCMAKE_BUILD_TYPE=Debug  -DMAKE_TEST=1  -DUSEPAPPSOTREE=1


#include <iostream>

#include <pappsomspp/pappsoexception.h>
#include <pappsomspp/exception/exceptionnotpossible.h>
#include <pappsomspp/msfile/msfileaccessor.h>
#include <pappsomspp/msrun/mzxmloutput.h>
#include <QDebug>
#include <QtCore>
#include <QFile>
#include <QtConcurrent>
#include "config.h"
//#include "common.h"

using namespace std;


int
main(int argc, char *argv[])
{
  // QCoreApplication a(argc, argv);
  cout << endl
       << "..:: Test Qexception transferred accross threads ::.." << endl;
  QFuture<void> future = QtConcurrent::run([=]() {
    throw pappso::ExceptionNotPossible(
      QObject::tr("Not possible, but possible to catch it accross threads"));
  });
  try
    {
      future.waitForFinished();
    }
  catch(pappso::PappsoException &error)
    {
      cerr << endl << "ERROR: " << error.qwhat().toStdString() << endl;
    }
  // return 0;
  qDebug() << "init test MSrun output";
  cout << endl << "..:: Test MSrun output ::.." << endl;
  QTime timer;

#if USEPAPPSOTREE == 1

  cout << endl << "..:: Test MSrun output starts ::.." << endl;
  pappso::MsFileAccessor file_access_A01(
    "/gorgone/pappso/data_extraction_pappso/mzXML/"
    //"/home/langella/data1/mzxml/"
    "20120906_balliau_extract_1_A01_urnb-1.mzXML",
    "runa1");

  pappso::MsRunReaderSPtr msrunA01 =
    file_access_A01.getMsRunReaderSPtrByRunId("", "runa01");

  QTextStream outputStream(stdout, QIODevice::WriteOnly);

  pappso::MzxmlOutput mzxml_ouput(outputStream.device());

  mzxml_ouput.write(msrunA01.get());

  mzxml_ouput.close();



#endif
  return 0;
}
