//
// File: test_psm.cpp
// Created by: Olivier Langella
// Created on: 22/1/2018
//
/*******************************************************************************
 * Copyright (c) 2018 Olivier Langella <Olivier.Langella@u-psud.fr>.
 *
 * This file is part of the PAPPSOms++ library.
 *
 *     PAPPSOms++ is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PAPPSOms++ is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warrantyo f
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PAPPSOms++.  If not, see <http://www.gnu.org/licenses/>.
 *
 * Contributors:
 *     Olivier Langella <Olivier.Langella@u-psud.fr> - initial API and
 *implementation
 ******************************************************************************/

// make test ARGS="-V -I 17,17"

#include <pappsomspp/mzrange.h>
#include <pappsomspp/psm/peptidespectrummatch.h>
#include <iostream>
#include <iomanip>
#include <set>
#include <QDebug>
#include <QString>
#include "common.h"
#include "config.h"

using namespace pappso;
using namespace std;
// using namespace pwiz::msdata;


int
main()
{

  cout << endl << "..:: readMgf ::.." << endl;
  bool refine_spectrum_synthesis = false;

  MassSpectrum spectrum_simple =
    readMgf(QString(CMAKE_SOURCE_DIR)
              .append("/test/data/peaklist_15046_simple_xt.mgf"));
  //.applyCutOff(150).takeNmostIntense(100).applyDynamicRange(100);


  Peptide peptide("AIADGSLLDLLR");
  PeptideSp peptide_sp(peptide.makePeptideSp());
  // peptide_sp.get()->addAaModification(AaModification::getInstance("MOD:00397"),
  // 0);

  PrecisionPtr precision = PrecisionFactory::getDaltonInstance(0.02);
  std::list<PeptideIon> ion_list;
  ion_list.push_back(PeptideIon::y);
  ion_list.push_back(PeptideIon::b);
  cout << "spectrum_simple size  " << spectrum_simple.size() << endl;

  PeptideSpectrumMatch psm(spectrum_simple, peptide_sp, 2, precision, ion_list);

  unsigned int test_count = psm.countTotalMatchedIons();
  if(test_count != 11)
    {
      cerr << "test_count != 11 " << test_count << endl;
      return 1;
    }
  // hyperscore="33.5"
  precision = PrecisionFactory::getDaltonInstance(0.0);
  PeptideSpectrumMatch psmb(
    spectrum_simple, peptide_sp, 2, precision, ion_list);
  test_count = psmb.countTotalMatchedIons();
  if(test_count != 0)
    {
      cerr << "test_count != 0 " << test_count << endl;
      return 1;
    }
  return 0;
}
