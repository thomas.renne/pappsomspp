
# cd buildwin64
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/Toolchain-mingw64.cmake ..


SET (USEQT5 1)
set (MINGW 1)
set (MAKE_TEST 0)

set(CMAKE_CXX_COMPILER "/mingw64/bin/g++.exe")
set(CMAKE_AUTOMOC ON)
set(CMAKE_INCLUDE_CURRENT_DIR ON)

# QUAZIP_INCLUDE_DIR         - Path to QuaZip include dir
set (QUAZIP_INCLUDE_DIR "/home/polipo/devel/quazip-0.7.3")
# QUAZIP_INCLUDE_DIRS        - Path to QuaZip and zlib include dir (combined from QUAZIP_INCLUDE_DIR + ZLIB_INCLUDE_DIR)
# QUAZIP_LIBRARIES           - List of QuaZip libraries
set (QUAZIP_QT5_LIBRARIES "/home/polipo/devel/quazip-0.7.3/quazip/release/quazip.dll")
# QUAZIP_ZLIB_INCLUDE_DIR    - The include dir of zlib headers

set (QCustomPlot_INCLUDES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1")
set (QCustomPlot_LIBRARIES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1/build/libqcustomplot.so")


set(ZLIB_LIBRARIES "/mingw64/lib/libz.a")


set(ALGLIB_FOUND 1)
set(ALGLIB_INCLUDE_DIR "/home/polipo/devel/alglib-3.16/src")
set(ALGLIB_LIBRARY "/home/polipo/devel/alglib-3.16/build/libalglib.so")
if(NOT TARGET ALGLIB::ALGLIB)
        add_library(ALGLIB::ALGLIB UNKNOWN IMPORTED)
        set_target_properties(ALGLIB::ALGLIB PROPERTIES
            IMPORTED_LOCATION             "${ALGLIB_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${ALGLIB_INCLUDE_DIR}")
endif()

set(Pwiz_FOUND 1)
set(Pwiz_INCLUDE_DIR "/home/polipo/devel/pwiz")
set(Pwiz_LIBRARY "/home/polipo/devel/pwiz/libpwiz.a")
if(NOT TARGET Pwiz::Pwiz)
        add_library(Pwiz::Pwiz UNKNOWN IMPORTED)
        set_target_properties(Pwiz::Pwiz PROPERTIES
            IMPORTED_LOCATION             "${Pwiz_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${Pwiz_INCLUDE_DIR}")
endif()


set(Boost_FOUND 1)
set(Boost_INCLUDE_DIRS "/home/polipo/devel/boost/boost_1_56_0")
set(Boost_LIBRARY_DIRS "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib")

set (Boost_chrono_FOUND    1)
set (Boost_filesystem_FOUND  1)
set (Boost_iostreams_FOUND   1)
set (Boost_program_options_FOUND  1)
set (Boost_serialization_FOUND 1)
set (Boost_system_FOUND  1)
set (Boost_thread_FOUND  1)

set (Boost_chrono_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_chrono.dll")
set (Boost_filesystem_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_filesystem.dll")
set (Boost_iostreams_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_iostreams.dll")
set (Boost_program_options_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_program_options.dll")
set (Boost_serialization_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_serialization.dll")
set (Boost_system_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_system.dll")
set (Boost_thread_LIBRARY    "/home/polipo/devel/boost/boost_1_56_0/first-build-mingw64/lib/libboost_thread.dll")

set (Boost_VERSION   "1.56.0")
set (Boost_LIB_VERSION   "1_56")
set (Boost_MAJOR_VERSION  "1")
set (Boost_MINOR_VERSION   "56")
set (Boost_SUBMINOR_VERSION "0")



# ZSTD library stuff : download sources from debian for example
# dget -u http://deb.debian.org/debian/pool/main/libz/libzstd/libzstd_1.4.4+dfsg-1.dsc
# or git clone https://github.com/facebook/zstd.git (simple Makefile)
# to compile using CMake :
# https://github.com/facebook/zstd/blob/dev/build/cmake/README.md
# once compiled, set here Zstd_INCLUDE_DIR and Zstd_LIBRARY
set(Zstd_INCLUDE_DIR /usr/include)
mark_as_advanced(Zstd_INCLUDE_DIR)

# Look for the necessary library
set(Zstd_LIBRARY /usr/lib/x86_64-linux-gnu/libzstd.so)
mark_as_advanced(Zstd_LIBRARY)

set(Zstd_FOUND 1)
set(Zstd_INCLUDE_DIRS ${Zstd_INCLUDE_DIR})
set(Zstd_LIBRARIES ${Zstd_LIBRARY})
if(NOT TARGET Zstd::Zstd)
    add_library(Zstd::Zstd UNKNOWN IMPORTED)
    set_target_properties(Zstd::Zstd PROPERTIES
        IMPORTED_LOCATION             "${Zstd_LIBRARY}"
        INTERFACE_INCLUDE_DIRECTORIES "${Zstd_INCLUDE_DIR}")
endif()
