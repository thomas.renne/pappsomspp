
# cd build
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/centos7.cmake ..

set(USEQT5 1)
set(MAKE_TEST 0)

set(Pwiz_FOUND 1)
set(Pwiz_INCLUDE_DIR "/usr/local/include/pwiz")
set(Pwiz_LIBRARY "/usr/local/lib/libpwiz.so")
