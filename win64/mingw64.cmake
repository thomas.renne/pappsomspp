#download Qt5 mingw64 environment from :
#http://www.rpmfind.net//linux/RPM/fedora/21/x86_64/m/mingw64-qt5-qtbase-5.3.2-1.fc21.noarch.html
#ftp://195.220.108.108/linux/sourceforge/p/ps/pspp4windows/used-repositories/2015-06-19/win64/src/mingw64-zlib-1.2.8-8.13.src.rpm
#http://rpm.pbone.net/index.php3/stat/4/idpl/24061932/dir/opensuse_12.x/com/mingw64-quazip-0.4.4-2.39.noarch.rpm.html

# cd buildwin64
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/mingw64.cmake ..


set (MAKE_TEST 0)


# the name of the target operating system
SET(CMAKE_SYSTEM_NAME Windows)



set(CMAKE_CXX_COMPILER x86_64-w64-mingw32-g++)
set(CMAKE_C_COMPILER x86_64-w64-mingw32-gcc)
SET(CMAKE_RC_COMPILER x86_64-w64-mingw32-windres)


# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH  /usr/x86_64-w64-mingw32 )

# search for programs in the build host directories
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)
# for libraries and headers in the target directories
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)

# QUAZIP_INCLUDE_DIR         - Path to QuaZip include dir
set (QUAZIP_INCLUDE_DIR "/home/polipo/devel/quazip-0.7.3")
# QUAZIP_INCLUDE_DIRS        - Path to QuaZip and zlib include dir (combined from QUAZIP_INCLUDE_DIR + ZLIB_INCLUDE_DIR)
# QUAZIP_LIBRARIES           - List of QuaZip libraries
set (QUAZIP_QT5_LIBRARIES "/home/polipo/devel/quazip-0.7.3/quazip/release/quazip.dll")
# QUAZIP_ZLIB_INCLUDE_DIR    - The include dir of zlib headers

set (QCustomPlot_INCLUDES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1")
set (QCustomPlot_LIBRARIES "/home/polipo/devel/qcustomplot-1.3.2+dfsg1/build/libqcustomplot.so")


set(ZLIB_LIBRARIES "/media/langella/pappso/bin/libpwiz.a")


set(ALGLIB_FOUND 1)
set(ALGLIB_INCLUDE_DIR "/home/langella/developpement/git/alglib/cpp/src")
set(ALGLIB_LIB "/home/langella/developpement/git/alglib/cpp/build/libalglib.a")
if(NOT TARGET ALGLIB::ALGLIB)
        add_library(ALGLIB::ALGLIB UNKNOWN IMPORTED)
        set_target_properties(ALGLIB::ALGLIB PROPERTIES
            IMPORTED_LOCATION             "${ALGLIB_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${ALGLIB_INCLUDE_DIR}")
    endif()

set(Pwiz_FOUND 1)
set(Pwiz_INCLUDE_DIR "/media/langella/pappso/sources/libpwiz-3.0.18342")
set(Pwiz_LIBRARY "/media/langella/pappso/bin/libpwiz.a")

if(NOT TARGET Pwiz::Pwiz)
        add_library(Pwiz::Pwiz UNKNOWN IMPORTED)
        set_target_properties(Pwiz::Pwiz PROPERTIES
            IMPORTED_LOCATION             "${Pwiz_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${Pwiz_INCLUDE_DIR}")
    endif()

