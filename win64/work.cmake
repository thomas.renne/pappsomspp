
# cd build
# cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_TOOLCHAIN_FILE=../win64/work.cmake ..

set(USEQT5 1)
set(MAKE_TEST 1)

set(GORGONE 1)


set(ODSSTREAM_QT5_FOUND 1)
set(ODSSTREAM_INCLUDE_DIR "/home/langella/developpement/git/libodsstream/src")
set(ODSSTREAM_QT5_LIBRARY "/home/langella/developpement/git/libodsstream/cbuild/src/libodsstream-qt5.so")


   
if(NOT TARGET OdsStream::Core)
    add_library(OdsStream::Core UNKNOWN IMPORTED)
    set_target_properties(OdsStream::Core PROPERTIES
            IMPORTED_LOCATION             "${ODSSTREAM_QT5_LIBRARY}"
            INTERFACE_INCLUDE_DIRECTORIES "${ODSSTREAM_INCLUDE_DIR}"
    )
endif()
